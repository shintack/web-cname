-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 02, 2019 at 05:11 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_cname`
--

-- --------------------------------------------------------

--
-- Table structure for table `actions_logs`
--

CREATE TABLE `actions_logs` (
  `actions_logs_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email_login` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  `note` text,
  `url` varchar(255) DEFAULT NULL,
  `actions` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actions_logs`
--

INSERT INTO `actions_logs` (`actions_logs_id`, `user_id`, `email_login`, `user_agent`, `ip_address`, `note`, `url`, `actions`, `timestamp`) VALUES
(1, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-08-29 04:54:41'),
(2, 0, NULL, 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-08-29 05:07:09'),
(3, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-08-31 02:39:45'),
(4, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-08-31 06:24:35'),
(5, 0, NULL, 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-08-31 06:24:43'),
(6, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-08-31 06:24:52'),
(7, 0, NULL, 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-08-31 06:25:23'),
(8, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-08-31 06:25:39'),
(9, 0, NULL, 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-08-31 07:42:28'),
(10, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-08-31 07:43:19'),
(11, 0, NULL, 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-08-31 07:54:01'),
(12, 0, NULL, 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-08-31 15:02:14'),
(13, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-08-31 15:02:31'),
(14, 0, NULL, 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-08-31 15:37:55'),
(15, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-09-02 01:23:56'),
(16, 0, NULL, 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-09-02 01:25:55'),
(17, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-09-02 01:26:05'),
(18, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '158.140.185.62', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-03 19:45:38'),
(19, 1, 'admin@admin.com', 'Chrome 68.0.3440.106 | Linux', '::1', 'auth/login/', 'http://localhost/deplaza/auth/login.html', 'Login', '2018-09-03 20:21:34'),
(20, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '158.140.185.62', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-04 11:46:21'),
(21, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '158.140.185.62', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-04 18:52:48'),
(22, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '114.124.180.101', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-04 22:25:09'),
(23, 1, 'admin@admin.com', 'Firefox 61.0 | Linux', '158.140.185.62', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-06 17:30:37'),
(24, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-09-08 11:37:16'),
(25, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-09-10 09:30:48'),
(26, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '223.255.225.234', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-10 09:53:51'),
(27, 0, NULL, 'Firefox 62.0 | Linux', '223.255.225.234', 'auth/logout/', 'http://git.babastudio.com:8787/deplaza.id/auth/logout.html', 'Logout', '2018-09-10 09:57:44'),
(28, 0, NULL, 'Firefox 62.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-09-10 14:43:33'),
(29, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-09-10 14:44:04'),
(30, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '223.255.229.76', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-11 07:54:05'),
(31, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-09-11 07:57:11'),
(32, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '120.188.64.210', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-11 13:27:56'),
(33, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '119.110.66.180', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-11 22:51:42'),
(34, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-09-13 08:08:33'),
(35, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://git.babastudio.com:8787/deplaza.id/auth/login.html', 'Login', '2018-09-14 01:29:02'),
(36, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-09-14 01:31:08'),
(37, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/deplaza.id/auth/login.html', 'Login', '2018-09-15 03:23:50'),
(38, 0, NULL, 'Firefox 62.0 | Linux', '127.0.0.1', 'auth/logout/', 'http://localhost/deplaza.id/auth/logout.html', 'Logout', '2018-09-15 03:25:56'),
(39, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://128.199.192.217/api/auth/login.html', 'Login', '2018-09-15 20:27:27'),
(40, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://deplaza.id/api/auth/login.html', 'Login', '2018-09-16 20:53:09'),
(41, 0, NULL, 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/logout/', 'http://deplaza.id/api/auth/logout.html', 'Logout', '2018-09-17 08:16:52'),
(42, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://deplaza.id/api/auth/login.html', 'Login', '2018-09-17 08:17:02'),
(43, 0, NULL, 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/logout/', 'http://deplaza.id/api/auth/logout.html', 'Logout', '2018-09-17 08:17:08'),
(44, 1, 'admin@admin.com', 'Opera 55.0.2994.61 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-18 07:26:02'),
(45, 0, NULL, 'Opera 55.0.2994.61 | Linux', '158.140.185.62', 'auth/logout/', 'http://api.deplaza.id/auth/logout.html', 'Logout', '2018-09-18 08:09:17'),
(46, 1, 'admin@admin.com', 'Opera 55.0.2994.61 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-18 10:12:22'),
(47, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-18 21:59:02'),
(48, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-19 09:32:33'),
(49, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-20 22:29:23'),
(50, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-21 13:35:41'),
(51, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-21 19:39:51'),
(52, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-22 11:27:11'),
(53, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-22 14:07:33'),
(54, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '116.206.8.24', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-22 14:35:13'),
(55, 0, NULL, 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/logout/', 'http://api.deplaza.id/auth/logout.html', 'Logout', '2018-09-22 14:35:43'),
(56, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-23 19:14:52'),
(57, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-24 08:25:00'),
(58, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-24 12:32:08'),
(59, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-09-25 07:41:49'),
(60, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-02 20:53:26'),
(61, 0, NULL, 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/logout/', 'http://api.deplaza.id/auth/logout.html', 'Logout', '2018-10-02 20:56:03'),
(62, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '182.0.167.240', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-02 21:08:01'),
(63, 1, 'admin@admin.com', 'Opera 55.0.2994.61 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-03 15:59:26'),
(64, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-04 15:51:33'),
(65, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-05 06:17:03'),
(66, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-05 14:29:57'),
(67, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-05 17:38:40'),
(68, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-05 19:54:42'),
(69, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-06 08:43:15'),
(70, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-06 13:24:19'),
(71, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-08 09:33:20'),
(72, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-09 11:29:49'),
(73, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '116.206.15.47', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-13 04:59:07'),
(74, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '114.142.173.44', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-13 19:48:16'),
(75, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-13 22:47:56'),
(76, 1, 'admin@admin.com', 'Chrome 69.0.3497.100 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-14 22:12:25'),
(77, 1, 'admin@admin.com', 'Chrome 69.0.3497.100 | Windows 7', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-14 22:34:29'),
(78, 1, 'admin@admin.com', 'Firefox 62.0 | Linux', '116.206.15.10', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-15 04:16:14'),
(79, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-15 10:02:42'),
(80, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-16 16:14:25'),
(81, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-20 12:56:56'),
(82, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-22 14:11:25'),
(83, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-24 14:24:12'),
(84, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-24 22:42:34'),
(85, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-24 22:56:25'),
(86, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-25 03:58:48'),
(87, 1, 'admin@admin.com', 'Firefox 62.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-25 07:06:33'),
(88, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-25 09:51:25'),
(89, 1, 'admin@admin.com', 'Chrome 70.0.3538.67 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-25 11:38:18'),
(90, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-25 13:29:37'),
(91, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-25 18:52:19'),
(92, 0, NULL, 'Firefox 60.0 | Linux', '158.140.185.62', 'auth/logout/', 'http://api.deplaza.id/auth/logout.html', 'Logout', '2018-10-25 18:55:02'),
(93, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-26 04:48:49'),
(94, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-26 10:37:32'),
(95, 1, 'admin@admin.com', 'Chrome 69.0.3497.100 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-29 09:20:16'),
(96, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-29 12:19:15'),
(97, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-30 12:54:12'),
(98, 1, 'admin@admin.com', 'Safari 601.1 | iOS', '114.124.232.203', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-30 12:56:43'),
(99, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 7', '120.188.92.47', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-30 13:07:46'),
(100, 1, 'admin@admin.com', 'Firefox 63.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-30 17:28:59'),
(101, 1, 'admin@admin.com', 'Firefox 63.0 | Windows 10', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-31 08:34:41'),
(102, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-10-31 11:02:26'),
(103, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 7', '114.4.214.27', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-11-01 07:58:07'),
(104, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-11-01 14:48:22'),
(105, 0, NULL, 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/logout/', 'http://api.deplaza.id/v2/auth/logout.html', 'Logout', '2018-11-01 17:14:06'),
(106, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-11-01 22:12:46'),
(107, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '158.140.185.62', 'auth/login/', 'http://api.deplaza.id/auth/login.html', 'Login', '2018-11-02 13:07:19'),
(108, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-05 14:33:48'),
(109, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '158.140.185.62', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-06 09:56:31'),
(110, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-07 09:05:19'),
(111, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 7', '120.188.32.68', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-07 16:36:01'),
(112, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 7', '146.196.106.125', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-08 13:05:41'),
(113, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 7', '146.196.106.125', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-08 18:03:57'),
(114, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '116.206.15.77', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-09 09:31:54'),
(115, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 7', '114.124.145.114', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-09 09:54:24'),
(116, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '116.206.15.77', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-09 10:03:12'),
(117, 0, NULL, 'Firefox 60.0 | Linux', '116.206.15.77', 'auth/logout/', 'https://api.deplaza.id/auth/logout.html', 'Logout', '2018-11-09 10:03:17'),
(118, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-09 10:10:27'),
(119, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-09 10:11:10'),
(120, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-10 15:16:42'),
(121, 1, 'admin@admin.com', 'Chrome 70.0.3538.77 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-11 00:58:16'),
(122, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '180.245.233.27', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-12 15:23:13'),
(123, 1, 'admin@admin.com', 'Firefox 63.0 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-13 11:30:24'),
(124, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '119.110.66.180', 'auth/login/', 'https://api.deplaza.id/v2/auth/login.html', 'Login', '2018-11-14 12:22:08'),
(125, 0, NULL, 'Firefox 60.0 | Linux', '119.110.66.180', 'auth/logout/', 'https://api.deplaza.id/v2/auth/logout.html', 'Logout', '2018-11-14 17:14:36'),
(126, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-15 11:01:28'),
(127, 1, 'admin@admin.com', 'Chrome 70.0.3538.102 | Windows 7', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-15 17:11:46'),
(128, 1, 'admin@admin.com', 'Chrome 70.0.3538.102 | Windows 7', '114.4.83.177', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-16 15:32:28'),
(129, 0, NULL, 'Chrome 70.0.3538.102 | Windows 7', '114.4.83.177', 'auth/logout/', 'https://api.deplaza.id/auth/logout.html', 'Logout', '2018-11-16 15:38:50'),
(130, 1, 'admin@admin.com', 'Chrome 70.0.3538.102 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-19 12:13:22'),
(131, 1, 'admin@admin.com', 'Chrome 70.0.3538.102 | Windows 7', '114.124.231.197', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-19 12:38:20'),
(132, 0, NULL, 'Chrome 70.0.3538.102 | Windows 7', '114.124.231.197', 'auth/logout/', 'https://api.deplaza.id/auth/logout.html', 'Logout', '2018-11-19 12:38:56'),
(133, 1, 'admin@admin.com', 'Firefox 60.0 | Linux', '114.142.172.32', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-19 15:10:25'),
(134, 1, 'admin@admin.com', 'Chrome 70.0.3538.102 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-21 08:36:30'),
(135, 0, NULL, 'Chrome 70.0.3538.102 | Windows 10', '66.96.238.89', 'auth/logout/', 'https://api.deplaza.id/auth/logout.html', 'Logout', '2018-11-21 08:50:46'),
(136, 1, 'admin@admin.com', 'Chrome 70.0.3538.102 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-21 09:00:47'),
(137, 1, 'admin@admin.com', 'Chrome 70.0.3538.102 | Windows 7', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-21 09:37:11'),
(138, 1, 'admin@admin.com', 'Chrome 70.0.3538.102 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-21 12:00:11'),
(139, 1, 'admin@admin.com', 'Firefox 63.0 | Windows 10', '66.96.238.89', 'auth/login/', 'https://api.deplaza.id/auth/login.html', 'Login', '2018-11-21 13:06:12'),
(141, 1, 'admin@admin.com', 'Firefox 66.0 | Linux', '127.0.0.1', 'auth/login/', 'http://localhost/sitebite/auth/login.html', 'Login', '2019-01-28 05:11:19');

-- --------------------------------------------------------

--
-- Table structure for table `email_logs`
--

CREATE TABLE `email_logs` (
  `email_logs_id` int(11) NOT NULL,
  `to_email` varchar(255) DEFAULT NULL,
  `from_email` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `email_result` text,
  `user_agent` varchar(255) DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  `timestamp_create` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `role` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `role`) VALUES
(1, 'admin', 'Administrator', ''),
(2, 'members', 'General User', '');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `modules` varchar(100) NOT NULL,
  `class` varchar(100) NOT NULL,
  `link` varchar(50) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL,
  `is_arrage` tinyint(3) NOT NULL,
  `is_parent` int(1) NOT NULL,
  `type` enum('backend','frontend') NOT NULL DEFAULT 'backend'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `modules`, `class`, `link`, `icon`, `image`, `is_active`, `is_arrage`, `is_parent`, `type`) VALUES
(15, 'menu', '', 'menu', 'menu', 'fa fa-list-alt', '', 1, 127, 36, 'backend'),
(36, 'System', '', '', '#', 'fa fa-cogs', '', 1, 100, 0, 'backend'),
(37, 'users', '', 'auth', 'auth', 'fa fa-users', '', 1, 2, 36, 'backend'),
(38, 'groups', '', 'groups', 'groups', 'fa fa-folder', '', 1, 1, 36, 'backend'),
(63, 'Merchant', 'users_merchnat', '', '#', 'fa fa-home', '', 1, 1, 0, 'backend'),
(64, 'Users Deplaza', 'users_deplaza', '', 'users_deplaza/member', 'fa fa-user', '', 1, 1, 63, 'backend'),
(65, 'Merchant Deplaza', 'users_deplaza', '', 'users_deplaza/merchant', 'fa fa-users', '', 1, 2, 63, 'backend'),
(66, 'product', 'product', '', '#', 'fa fa-table', '', 1, 2, 0, 'backend'),
(67, 'kategori produk', 'product', '', '#', 'fa fa-table', '', 1, 1, 66, 'backend'),
(68, 'daftar produk', 'product', '', '#', 'fa fa-table', '', 1, 2, 66, 'backend'),
(69, 'produk setting', 'product', 'config', 'product/config', 'fa fa-table', '', 1, 127, 66, 'backend'),
(70, 'shipping', 'shipping', '', '#', 'fa fa-table', '', 1, 10, 0, 'backend'),
(71, 'Kurir', 'shipping', 'courier', 'shipping/courier', 'fa fa-table', '', 1, 1, 70, 'backend'),
(72, 'Report', 'transaction', 'transaction', '#', 'fa fa-file', '', 1, 4, 0, 'backend'),
(73, 'Laporan Transaksi', 'transaction', 'transaction', 'transaction', 'fa fa-money', '', 1, 1, 72, 'backend'),
(74, 'Transaksi Baru', 'transaction', 'transaction', 'transaction/index/new', 'fa fa-money', '', 1, 5, 72, 'backend'),
(75, 'Sedang dalam proses', 'transaction', 'transaction', 'transaction/index/process', 'fa fa-table', '', 1, 6, 72, 'backend'),
(76, 'Dalam Pengiriman', 'transaction', 'transaction', 'transaction/index/shipping', 'fa fa-table', '', 1, 7, 72, 'backend'),
(77, 'Konfirmasi Penerimaan', 'transaction', 'transaction', 'transaction/index/confirm', 'fa fa-table', '', 1, 8, 72, 'backend'),
(78, 'Transaksi Selesai', 'transaction', 'transaction', 'transaction/index/finish', 'fa fa-table', '', 1, 9, 72, 'backend'),
(79, 'hosting', 'host', '', '#', 'fa fa-globe', '', 1, 1, 0, 'backend'),
(80, 'Paket Hosting', 'host', 'host_packet', 'host/host_packet', 'fa fa-globe', '', 1, 1, 79, 'backend'),
(81, 'register paket', 'host', '', 'host/host_register', 'fa fa-globe', '', 1, 2, 79, 'backend'),
(82, 'konfirmasi', 'host', '', 'host/host_confirm', 'fa fa-globe', '', 1, 3, 79, 'backend'),
(83, 'rekening', 'rekening', '', '#', 'fa fa-money', '', 1, 10, 0, 'backend'),
(84, 'master bank', 'rekening', 'bank', 'rekening/bank', 'fa fa-building', '', 1, 1, 83, 'backend'),
(85, 'Rekening Merchant', 'rekening', 'rekening_merchnat', 'rekening/rekening_merchant', 'fa fa-money', '', 1, 2, 83, 'backend'),
(86, 'Pernarikan Dana', 'rekening', 'rekening_penarikan', 'rekening/rekening_penarikan', 'fa fa-money', '', 1, 3, 83, 'backend'),
(88, 'pembayaran', 'payment', 'metode_pembayaran', '#', 'fa fa-money', '', 1, 11, 0, 'backend'),
(89, 'metode pembayaran', 'payment', 'metode_pembayaran', 'payment/metode_pembayaran', 'fa fa-university', '', 1, 1, 88, 'backend'),
(90, 'pembayaran via', 'payment', 'pembayaran', 'payment/pembayaran', 'fa fa-money', '', 1, 2, 88, 'backend');

-- --------------------------------------------------------

--
-- Table structure for table `merchants`
--

CREATE TABLE `merchants` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `is_active` char(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merchants`
--

INSERT INTO `merchants` (`id`, `name`, `domain`, `theme_id`, `is_active`, `created_at`, `updated_at`) VALUES
(2, 'localhost', 'localhost', 5, '1', '2019-02-02 10:12:18', '2019-02-02 03:35:28'),
(3, 'local 2', '127.0.0.1', 4, '1', '2019-02-02 10:12:32', '2019-02-02 04:01:27');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `notification_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `type` enum('bc','pm') NOT NULL,
  `is_reply` int(11) DEFAULT NULL,
  `metadata` text,
  `message` varchar(255) NOT NULL,
  `is_read` enum('N','Y') DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`notification_id`, `sender_id`, `receiver_id`, `type`, `is_reply`, `metadata`, `message`, `is_read`, `created`) VALUES
(1, NULL, NULL, 'bc', NULL, '{\"masked_card\":\"411111-1111\",\"approval_code\":\"000000\",\"bank\":\"mandiri\",\"eci\":\"02\",\"transaction_time\":\"2014-04-07 16:22:36\",\"gross_amount\":\"2700\",\"order_id\":\"2014040745\",\"payment_type\":\"credit_card\",\"signature_key\":\"dff6dd7437c1ceb49c12add04dcce3a647c9065cc7d389df2c712d47241c79a76a3a265e7e55ab8e905f74aacd738d339317d8c09167654c542625cb7a260ad2\",\"status_code\":\"200\",\"transaction_id\":\"826acc53-14e0-4ae7-95e2-845bf0311579\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"status_message\":\"Veritrans payment notification\"}', 'tes_notif', NULL, '2018-09-07 03:57:29'),
(2, NULL, NULL, 'bc', NULL, '{\"masked_card\":\"411111-1111\",\"approval_code\":\"000000\",\"bank\":\"mandiri\",\"eci\":\"02\",\"transaction_time\":\"2014-04-07 16:22:36\",\"gross_amount\":\"2700\",\"order_id\":\"2014040745\",\"payment_type\":\"credit_card\",\"signature_key\":\"dff6dd7437c1ceb49c12add04dcce3a647c9065cc7d389df2c712d47241c79a76a3a265e7e55ab8e905f74aacd738d339317d8c09167654c542625cb7a260ad2\",\"status_code\":\"200\",\"transaction_id\":\"826acc53-14e0-4ae7-95e2-845bf0311579\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"status_message\":\"Veritrans payment notification\"}', 'tes_notif', NULL, '2018-09-07 07:04:00'),
(3, NULL, NULL, 'bc', NULL, '{\"masked_card\":\"411111-1111\",\"approval_code\":\"000000\",\"bank\":\"mandiri\",\"eci\":\"02\",\"transaction_time\":\"2014-04-07 16:22:36\",\"gross_amount\":\"2700\",\"order_id\":\"2014040745\",\"payment_type\":\"credit_card\",\"signature_key\":\"dff6dd7437c1ceb49c12add04dcce3a647c9065cc7d389df2c712d47241c79a76a3a265e7e55ab8e905f74aacd738d339317d8c09167654c542625cb7a260ad2\",\"status_code\":\"200\",\"transaction_id\":\"826acc53-14e0-4ae7-95e2-845bf0311579\",\"transaction_status\":\"capture\",\"fraud_status\":\"accept\",\"status_message\":\"Veritrans payment notification\"}', 'tes_notif', NULL, '2018-09-07 07:36:31');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `environment` char(1) NOT NULL DEFAULT '0',
  `site_name` varchar(255) DEFAULT NULL,
  `site_logo` varchar(255) DEFAULT NULL,
  `og_image` varchar(255) DEFAULT NULL,
  `fbapp_id` varchar(255) DEFAULT NULL,
  `site_footer` text,
  `default_email` varchar(255) DEFAULT NULL,
  `keywords` text,
  `themes_config` varchar(255) DEFAULT NULL,
  `admin_lang` varchar(255) DEFAULT NULL,
  `additional_js` text,
  `additional_metatag` text,
  `googlecapt_active` int(11) DEFAULT NULL,
  `googlecapt_sitekey` varchar(255) DEFAULT NULL,
  `googlecapt_secretkey` varchar(255) DEFAULT NULL,
  `pagecache_time` int(3) DEFAULT NULL,
  `email_protocal` varchar(20) DEFAULT NULL,
  `smtp_host` varchar(255) DEFAULT NULL,
  `smtp_user` varchar(255) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(5) DEFAULT NULL,
  `sendmail_path` varchar(255) DEFAULT NULL,
  `member_confirm_enable` int(11) DEFAULT NULL,
  `member_close_regist` int(11) DEFAULT NULL,
  `gmaps_key` varchar(255) DEFAULT NULL,
  `gmaps_lat` varchar(100) DEFAULT NULL,
  `gmaps_lng` varchar(100) DEFAULT NULL,
  `ga_client_id` varchar(255) DEFAULT NULL,
  `ga_view_id` varchar(255) DEFAULT NULL,
  `gsearch_active` int(11) DEFAULT NULL,
  `gsearch_cxid` varchar(255) DEFAULT NULL,
  `maintenance_active` int(11) DEFAULT NULL,
  `html_optimize_disable` int(11) DEFAULT NULL,
  `adobe_cc_apikey` varchar(255) DEFAULT NULL,
  `facebook_page_id` varchar(255) DEFAULT NULL,
  `assets_static_active` int(11) DEFAULT NULL,
  `assets_static_domain` varchar(255) DEFAULT NULL,
  `fb_messenger` int(11) DEFAULT NULL,
  `max_cuti` int(2) NOT NULL,
  `timestamp_update` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `environment`, `site_name`, `site_logo`, `og_image`, `fbapp_id`, `site_footer`, `default_email`, `keywords`, `themes_config`, `admin_lang`, `additional_js`, `additional_metatag`, `googlecapt_active`, `googlecapt_sitekey`, `googlecapt_secretkey`, `pagecache_time`, `email_protocal`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `sendmail_path`, `member_confirm_enable`, `member_close_regist`, `gmaps_key`, `gmaps_lat`, `gmaps_lng`, `ga_client_id`, `ga_view_id`, `gsearch_active`, `gsearch_cxid`, `maintenance_active`, `html_optimize_disable`, `adobe_cc_apikey`, `facebook_page_id`, `assets_static_active`, `assets_static_domain`, `fb_messenger`, `max_cuti`, `timestamp_update`) VALUES
(1, '1', 'shintackeror', '', '', '1226246327478391', '&copy; %Y% shintackeror', 'shintackeror404@gmail.com', 'Web programming, web design, pemrograman web, desain web, desain grafis, pemrograman, program, koding, coding, html, css, php, javascript, coreldraw, sublime, atom, visual code, dreamweaver', 'newfrontend', 'english', '', '', NULL, '6Lf5Q0IUAAAAAMULY4zZPZGm9FA6A7uDm0-tPjym', '6Lf5Q0IUAAAAALP_d9JgjsgreZdMpvUzLGNxi3Mx', 60, 'smtp', 'ssl://smtp.gmail.com', 'shintackeror404@gmail.com', 'kmzway87AA', '465', '', NULL, NULL, 'AIzaSyBqrMqEOR8sMFiPut6ZwJJScuM1mvF6Re0', '-28.621975', '150.689082', '731289890901-9f0bhdh6am9987oleq41ur7ak4kpsedq.apps.googleusercontent.com', 'ga:172842810', NULL, '016007005639982460691:pe9hwht07v4', NULL, NULL, '', '', NULL, '', NULL, 15, '2018-04-04 16:30:25');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `path` varchar(255) NOT NULL,
  `is_default` char(1) NOT NULL DEFAULT '0',
  `is_active` char(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `path`, `is_default`, `is_active`, `created_at`, `updated_at`) VALUES
(4, 'material', 'material', '0', '1', '2019-02-02 10:08:25', '2019-02-02 03:42:23'),
(5, 'sitebeat', 'sitebeat', '1', '1', '2019-02-02 10:08:58', '2019-02-02 03:42:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, 'oaWTT3j1bFBg0MDH30LO3e', 1268889823, 1548652278, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(86, '127.0.0.1', NULL, '$2y$08$4cMz7RW851JvJfTTPzUUJO5OFZg8t0n2kBzAs8LFtMn6MdIPUBOuS', NULL, 'insan.mm@yahoo.com', '031fbe857d65f92857a8b734b0ae192e7db327d0', NULL, NULL, NULL, 1548652514, NULL, 0, 'tes insan', 'okook', NULL, '123123');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(19, 1, 1),
(68, 53, 2),
(70, 54, 7),
(75, 55, 6),
(76, 56, 5),
(77, 57, 4),
(78, 58, 3),
(80, 59, 8),
(98, 81, 2),
(99, 82, 2),
(100, 83, 2),
(101, 84, 2),
(103, 85, 9),
(104, 86, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups_menu`
--

CREATE TABLE `user_groups_menu` (
  `akses_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `lihat` int(11) NOT NULL,
  `daftar` int(11) NOT NULL,
  `tambah` int(11) NOT NULL,
  `ubah` int(11) NOT NULL,
  `hapus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_groups_menu`
--

INSERT INTO `user_groups_menu` (`akses_id`, `group_id`, `menu_id`, `lihat`, `daftar`, `tambah`, `ubah`, `hapus`) VALUES
(229, 1, 72, 72, 72, 72, 72, 72),
(230, 1, 36, 36, 36, 36, 36, 36),
(231, 1, 70, 70, 70, 70, 70, 70),
(232, 1, 88, 88, 88, 88, 88, 88),
(233, 1, 63, 63, 63, 63, 63, 63),
(234, 1, 83, 83, 83, 83, 83, 83),
(235, 1, 79, 79, 79, 79, 79, 79),
(236, 1, 66, 66, 66, 66, 66, 66),
(237, 1, 15, 15, 15, 15, 15, 15),
(238, 1, 38, 38, 38, 38, 38, 38),
(239, 1, 37, 37, 37, 37, 37, 37),
(240, 1, 64, 64, 64, 64, 64, 64),
(241, 1, 65, 65, 65, 65, 65, 65),
(242, 1, 69, 69, 69, 69, 69, 69),
(243, 1, 68, 68, 68, 68, 68, 68),
(244, 1, 67, 67, 67, 67, 67, 67),
(245, 1, 71, 71, 71, 71, 71, 71),
(246, 1, 78, 78, 78, 78, 78, 78),
(247, 1, 77, 77, 77, 77, 77, 77),
(248, 1, 76, 76, 76, 76, 76, 76),
(249, 1, 75, 75, 75, 75, 75, 75),
(250, 1, 74, 74, 74, 74, 74, 74),
(251, 1, 73, 73, 73, 73, 73, 73),
(252, 1, 80, 80, 80, 80, 80, 80),
(253, 1, 81, 81, 81, 81, 81, 81),
(254, 1, 82, 82, 82, 82, 82, 82),
(255, 1, 86, 86, 86, 86, 86, 86),
(256, 1, 85, 85, 85, 85, 85, 85),
(257, 1, 84, 84, 84, 84, 84, 84),
(258, 1, 89, 89, 89, 89, 89, 89),
(259, 1, 90, 90, 90, 90, 90, 90);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions_logs`
--
ALTER TABLE `actions_logs`
  ADD PRIMARY KEY (`actions_logs_id`);

--
-- Indexes for table `email_logs`
--
ALTER TABLE `email_logs`
  ADD PRIMARY KEY (`email_logs_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchants`
--
ALTER TABLE `merchants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `user_groups_menu`
--
ALTER TABLE `user_groups_menu`
  ADD PRIMARY KEY (`akses_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions_logs`
--
ALTER TABLE `actions_logs`
  MODIFY `actions_logs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `email_logs`
--
ALTER TABLE `email_logs`
  MODIFY `email_logs_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `merchants`
--
ALTER TABLE `merchants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `user_groups_menu`
--
ALTER TABLE `user_groups_menu`
  MODIFY `akses_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
