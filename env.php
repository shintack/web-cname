<?php
defined('FCPATH') OR exit('No direct script access allowed');

/* Database Host */
if ($_SERVER['SERVER_NAME']=="localhost" || $_SERVER['SERVER_NAME']=="127.0.0.1") {
	//db marketing system
	$hostname = 'localhost';
	$username = 'root';
	$password = '';
	$database = 'web_cname';
}else{
	$hostname = 'localhost';
	$username = 'seller1d_db';
	$password = 'b4b4studio*';
	$database = 'seller1d_db';
}

define('DB_HOST', $hostname);

/* Database Username */
define('DB_USERNAME', $username);

/* Database Password */
define('DB_PASS', $password);

/* Database Name */
define('DB_NAME', $database);

/* Base URL */
$_base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$_base_url .= "://".$_SERVER['HTTP_HOST'];
$_base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
define('BASE_URL', $_base_url);

/* Email Domain */
define('EMAIL_DOMAIN', 'babastudio.com');

/* Time Zone */
define('TIME_ZONE', 'Asia/Jakarta');
