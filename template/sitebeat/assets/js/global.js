$(document).ready(function(){

	// jQuery.scrollSpeed(200, 900);

	AOS.init({
		duration: 1000
	});


	getScrollPosition();

	$(window).scroll(function(){
		getScrollPosition();
	});


	$('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });

	 $('input').blur(function() {

	    // check if the input has any value (if we've typed into it)
	    if ($(this).val())
	      $(this).addClass('used');
	    else
	      $(this).removeClass('used');
	  });

	var swiper = new Swiper('.swiper-testimonial', {
        slidesPerView: 3,
        spaceBetween: 40,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        breakpoints: {
            480: {
              slidesPerView: 1,
            },
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('#atm').hide();
    $('#ebanking').hide();
    var atm_on = false;
    $('#atm-trigger').click(function(){
        if (atm_on) {
            $('#atm').slideUp();
            $('#atm-trigger i').removeClass('on');
            atm_on = false;
        }else{
            $('#atm').slideDown();
            $('#atm-trigger i').addClass('on');
            atm_on = true;
        }
    })
    var ebanking_on = false;
    $('#ebanking-trigger').click(function(){
        if (ebanking_on) {
            $('#ebanking').slideUp();
            $('#ebanking-trigger i').removeClass('on');
            ebanking_on = false;
        }else{
            $('#ebanking').slideDown();
            $('#ebanking-trigger i').addClass('on');
            ebanking_on = true;
        }
    })


});

function getScrollPosition(){
	var s = $(window).scrollTop();
    if (s < $('#navbar').height()) {
        $('#backtotop').addClass('btt-hide');
        $('#navbar').addClass('intop');
        if (!$('#navbar').hasClass("not-home")) {
            $('#navbar').removeClass('bg-blue-gradient');
        }
    }else{
        $('#backtotop').removeClass('btt-hide');
        $('#navbar').removeClass('intop');
        if (!$('#navbar').hasClass("not-home")) {
            $('#navbar').addClass('bg-blue-gradient');
        }
    };
}

// Custom scrolling speed with jQuery
// Source: github.com/ByNathan/jQuery.scrollSpeed
// Version: 1.0.2

(function($) {
    jQuery.scrollSpeed = function(step, speed, easing) {
        var $document = $(document),
            $window = $(window),
            $body = $('html, body'),
            option = easing || 'default',
            root = 0,
            scroll = false,
            scrollY,
            scrollX,
            view;
        if (window.navigator.msPointerEnabled)
            return false;
        $window.on('mousewheel DOMMouseScroll', function(e) {
            var deltaY = e.originalEvent.wheelDeltaY,
                detail = e.originalEvent.detail;
                scrollY = $document.height() > $window.height();
                scrollX = $document.width() > $window.width();
                scroll = true;
            if (scrollY) {
                view = $window.height();

                if (deltaY < 0 || detail > 0)
                    root = (root + view) >= $document.height() ? root : root += step;
                if (deltaY > 0 || detail < 0)
                    root = root <= 0 ? 0 : root -= step;
                $body.stop().animate({
                    scrollTop: root
                }, speed, option, function() {
                    scroll = false;
                });
            }
            if (scrollX) {

                view = $window.width();

                if (deltaY < 0 || detail > 0)
                    root = (root + view) >= $document.width() ? root : root += step;
                if (deltaY > 0 || detail < 0)
                    root = root <= 0 ? 0 : root -= step;
                $body.stop().animate({
                    scrollLeft: root
                }, speed, option, function() {
                    scroll = false;
                });
            }
            return false;
        }).on('scroll', function() {
            if (scrollY && !scroll) root = $window.scrollTop();
            if (scrollX && !scroll) root = $window.scrollLeft();

        }).on('resize', function() {
            if (scrollY && !scroll) view = $window.height();
            if (scrollX && !scroll) view = $window.width();

        });
    };
    jQuery.easing.default = function (x,t,b,c,d) {
        return -c * ((t=t/d-1)*t*t*t - 1) + b;
    };
})(jQuery);
