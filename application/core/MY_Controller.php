<?php //(defined('BASEPATH')) OR exit('No direct script access allowed');

//require APPPATH."third_party/MX/Controller.php";
class MY_Controller extends CI_Controller
{
	public $path_assets;

	public $_mod;
    public $_tabel;
    public $_pk;
    public $_field;
    public $_template;
    public $_view;
    public $_notification = array();
    public $_theme = 'deplaza';
    public $_assets;
	public $token;
	public $data;

    public $_base_api_url  = 'https://api.deplaza.id/';
    public $_serp_url      = 'v1/serp?';
    public $_backlink_url  = 'v2/backlink?';
    public $_products_url  = 'v1/marketplace/';
    public $_keyword_url    = 'v3/keyword?';

    function __construct()
    {
        parent::__construct();
        $this->_assets = base_url('template/'.$this->_theme);
        $this->load->library('recaptcha');
		$this->load->helper('notification_helper');
		$this->_base_api_url = $_SERVER['SERVER_NAME']=='localhost' ? 'http://localhost/deplaza/' : 'https://api.deplaza.id/';

		// header("Access-Control-Allow-Origin: *");
		// header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
		// header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		if ($this->session->userdata('user_id')!=NULL) {
			$this->_notification = []; //get_notif($this->session->userdata('user_id'));
		}
		$this->cek_merchant();
    }

	public function ingroup()
	{
		if (!in_group([1,3,4,5,6,7,8,9])) {
            redirect(site_url());
        }
	}

    public function load_setting()
    {

    }

	public function cek_merchant()
	{
		$domain = $_SERVER['SERVER_NAME'];
		//set config on session
		// if ( !$this->session->userdata('config') ) {
			$merchant = $this->db
				->select("merchants.domain, merchants.name as web_name, themes.path")
				->join('themes','themes.id = merchants.theme_id')
				->get_where('merchants',[
					'domain'			  => $domain,
					'merchants.is_active' => '1',
				]);
			if ( $merchant->num_rows()>0  ) {
				$this->session->set_userdata([
					'config' => $merchant->row(),
				]);
			}else{
				show_404();
				// redirect('ups404');
			}
		// }else{
			// load config from session
			$config = $this->session->userdata('config');
			$this->_template    = 'layout/'.$config->path.'/template';
			$this->path_assets  = base_url('template/'.$config->path.'/');
		// }
		// vardump($config);
	}

    public function upload_image($name,$field,$path)
    {
        $terupload = FALSE;
        if (!empty($_FILES) && $_FILES[$field]['size'] > 0) {
            $this->load->library('upload');
            $nmfile = $name;//"bukti_".$invoice; //nama file + fungsi time
            $config['upload_path'] = $path; //Folder untuk menyimpan hasil upload
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '1024'; //maksimum besar file 3M
            $config['max_width']  = '5000'; //lebar maksimum 5000 px
            $config['max_height']  = '5000'; //tinggi maksimu 5000 px
            $config['file_name'] = $nmfile; //nama yang terupload nantinya
            $config['overwrite'] = TRUE;
            $tipe_file = substr($_FILES[$field]['type'],6);
            $this->upload->initialize($config);
            $gbr = $this->upload->data();
            if ($this->upload->do_upload($field))
            {
                $gbr = $this->upload->data();
                //dibawah ini merupakan code untuk resize
                /*
                $config2['image_library'] = 'gd2';
                $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config2['new_image'] = $path;//'./uploads/min/'; // folder tempat menyimpan hasil resize
                $config2['maintain_ratio'] = TRUE;
                //ubah ukuran gambar disini
                $config2['width'] = 720; //lebar setelah resize menjadi 100 px
                $config2['height'] = 480; //lebar setelah resize menjadi 100 px
                $this->load->library('image_lib',$config2);
                //pesan yang muncul jika resize error dimasukkan pada session flashdata
                if ( !$this->image_lib->resize()){
                    $this->output->set_output("eror resize");
                    $this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
                }
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Upload gambar berhasil !!</div></div>");
                //$this->output->set_output($config['file_name'].'.'.$tipe_file);
                */
                $nama_file_jadi = $gbr['file_name'];//$config['file_name'];//.'.'.$tipe_file;
                return [
                    'status'  => TRUE,
                    'message' => $nama_file_jadi,
                ];
            }elseif ( ! $this->upload->do_upload($field)){
                $eror = $this->upload->display_errors();
                return [
                    'status'  => FALSE,
                    'message' => json_encode($eror),
                ];
            }
        }else{
            return [
                    'status'  => FALSE,
                    'message' => 'No Image',
                ];
        }
    }

	public function send_mail($from,$subject,$message,$to,$bcc=NULL)
    {
        $config = [
            'charset' => 'utf-8',
            'useragent' => 'Codeigniter',
            'protocol'=> "smtp",
            'mailtype'=> "html",
            'smtp_host'=> "ssl://smtp.gmail.com",//pengaturan smtp
            'smtp_port'=> 465,
            'smtp_timeout'=> 400,
            'smtp_user'=> "babalearningsystem@gmail.com", // isi dengan email kamu
            'smtp_pass'=> "b4b4studio*", // isi dengan password kamu
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'wordwrap' => TRUE,
        ];

        $this->load->library('email');
        $this->email->initialize($config);
        //konfigurasi pengiriman
        $this->email->from($from, 'babastudio');
        $this->email->subject($subject);
        $this->email->message($message);
        $send = FALSE;
        if ($to!=NULL) {
            $this->email->to($to);
            $send = TRUE;
        }elseif(($to&&$bcc)!=NULL){
            $this->email->to($to);
            $this->email->bcc($bcc);
            $send = TRUE;
        }elseif($to==NULL && $bcc!=NULL){
            $this->email->bcc($bcc);
            $send = TRUE;
        }
		return $this->email->send();
    }

     /**
    *     @param string $token
    *     @return [int if allow access FALSE is not alowed]
    */
    //privilage
    public function get_akses()
    {
		$this->token = $this->uri->segment(3);
		if ($this->input->post('token')!=NULL) {
			$this->token = $this->input->post('token');
		}
        $this->db = $this->load->database('ecommerce_db', TRUE);
      	$cek = $this->db->get_where('shop_merchant',['token'=>$this->token])->row();
      	return $cek?[
              'merchant_id'    => (int)$cek->shop_merchant_id,
              'merchant_type'  => $cek->type,
          ]:FALSE;
    }

    public function access_denied()
    {
      $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode([
          'status'  => FALSE,
          'message' => 'Access Denied',
          'data'    => NULL,
      ]));
    }
    //end privilage

}
