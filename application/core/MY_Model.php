<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Model extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
	}

	/**
	 * get one ror
	 * @param string $tabel nama tabel
	 * @param string $field_order
	 * @param string $order
	 * @return array object
	 */
    function get_all($tabel,$field_order,$order='DESC')
    {
        $this->db->order_by($field_order, $order);
        return $this->db->get($tabel)->result();
    }

	/**
	 * get one ror
	 * @param string $tabel nama tabel
	 * @param array $where
	 * @return array object
	 */
	function get_row($tabel,$where)
	{
		if ($tabel!==NULL) {
			if (is_array($where)) {
				foreach($where as $key => $value){
					$this->db->where($key,$value);
				}
			}
			return $this->db->get($tabel)->row();
		}
    }

    function get_where_result($tabel,$where,$join=NULL,$select=NULL)
	{
		if ($tabel!==NULL) {
            if ($select!=NULL) {
                $this->db->select($select);
            }
            if (is_array($join)) {
                foreach($join as $tb_join => $field){
                    $this->db->join($tb_join,$field);
                }
            }
			if (is_array($where)) {
				foreach($where as $key => $value){
					$this->db->where($key,$value);
				}
			}
			return $this->db->get($tabel)->result();
		}
    }

    function activate($tabel,$where)
    {
        $data = $this->get_row($tabel,$where);
        $pk = $id = NULL;
        foreach($where as $key => $value){
            $pk = $key; $id = $value;
        }
        if ($data->is_active=='Y') {
            $update = [ 'is_active' => 'N' ];
        }else{
            $update = [ 'is_active' => 'Y' ];
        }
        return $this->update($tabel,$pk,$id,$update);
    }

	// get data with limit and search
    function get_limit_data($tabel,$pk,$limit = 0, $start = 0, $q = NULL, $fields = NULL, $order = 'DESC', $join = NULL, $type_join = NULL) {
        $this->db->order_by($pk, $order);
        if (is_array($fields)&&$q!=NULL) {
            foreach($fields as $key => $value){
                if ($key[0]) {
                    $this->db->like($value, $q);
                }
                $this->db->or_like($value, $q);
            }
        }
        if (is_array($join)) {
            foreach($join as $tb_join => $field){
                if ($type_join != NULL) {
                   $this->db->join($tabel,$field,$type_join);
                }else{
                    $this->db->join($tb_join,$field);
                }
            }
        }
        if (is_numeric($limit)&&$limit>0) {
            return $this->db->limit($limit, $start)->get($tabel)->result();
        }else{
            return $this->db->get($tabel)->num_rows();
        }
    }

    // insert data
    function insert($tabel,$data)
    {
        $this->db->insert($tabel, $data);
        return $this->db->insert_id();
    }

    // update data
    function update($tabel,$pk,$id, $data)
    {
        $this->db->where($pk, $id);
        return $this->db->update($tabel, $data);
    }

    // delete data
    function delete($tabel,$pk,$id)
    {
        $this->db->where($pk, $id);
        return $this->db->delete($tabel);
    }

    function modal_delete($input,$id)
    {
        $modal = '
        <!-- Modal -->
        <div id="myModalDelete'.$id.'" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header bg-red">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Peringatan!!!</h4>
              </div>
              <div class="modal-body">
                <h1>Yakin akan menghapus data ini? </h1>
              </div>
              <div class="modal-footer">
                '.form_open($input["delete"]).'
                <input type="hidden" name="id" value="'.$id.'" />
                <button type="submit" class="btn btn-danger">Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                '.form_close().'
              </div>
            </div>

          </div>
        </div>
        ';
        return $modal;
    }

    public function upload_image($name,$field,$path)
    {
        $terupload = FALSE;
        if (!empty($_FILES) && $_FILES[$field]['size'] > 0) {
            $this->load->library('upload');
            $nmfile = $name;//"bukti_".$invoice; //nama file + fungsi time
            $config['upload_path'] = $path; //Folder untuk menyimpan hasil upload
            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp|pdf'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '1024'; //maksimum besar file 3M
            $config['max_width']  = '5000'; //lebar maksimum 5000 px
            $config['max_height']  = '5000'; //tinggi maksimu 5000 px
            $config['file_name'] = $nmfile; //nama yang terupload nantinya
            $config['overwrite'] = TRUE;
            $tipe_file = substr($_FILES[$field]['type'],6);
            $this->upload->initialize($config);
            $gbr = $this->upload->data();
            if ($this->upload->do_upload($field))
            {
                $gbr = $this->upload->data();
                //dibawah ini merupakan code untuk resize
                /*
                $config2['image_library'] = 'gd2';
                $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config2['new_image'] = $path;//'./uploads/min/'; // folder tempat menyimpan hasil resize
                $config2['maintain_ratio'] = TRUE;
                //ubah ukuran gambar disini
                $config2['width'] = 720; //lebar setelah resize menjadi 100 px
                $config2['height'] = 480; //lebar setelah resize menjadi 100 px
                $this->load->library('image_lib',$config2);
                //pesan yang muncul jika resize error dimasukkan pada session flashdata
                if ( !$this->image_lib->resize()){
                    $this->output->set_output("eror resize");
                    $this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));
                }
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Upload gambar berhasil !!</div></div>");
                //$this->output->set_output($config['file_name'].'.'.$tipe_file);
                */
                $nama_file_jadi = $gbr['file_name'];//$config['file_name'];//.'.'.$tipe_file;
                return [
                    'status'  => TRUE,
                    'message' => $nama_file_jadi,
                ];
            }elseif ( ! $this->upload->do_upload($field)){
                $eror = $this->upload->display_errors();
                return [
                    'status'  => FALSE,
                    'message' => json_encode($eror),
                ];
            }
        }else{
            return [
                    'status'  => FALSE,
                    'message' => 'No Image',
                ];
        }
    }

	/**
     * cleanEmailFormat
     *
     * Function for clean email format
     *
     * @param	string	$email  for email address input
     * @return	string
     */
    public function cleanEmailFormat($email){
        $search = array('&','/',';','\\','"',"'",'|',' ','{','}');
        $email = str_replace($search, '', trim($email));
        unset($search);
        return $this->security->xss_clean($email);
    }

	/**
     * pwdEncypt
     *
     * Function for encyption the password with password_hash (BCRYPT)
     *
     * @param	string	$password    password
     * @return	string
     */
    public function pwdEncypt($password) {
        $options = array('cost' => 12);
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }


}
