<?php


function go_back(){
    return '
    <script>
        window.history.back();
        location.reload();
    </script>';
}

function is_active($name,$selected){
    return '
    <select class="form-control" name="$name" id="$name">
        <option value="Y" <?=($selected=="Y")?"selected":""?> >Aktif</option>
        <option value="N" <?=($selected=="N")?"selected":""?> >Non Aktif</option>
    </select>
    ';
}


function status($value,$type='aktif')
{
    if ($value==1 || $value=='Y') {
        return ($type=='aktif')?'Aktif':'Ya';
    }else{
        return ($type=='aktif')?'Non Aktif':'Tidak';
    }
}

function menu_name($id){
    $ci = get_instance();
    $data = $ci->db->where('id',$id)->get('menu')->row();
    if ($data!=NULL) {
        return $data->name;
    }
}

function jabatan_name($id){
    $ci = get_instance();
    $data = $ci->db->where('is_parent',$id)->get('division')->result();
    if ($data!=NULL) {
        return $data;
    }
}


function set_sweat($type,$message)
{
    $ci = &get_instance();
    if ($type == 'success') {
        $flash = [
            'title' => 'Success!',
            'icon' => 'success' ,
            'message' => $message,
        ];
    }elseif('warning'){
        $flash = [
            'title' => 'Warning!',
            'icon' => 'warning' ,
            'message' => $message,
        ];
    }elseif('error'){
        $flash = [
            'title' => 'Error!',
            'icon' => 'error' ,
            'message' => $message,
        ];
    }elseif('info'){
        $flash = [
            'title' => 'Info!',
            'icon' => 'info' ,
            'message' => $message,
        ];
    }
    $ci->session->set_flashdata('message', $flash);
}

function cmb_dinamis($name,$table,$field,$pk,$selected,$require=TRUE,$is_active=FALSE,$str_active='Y'){
    $ci = get_instance();
    $wajib = 'required';
    if (!$require==TRUE) { $wajib = ''; }
    $cmb = "<select name='$name' id='$name' class='select2 form-control' ".$wajib." style='width:100%;' > ";
    if ($is_active) {
        $ci->db->where('is_active',$str_active);
    }
    $data = $ci->db->get($table)->result();
    $cmb .= "<option value=''>--Pilih--</option>";
    foreach ($data as $d){
        $cmb .="<option value='".$d->$pk."'";
        $cmb .= $selected==$d->$pk?" selected='selected'":'';
        $cmb .=">".  strtoupper($d->$field)."</option>";
    }
    $cmb .="</select>";
    /*
    $cmb .= '<script>
    $(document).ready(function() {
        $(".select2").select2();
    });
    </script>';
    */
    return $cmb;
}

function cmb_dinamis_multiple($name,$table,$field,$pk,$selected,$require=TRUE,$is_active=FALSE,$not_me=NULL){
    $ci = get_instance();
    $wajib = 'required';
    if (!$require==TRUE) { $wajib = ''; }
    $cmb = '<select name="'.$name.'[]" class="form-control multiple" '.$wajib.' style="width:100%;" multiple="multiple" > ';
    if ($is_active) {
        $ci->db->where('is_active','Y');
    }
    $data = $ci->db->get($table)->result();
    //$cmb .= '<option value="">--Pilih--</option>';
    foreach ($data as $d){
        if ($not_me!=NULL) {
            if($d->$pk!=$not_me){
                $cmb .='<option value="'.$d->$pk.'"';
                $cmb .= $selected==$d->$pk?" selected='selected'":'';
                $cmb .=">".  strtoupper($d->$field)."</option>";
            }
        }else{
            $cmb .='<option value="'.$d->$pk.'"';
            $cmb .= $selected==$d->$pk?" selected='selected'":'';
            $cmb .=">".  strtoupper($d->$field)."</option>";
        }
    }
    $cmb .='</select>';
    return $cmb;
}

function select2_dinamis($name,$table,$field,$placeholder){
    $ci = get_instance();
    $select2 = '<select name="'.$name.'" class="form-control select2 select2-hidden-accessible" multiple=""
               data-placeholder="'.$placeholder.'" style="width: 100%;" tabindex="-1" aria-hidden="true">';
    $data = $ci->db->get($table)->result();
    foreach ($data as $row){
        $select2.= ' <option>'.$row->$field.'</option>';
    }
    $select2 .='</select>';
    return $select2;
}
function datalist_dinamis($name,$table,$field,$value=null){
    $ci = get_instance();
    $string = '<input value="'.$value.'" name="'.$name.'" list="'.$name.'" class="form-control">
    <datalist id="'.$name.'">';
    $data = $ci->db->get($table)->result();
    foreach ($data as $row){
        $string.='<option value="'.$row->$field.'">';
    }
    $string .='</datalist>';
    return $string;
}

function rename_string_is_aktif($string){
        return ($string==('Y'))?'Aktif':'Tidak Aktif';
}


function alert($class,$title,$description){
    return '<div class="alert '.$class.' alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> '.$title.'</h4>
                '.$description.'
              </div>';
}
// untuk chek akses level pada modul peberian akses
function checked_akses($id_user_level,$id_menu){
    $ci = get_instance();
    $ci->db->where('id_user_level',$id_user_level);
    $ci->db->where('id_menu',$id_menu);
    $data = $ci->db->get('tbl_hak_akses');
    if($data->num_rows()>0){
        return "checked='checked'";
    }
}
function autocomplate_json($table,$field){
    $ci = get_instance();
    $ci->db->like($field, $_GET['term']);
    $ci->db->select($field);
    $collections = $ci->db->get($table)->result();
    foreach ($collections as $collection) {
        $return_arr[] = $collection->$field;
    }
    echo json_encode($return_arr);
}


function hak_akses($user_id=NULL){
    $ci = &get_instance();
    $class_name = $ci->router->fetch_class();
    $method = $ci->router->fetch_method();
    $staffdata = $ci->session->userdata('staffdata');
    if ($user_id==NULL) {
        $user_id = $ci->session->userdata('staffdata')->staff_id;
    }
    $ci->db
    ->select('link,menu_id,lihat,daftar,tambah,ubah,hapus')
    ->join('menu','menu.id = user_groups_menu.menu_id')
    ->join('users_groups','users_groups.group_id = user_groups_menu.group_id');
    $ci->db->where('class',$class_name);
    $ci->db->where('user_id',$ci->session->userdata('user_id'));
    $x = $ci->db->get('user_groups_menu')->row();
    //vardump($x);
    if ($x==NULL) {
        set_sweat('warning','Anda tidak punya hak akses kesini!!!!');
        redirect(site_url('home'));
    }
    if ($method == 'read') {
        if ($x->lihat == 0) {
            set_sweat('warning','Anda tidak punya hak akses READ !!!!');
            redirect(site_url('home'));
        }
    }
    if ($method == 'index') {
        if ($x->daftar == 0) {
            set_sweat('warning','Anda tidak punya hak akses LIST !!!!');
            redirect(site_url($class_name.'/read/'.$user_id));
        }
    }
    if ($method == 'create') {
        if ($x->tambah == 0) {
            set_sweat('warning','Anda tidak punya hak akses ADD !!!!');
            redirect(site_url('home'));
        }
    }
    if ($method == 'create_action') {
        if ($x->tambah == 0) {
            set_sweat('warning','Anda tidak punya hak akses ADD !!!!');
            redirect(site_url('home'));
        }
    }
    if ($method == 'update' ) {
        if ($x->ubah == 0) {
            set_sweat('warning','Anda tidak punya hak akses UPDATE !!!!');
            redirect(site_url('home'));
        }
    }
    if ($method == 'update_action' ) {
        if ($x->ubah == 0) {
            set_sweat('warning','Anda tidak punya hak akses UPDATE !!!!');
            redirect(site_url('home'));
        }
    }
    if ($method == 'delete') {
        if ($x->hapus == 0) {
            set_sweat('warning','Anda tidak punya hak akses DELETE !!!!');
            redirect(site_url('home'));
        }
    }
}

function cek_staffdata($user_id){
    $ci = &get_instance();
    if ($ci->session->userdata('staffdata')==NULL) {
        $staffdata = $ci->db->where('user_id',$user_id)->get('staffdata')->row();
        $ci->session->set_userdata('staffdata',$staffdata);
        if ($staffdata==NULL) {
            set_sweat('warning','Silahkan lengkapi data diri terlebih dahulu!!!!');
            redirect(site_url('staffdata/my_profile'));
        }
    }
}

function logs($action=NULL,$note=NULL){
    $ci = &get_instance();
    $ci->load->library('user_agent');
    if ($ci->agent->is_browser()){
        $agent = $ci->agent->browser().' '.$ci->agent->version();
    }elseif ($ci->agent->is_mobile()){
        $agent = $ci->agent->mobile();
    }else{
        $agent = 'Data user gagal di dapatkan';
    }
        $class_name = $ci->router->fetch_class();
    $method = $ci->router->fetch_method();
    $data = [
        'user_id'       => ($ci->session->userdata('user_id'))?$ci->session->userdata('user_id'):0,
        'email_login'   => $ci->session->userdata('email'),
        'user_agent'    => $agent.' | '.$ci->agent->platform(),
        'ip_address'    => $ci->input->ip_address(),
        'url'           => current_url(),
        'actions'       => $action,
        'note'          => $ci->router->fetch_class().'/'.$ci->router->fetch_method().'/'.$note,
        'timestamp'     => date('Y-m-d H:i:s'),
    ];
    $ci->db->insert('actions_logs',$data);
    //vardump($data);
    //$ci->db->
}

/**
*   @param string id_group
*   @param string menu_id
*   @param string akses nama field akses
*   @return bool akses menu
*/
if (!function_exists('get_akses')) {
    function get_akses($group_id,$menu_id,$akses)
    {
        $ci = &get_instance();
        if ($akses!=NULL) {
            $ci->db->select($akses);
        }
        $privilage = $ci->db->where([
            'group_id' => $group_id,
            'menu_id'  => $menu_id,
        ])->get('user_groups_menu')->row();
        if ($privilage!=NULL) {
            if ($privilage->$akses == $menu_id) {
                return TRUE;
            }
        }return FALSE;
    }
}


if (!function_exists('logged_in')) {
    function logged_in(){
        $ci = &get_instance();
        $ci->load->library(array('ion_auth'));
        if (!$ci->ion_auth->logged_in())
        {
            //set_sweat('eror','Silahkan login!!!!');
            redirect('login', 'refresh');
        }
    }
}

if (!function_exists('cek_logged_in')) {
    function cek_logged_in(){
        $ci = &get_instance();
        $ci->load->library(array('ion_auth'));
        return $ci->ion_auth->logged_in();
    }
}

if (!function_exists('is_admin')) {
    function is_admin(){
        $ci = &get_instance();
        $ci->load->library(array('ion_auth'));
        if (!$ci->ion_auth->is_admin())
        {
            set_sweat('eror','Maaf, hanya admin yg boleh akses!!!!');
            redirect(site_url('home'));
        }
    }
}

if (!function_exists('in_group')) {
    function in_group($group_id=1){ //default admin
        $ci = &get_instance();
        $groups = $ci->session->userdata('groups');
        //vardump($groups);
        if (is_array($group_id) && $groups!=NULL) {
            foreach ($group_id as $key => $value) {
                foreach($groups as $row){
                    if ($row['id']==$value) {
                        return TRUE;
                        break;
                    }
                }
            }
        }else{
            //vardump($groups);
            if ($groups!=NULL) {
                foreach($groups as $row){
                    if ($row['id']==$group_id) {
                        return TRUE;
                        break;
                    }
                }
            }
        }
        return FALSE;
    }
}

function echo_form($field,$req='required',$permission_id=NULL,$readonly=NULL)
{
    $echo = '';

    // $ci = &get_instance();
    // if ($permission_id!=NULL) {
    //     $ci->db->get_where();
    // }

    if ($field->field_type=='text') {
        $echo = '<input type="'.$field->field_type.'" id="'.$field->field_name.'" name="field_value[]" '.$readonly.' class="form-control input-md" value="'.$permission_id.'" '.$req.'>';
    }elseif ($field->field_type=='number') {
        $echo = '<input type="'.$field->field_type.'" id="'.$field->field_name.'" name="field_value[]" '.$readonly.' class="form-control input-md" value="'.$permission_id.'" '.$req.'>';
    }elseif ($field->field_type=='date') {
        $echo = '<input type="'.$field->field_type.'" id="'.$field->field_name.'" name="field_value[]" '.$readonly.' class="form-control input-md" value="'.$permission_id.'" '.$req.'>';
    }elseif ($field->field_type=='time') {
        $echo = '<input type="'.$field->field_type.'" id="'.$field->field_name.'" name="field_value[]" '.$readonly.' class="form-control input-md" '.$req.'>';
    }elseif ($field->field_type=='textarea') {
        $echo = '
            <textarea name="field_value[]" id="'.$field->field_name.'" class="form-control input-md">'.$permission_id.'</textarea>
        ';
    }
    return $echo;
    //var_dump($field);
}

if (!function_exists('create_breadcrumb')) {
    function create_breadcrumb($initial_crumb = '', $initial_crumb_url = '') {
        $ci = &get_instance();
        $open_tag = '<ol class="breadcrumb">';
        $close_tag = '</ol>';
        $crumb_open_tag = '<li>';
        $active_crumb_open_tag = '<li class="active">';
        $crumb_close_tag = '</li>';
        $separator = '<span class="crumb-separator"></span>';
        $total_segments = $ci->uri->total_segments();
        $breadcrumbs = $open_tag;
        if ($initial_crumb != '') {
            $breadcrumbs .= $crumb_open_tag;
            $breadcrumbs .= create_crumb_href($initial_crumb, false, true) . $separator;
        }

        $segment = '';
        $crumb_href = '';

        for ($i = 1; $i <= $total_segments; $i++) {
            if (strlen($ci->uri->segment($i))<25) {
                $segment = $ci->uri->segment($i);
                $crumb_href .= $ci->uri->segment($i) . '/';
                if ($total_segments > $i) {
                    $breadcrumbs .= $crumb_open_tag;
                    $breadcrumbs .= create_crumb_href($segment, $crumb_href);
                    $breadcrumbs .= $separator;
                }else{
                    $breadcrumbs .= $active_crumb_open_tag;
                    $breadcrumbs .= create_crumb_href($segment, $crumb_href);
                }
            }

            $breadcrumbs .= $crumb_close_tag;
        }
        $breadcrumbs .= $close_tag;
        return $breadcrumbs;
    }
}
if (!function_exists('create_crumb_href')) {
    function create_crumb_href($uri_segment, $crumb_href = false, $initial = false) {
        $ci = &get_instance();
        $base_url = $ci->config->base_url();

        $crumb_href = rtrim($crumb_href, '/');

        if($initial) {
            return '<a href="' . $base_url . '">' . ucwords(str_replace(array('-', '_'), ' ', $uri_segment)) . '</a>';
        }else{
            return '<a href="' . $base_url . $crumb_href . '">' . ucwords(str_replace(array('-', '_'), ' ', $uri_segment)) . '</a>';
        }
    }
}

if (!function_exists('encrypt')) {
    function encrypt($data) {
        $ci = &get_instance();
        return $ci->encrypt->encode($data);
    }
}

if (!function_exists('decrypt')) {
    function decrypt($data) {
        $ci = &get_instance();
        return $ci->encrypt->decode($data);
    }
}

if (!function_exists('vardump')) {
    function vardump($data) {
        echo '<pre>';
        var_dump($data);
        echo '</pre>';exit();
    }
}






//////////////////////////////////////////////////////////////////////
//PARA: Date Should In YYYY-MM-DD Format
//RESULT FORMAT:
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                            =>  3 Month 14 Day
// '%d Day %h Hours'                                            =>  14 Day 11 Hours
// '%d Day'                                                        =>  14 Days
// '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
// '%h Hours                                                    =>  11 Hours
// '%a Days                                                        =>  468 Days
//////////////////////////////////////////////////////////////////////
function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    $interval = date_diff($datetime1, $datetime2);
    return $interval->format($differenceFormat);
}

function masa_kerja($tgl_masuk){
    return
    dateDifference(date('Y-m-d'),date('Y-m-d',strtotime($tgl_masuk)),'%y').' Tahun '.
    dateDifference(date('Y-m-d'),date('Y-m-d',strtotime($tgl_masuk)),'%m').' Bulan '.
    dateDifference(date('Y-m-d'),date('Y-m-d',strtotime($tgl_masuk)),'%d').' Hari';
}

function sisa_cuti($staff_id)
{
    $ci = &get_instance();
    if ($staff_id!=NULL) {
        $data_staff = $ci->db->get_where('staffdata',['staff_id'=>$staff_id])->row();
        $tgl_masuk  = $data_staff->tgl_masuk;
        $lama_kerja = dateDifference(date('Y-m-d'),date('Y-m-d',strtotime($tgl_masuk)),'%y');
        if ($lama_kerja>0) {
            //lebih dari 1 tahun
            $max_cuti   = $ci->db->get('settings')->row()->max_cuti;
            $cuti_bersama = $ci->db->get_where('holidays',['is_red'=>'N'])->num_rows();
            $ambil_cuti   = $ci->db->get_where('permission',['staff_id'=>$staff_id,'permission_type_id'=>3,'permission_type_id'=>6])->num_rows();
            return [ 'max_cuti' => $max_cuti,
                     'cuti_bersama' => $cuti_bersama,
                     'ambil_cuti' => $ambil_cuti,
                     'jatah_cuti' => $max_cuti - $cuti_bersama - $ambil_cuti, ];
        }
    }
}

function cek_hari($date)
{
    if (date('D',strtotime($date))=='Sun') {
        return 'minggu';
    }elseif (date('D',strtotime($date))=='Mon') {
        return 'senin';
    }elseif (date('D',strtotime($date))=='Tue') {
        return 'selasa';
    }elseif (date('D',strtotime($date))=='Wed') {
        return 'rabu';
    }elseif (date('D',strtotime($date))=='Thu') {
        return 'kamis';
    }elseif (date('D',strtotime($date))=='Fri') {
        return 'jumat';
    }elseif (date('D',strtotime($date))=='Sat') {
        return 'sabtu';
    }else{
        return NULL;
    }
}

function cek_telat($staff_id,$date,$time)
{
    $ci = &get_instance();
    $telat = [
        'telat'  => FALSE,
        'durasi' => 0,
        'in'     => FALSE,
        'out'    => FALSE,
        'day'    => NULL,
        'date'   => NULL,
    ];
    $hari = cek_hari($date);
    $jadwal = $ci->db->join('work_time','work_time.work_time_id = work_schedule.work_time_id')->get_where('work_schedule',['staff_id'=>$staff_id,'day'=>$hari])->row();
    if ($jadwal!=NULL) {
        if ($time > $jadwal->work_time_start && $jadwal->work_time_start > date('H:i:s',strtotime('00:00:00'))) {
            $telat = [
                'telat'  => TRUE,
                'durasi' => dateDifference($time, $jadwal->work_time_start, '%h' ).":".dateDifference($time, $jadwal->work_time_start, '%i' ),
                'in'     => $jadwal->work_time_start,
                'out'    => $jadwal->work_time_end,
                'day'    => $hari,
                'date'   => $date,
            ];
        }else{
           $telat = [
                'telat'  => FALSE,
                'durasi' => dateDifference($time, $jadwal->work_time_start, '%h' ).":".dateDifference($time, $jadwal->work_time_start, '%i' ),
                'in'     => $jadwal->work_time_start,
                'out'    => $jadwal->work_time_end,
                'day'    => $hari,
                'date'   => $date,
            ];
        }
    }
    return $telat;
}

function list_month($name=NULL)
{
    $month = [
        '00' => '- Bulan -',
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember',
    ];
    if ($month!=NULL) {
        foreach ($month as $key => $value) {
            if ($name==$key) {
                $month = $value;
                break;
            }
        }
    }
    return $month;
}

function clearJSON($json_data) {
 if (0 === strpos(bin2hex($json_data), 'efbbbf')) {
       return substr($json_data, 3);
    } else {
     return $json_data;
    }
}

function cek_permission_approval($staff_id,$permission_id)
{
    $ci = &get_instance();
    $cek = $ci->db->get_where('notification',['fk'=>$permission_id,'receiver_id'=>$staff_id])->row();
    if ($cek!=NULL && count($cek)>0) {
        return TRUE;
    }return FALSE;
}

function nama_perusahaan($staff_id){
    $ci = &get_instance();
    return $ci->db->join('company','company.company_id = company_staff.company_id')->get_where('company_staff',['staff_id'=>$staff_id])->row();
}

function list_perizinan($where)
{
    $ci = &get_instance();
    if ($where!=NULL) {
        if (is_array($where)) {
            foreach ($where as $key => $value) {
                $ci->db->where($key,$value);
            }
        }else{
            $ci->db->where('type_id',$where);
        }
    }
    return $ci->db->get('permission_type')->result();
}

/**
 * @param array object $data
 * @param string field
 * @param string compare
 */
function get_compare($data,$field,$compare)
{
    /*
    $datas = $data;
    foreach ($datas as $row) {
        return $compare;
        break;
        if ($row->$field == $compare) {
            return TRUE;
            break;
        }
    }return FALSE;
    */
    $grouping   = [];
    $master     = [];
    foreach ($datas as $row) {
        if ($row->is_parent!=0) {
            $grouping[] = $row;
        }else{
            $master[]   = $row;
        }
    }


}

function hapus_simbol($result) {
    $result = strtolower($result);
    $result = preg_replace('/&amp;.+?;/', ' ', $result);
    $result = preg_replace('/\s+/', ' ', $result);
    $result = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '+', $result);
    $result = preg_replace('|-+|', ' ', $result);
    $result = preg_replace('/&amp;#?[a-z0-9]+;/i',' ',$result);
    $result = preg_replace('/[^%A-Za-z0-9 _-]/', ' ', $result);
    // $result = trim($result, '');
    return $result;
}

function in_array_r($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }
    return false;
}
