<?php

/**
*   @param $type []
*/
function send_notification($type,$from,$to,$message,$metadata='')
{
    $ci = get_instance();
    //send notification
    if (is_array($to)) {
        foreach ($to as $key => $value) {
            $data[] = [
                'sender_id'     => $from,
                'receiver_id '  => $value,
                'type'          => $type,
                'message'       => $message,
                'metadata'      => $metadata,
                'created'       => date('Y-m-d H:i:s'),
            ];
        }
        $input = $ci->db->insert_batch('notification',$data);
        $admin = [
            'sender_id'     => $from,
            'receiver_id '  => 1,
            'type'          => $type,
            'message'       => $message,
            'metadata'      => $metadata,
            'created'       => date('Y-m-d H:i:s'),
        ];
        cc_notifadmin($admin);
        $action = ($input>0)?TRUE:FALSE;
    }else{
        $data = [
            'sender_id'     => $from,
            'receiver_id '  => $to,
            'type'          => $type,
            'message'       => $message,
            'metadata'      => $metadata,
            'created'       => date('Y-m-d H:i:s'),
        ];
        $action = $ci->db->insert('notification',$data);
        cc_notifadmin($data);
    }
    return $action;
}

function cc_notifadmin($data){
    $ci = get_instance();
    $data['receiver_id'] = 1;
    $ci->db->insert('notification',$data);
}

function get_notif($reciver_id)
{
    $ci = get_instance();
    return $ci->db->select("*,
        (SELECT first_name FROM users WHERE users.id = notification.sender_id) as pengirim
    ")->get_where('notification',['receiver_id'=>$reciver_id])->result();
}

function read_notif($notif_id)
{
    $ci = get_instance();
    //send notification
    $data = [
        'is_read' => 'Y',
    ];
    return $ci->db->where('notification_id')->update('notification',$data);

}


function where_status()
{

}

function group_user_involved($stages_parent)
{
    $ci = get_instance();
    $z = $ci->db
    ->join('stages_involved','stages_involved.stage_id = stages.stage_id')
    ->where('stages_parent',$stages_parent)->get('stages')->result();
    vardump($z);
}
