<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings 
{
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
    }

    public function load_settings()
    {
        return $this->CI->db->get('settings')->row();
    }

}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */