<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission
{
    private $ci;

    function __construct()
    {
        $this->ci =& get_instance();
    }


    public function access($user_id=NULL){
        $class_name = $this->ci->router->fetch_class();
        $method = $this->ci->router->fetch_method();
        $staffdata = $this->ci->session->userdata('staffdata');
        if ($user_id==NULL) {
            $user_id = $ci->session->userdata('staffdata')->staff_id;
        }
        $this->ci->db
        ->select('link,menu_id,lihat,daftar,tambah,ubah,hapus')
        ->join('menu','menu.id = user_groups_menu.menu_id')
        ->join('users_groups','users_groups.group_id = user_groups_menu.group_id');
        $this->ci->db->where('class',$class_name);
        $this->ci->db->where('user_id',$ci->session->userdata('user_id'));
        $x = $this->ci->db->get('user_groups_menu')->row();
        //vardump($x);
        if ($x==NULL) {
            set_sweat('warning','Anda tidak punya hak akses kesini!!!!');
            redirect(site_url('home'));
        }
        if ($method == 'read') {
            if ($x->lihat == 0) {
                set_sweat('warning','Anda tidak punya hak akses READ !!!!');
                redirect(site_url('home'));
            }
        }
        if ($method == 'index') {
            if ($x->daftar == 0) {
                set_sweat('warning','Anda tidak punya hak akses LIST !!!!');
                redirect(site_url($class_name.'/read/'.$user_id));
            }
        }
        if ($method == 'create') {
            if ($x->tambah == 0) {
                set_sweat('warning','Anda tidak punya hak akses CREATE !!!!');
                redirect(site_url('home'));
            }
        }
        if ($method == 'create_action') {
            if ($x->tambah == 0) {
                set_sweat('warning','Anda tidak punya hak akses INSERT !!!!');
                redirect(site_url('home'));
            }
        }
        if ($method == 'update' ) {
            if ($x->ubah == 0) {
                set_sweat('warning','Anda tidak punya hak akses UPDATE !!!!');
                redirect(site_url('home'));
            }
        }
        if ($method == 'update_action' ) {
            if ($x->ubah == 0) {
                set_sweat('warning','Anda tidak punya hak akses UPDATE !!!!');
                redirect(site_url('home'));
            }
        }
        if ($method == 'delete') {
            if ($x->hapus == 0) {
                set_sweat('warning','Anda tidak punya hak akses DELETE !!!!');
                redirect(site_url('home'));
            }
        }
    }

}
