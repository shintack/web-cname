<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * RajaOngkir API Key
 * Silakan daftar akun di RajaOngkir.com untuk mendapatkan API Key
 * http://rajaongkir.com/akun/daftar
 */
$config['rajaongkir_api_key'] = "5443b9a734c84a3f9f59b60240510c15";

/**
 * RajaOngkir account type: starter, basic, pro
 * http://rajaongkir.com/dokumentasi#akun-ringkasan
 * 
 */
$config['rajaongkir_account_type'] = "pro";
