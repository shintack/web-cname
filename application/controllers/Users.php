<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller
{    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');
        $this->_init();
        logged_in();
    }

    //1. _init
    public function _init()
    {
        $this->_mod    = '';//Users'; 
        $this->_template = 'template/admin/template';
        $this->_view     = 'admin';
        $x = (object)$this->Users_model->structure();
        $this->_tabel    = $x->table;
        $this->_pk       = $x->pk;
        $this->_field    = $x->field;
    }

    //digunakan untuk ajax datatable
    public function get_data()
    {
        echo $data = $this->Users_model->get_datatables();
    }



    public function index()
    {
       //add data breadcumbs
       $data['title'] = 'Users';
       $data['sub_title'] = 'Daftar Data';
       $data['home']  = 'Dashboard';
       $data['home_link'] = site_url(); 
       $data['input'] = $this->Users_model->_set_value();
       //set module
       $data['module'] = $this->_mod;
       $data['ajax']   = site_url('users/get_data');
       $this->template->load($this->_template,$this->_view.'/users/users_list',$data);
    }

    //2. liat data satuan
    public function read($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Users_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Users_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Users';
            $data['sub_title'] = 'Lihat Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_view.'/users/users_read',$data);
        } else {
            set_sweat('eror','Data gak ada bro!!!!');
            redirect(site_url($this->_mod.'/users'));
        }
    }

    //3. create 
    public function create() 
    {
        $data = $this->Users_model->_set_value();
        //add data breadcumbs
        $data['title'] = 'Users';
        $data['sub_title'] = 'Tambah Data';
        $data['home']  = 'Dashboard';
        $data['home_link'] = site_url(); 
        //set module
        $data['module'] = $this->_mod;
        $this->template->load($this->_template,$this->_view.'/users/users_form',$data);
    }

    //4. update
    public function update($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Users_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Users_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Users';
            $data['sub_title'] = 'Update Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_view.'/users/users_form',$data);
        } else {
            set_sweat('eror','Data gak ada bro!!!!');
            redirect(site_url($this->_mod.'/users'));
        }
    }


    /**
     * dimari khusus buat proses data ya bos 
     * -- content --
     * 1. create_action => save data
     * 2. update_action => update data
     * 3. delete        => hapus data 
     * 4. excel         => export to excel 
     */
    public function create_action() 
    {
        $this->Users_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = $this->Users_model->_get_post();

            $this->Users_model->insert($this->_tabel,$data);
            set_sweat('success','Data berhasil disimpan!!!!');
            redirect(site_url($this->_mod.'/users'));
        }
    }

    public function update_action() 
    {
        $this->Users_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = $this->Users_model->_get_post();
            $this->Users_model->update($this->_tabel,$this->_pk,$this->input->post('id', TRUE), $data);
            set_sweat('success','Data berhasil disimpan!!!!');
            redirect(site_url($this->_mod.'/users'));
        }
    }
 
    public function delete() 
    {
        $id    = $this->input->post('id',TRUE);
        $where = [ $this->_pk => $id ];
        $row   = $this->Users_model->get_row($this->_tabel,$where);
        if ($row) {
            $this->Users_model->delete($this->_tabel,$this->_pk,$id);
            set_sweat('success','Data berhasil dihapus!!!!');
            redirect(site_url($this->_mod.'/users'));
        } else {
            set_sweat('eror','Data gak ada bro!!!!');
            redirect(site_url($this->_mod.'/users'));
        }
    }


    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "users.xls";
        $judul = "users";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Ip Address");
		xlsWriteLabel($tablehead, $kolomhead++, "Username");
		xlsWriteLabel($tablehead, $kolomhead++, "Password");
		xlsWriteLabel($tablehead, $kolomhead++, "Salt");
		xlsWriteLabel($tablehead, $kolomhead++, "Email");
		xlsWriteLabel($tablehead, $kolomhead++, "Activation Code");
		xlsWriteLabel($tablehead, $kolomhead++, "Forgotten Password Code");
		xlsWriteLabel($tablehead, $kolomhead++, "Forgotten Password Time");
		xlsWriteLabel($tablehead, $kolomhead++, "Remember Code");
		xlsWriteLabel($tablehead, $kolomhead++, "Created On");
		xlsWriteLabel($tablehead, $kolomhead++, "Last Login");
		xlsWriteLabel($tablehead, $kolomhead++, "Active");
		xlsWriteLabel($tablehead, $kolomhead++, "First Name");
		xlsWriteLabel($tablehead, $kolomhead++, "Last Name");
		xlsWriteLabel($tablehead, $kolomhead++, "Company");
		xlsWriteLabel($tablehead, $kolomhead++, "Phone");

		foreach ($this->Users_model->get_all($this->_tabel,$this->_pk) as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->ip_address);
			xlsWriteLabel($tablebody, $kolombody++, $data->username);
			xlsWriteLabel($tablebody, $kolombody++, $data->password);
			xlsWriteLabel($tablebody, $kolombody++, $data->salt);
			xlsWriteLabel($tablebody, $kolombody++, $data->email);
			xlsWriteLabel($tablebody, $kolombody++, $data->activation_code);
			xlsWriteLabel($tablebody, $kolombody++, $data->forgotten_password_code);
			xlsWriteNumber($tablebody, $kolombody++, $data->forgotten_password_time);
			xlsWriteLabel($tablebody, $kolombody++, $data->remember_code);
			xlsWriteNumber($tablebody, $kolombody++, $data->created_on);
			xlsWriteNumber($tablebody, $kolombody++, $data->last_login);
			xlsWriteLabel($tablebody, $kolombody++, $data->active);
			xlsWriteLabel($tablebody, $kolombody++, $data->first_name);
			xlsWriteLabel($tablebody, $kolombody++, $data->last_name);
			xlsWriteLabel($tablebody, $kolombody++, $data->company);
			xlsWriteLabel($tablebody, $kolombody++, $data->phone);

			$tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=users.doc");

        $data = array(
            'users_data' => $this->Users_model->get_all($this->_tabel,$this->_pk),
            'start' => 0
        );
        
        $this->load->view($this->_view.'/users/users_doc',$data);
    }

    function pdf()
    {
        $data = array(
            'users_data' => $this->Users_model->get_all($this->_tabel,$this->_pk),
            'start' => 0
        );
        
        $this->load->library('Pdf');
        $html = $this->pdf->load_view($this->_view.'/users/users_pdf', $data);
        $this->pdf->render();
        $filename = 'pdf';
        $this->pdf->stream($filename, array('Attachment' => false));
        exit(0); 
    }

}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2018-06-03 01:09:12 */
/* http://shintackeror.web.id thanks to http://harviacode.com */