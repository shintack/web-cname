<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends MY_Controller{
    function __construct(){
        parent::__construct();
        logged_in();
    }
 
    function index(){
        //$this->load->view('v_upload');    
    }
 
    function do_upload(){

        $this->upload_image($name,$field,$path);

        $config['upload_path']="./uploads/documents";
        $config['allowed_types']='gif|jpg|png';
        $config['encrypt_name'] = TRUE;
         
        $this->load->library('upload',$config);
        if($this->upload->do_upload("file")){
            $data = array('upload_data' => $this->upload->data());
 
            $judul= $this->input->post('judul');
            $image= $data['upload_data']['file_name']; 
             
            $result= $this->m_upload->simpan_upload($judul,$image);
            echo json_decode($result);
        }
 
     }
     
} 