<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cekdomain extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('curl');
    }

    public function index()
    {
      // code...
    }

    public function domain()
    {
      $extention = ['.com','.id','.net','.org','.biz','info','.tv','.name','.edu','.sch','.web.id','.my.id','.or.id','.ac.id'];
      $domain    = $this->input->post('domain');
      if ( $domain ) {
        $domain_ok = '';
        foreach ($extention as $ex) {
          $domain_ok .= $domain.$ex.',';
        }
        $params = [
          'domains' => $domain_ok,
        ];
        $params = http_build_query($params);
        // vardump($params);
        $this->curl->http_header('X-Mashape-Key','T7H5l7IMF8msh72YRasTYi66nauLp17qiocjsnsXSLQz8lxZPs');
        $curl = $this->curl->simple_get('https://pointsdb-bulk-domain-check-v1.p.mashape.com/domain_check?'.$params);
        $curl = json_decode($curl);
        echo $this->build_table($curl);
      }
    }

    public function build_table($data)
    {
      $html = '';
      if ( isset($data->availability) ) {
        $html .= '
        <div class="table-responsive">
          <table class="table">
        ';
        foreach ($data->availability as $key => $value) {
          $html .= '<tr>';
          $html .= '<td>'.$key.'</td>';
          $html .= '<td></td>';
          $avaliable = $value?'<button onclick="pilihDomain(\''.$key.'\')" class="btn btn-sm btn-primary">Pilih</button>':'tidak tersedia';
          $html .= '<td width="30%" align="right">'.$avaliable.'</td>';
          $html .= '</tr>';
        }
        $html .= '
          </table>
        </div>
        ';
      }
      return $html;
    }
}
