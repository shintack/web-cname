<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tes extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
      $extention = ['.com','.id','.net','.org','.biz','info','.tv','.name','.edu','.sch','.web.id','.my.id','.or.id','ac.id'];
      $domain    = 'babastudio';
      $domain_ok = '';
      foreach ($extention as $ex) {
        $domain_ok .= $domain.$ex.',';
      }
      $params = [
        'domains' => $domain_ok,
      ];
      $params = http_build_query($params);
      // vardump($params);
      $this->load->library('curl');
      $this->curl->http_header('X-Mashape-Key','T7H5l7IMF8msh72YRasTYi66nauLp17qiocjsnsXSLQz8lxZPs');
      $curl = $this->curl->simple_get('https://pointsdb-bulk-domain-check-v1.p.mashape.com/domain_check?'.$params);
      $curl = json_decode($curl);
      vardump($curl);
      // These code snippets use an open-source library. http://unirest.io/php
      // $response = Unirest\Request::get("https://pointsdb-bulk-domain-check-v1.p.mashape.com/domain_check?domains=babastudio.com%2Cbabastudio.id%2Cbabastudio.org%2Cbabastudio.net",
      //   array(
      //     "X-Mashape-Key" => "T7H5l7IMF8msh72YRasTYi66nauLp17qiocjsnsXSLQz8lxZPs"
      //   )
      // );
    }

}
