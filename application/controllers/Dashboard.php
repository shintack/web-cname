<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->_init();
        logged_in();
    }

    //1. _init
    private function _init()
    {
        $this->data['title'] = 'Dashboard';
        $this->data['sub_title'] = 'Daftar Data';
        $this->data['home']  = 'Dashboard';
        $this->data['home_link'] = site_url();
        $this->data['module'] = $this->_mod;
        $this->_template = 'layout/material/template';
        $this->_view     = 'front/sitebeat/dashboard/';
    }

    public function index()
    {
      $param = http_build_query([
        'email' => $this->session->userdata('email'),
      ]);
      $this->data['urlDataTable'] = $this->_base_api_url.'host/host_register/get_data_user?'.$param;
      //get my order
      $this->template->load($this->_template,$this->_view.'index',$this->data);
    }

}
