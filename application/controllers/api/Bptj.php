<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class Bptj extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index_post()
    {
        $data   = [];
        $action = $this->post('act');
        $key    = $this->post('key');
        $param  = $this->post('data');
        $secret = 'auahG3l4p5';
        if ($key===$secret) {
          if ($action!=NULL) {
              if ($action==='login') {
                  $data = $this->login($param);
              }elseif($action==='register'){
                  $data = $this->register($param);
              }
          }else{
              $data = [
                  'status'    => FALSE,
                  'message'   => 'NO ACTION',
              ];
          }
        }else{
          $data = [
              'status'    => FALSE,
              'message'   => 'KEY NOT VALID',
          ];
        }
        $this->response($data, 200);
    }

    /**
    *   ACTION
    *
    */
    private function login($data)
    {
      $this->load->model('Ion_auth_model');
      /**
       * data = [username, password] (json_encode)
       */
      $data_login = json_decode($data);
      $remember = TRUE; //(bool) $this->input->post('remember');
      //$return = $this->ion_auth_model->login($data_login->email, $data_login->password, $remember);
      if ($this->ion_auth_model->login($data_login->email, $data_login->password, $remember)) {
          $data = [
              'status'    => TRUE,
              'message'   => "LOGIN SUCCESS",
              'data'      => $this->ion_auth_model->get_userdata($data_login->email),
          ];
      }else{
          $data = [
              'status'    => FALSE,
              'message'   => "LOGIN FAILED",
              'data'      => NULL, //$this->ion_auth_model->get_userdata($data_login->email),
          ];
      }
      return $data;
    }

    private function register($data)
    {
      $data = [
          'status'    => TRUE,
          'message'   => "INI REGISTER",
          'data'      => NULL, //$this->ion_auth_model->get_userdata($data_login->email),
      ];
      return $data;
    }
    //Menampilkan data kontak
    /*
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $kontak = $this->db->get('users')->result();
        } else {
            $this->db->where('id', $id);
            $kontak = $this->db->get('users')->result();
        }
        $this->response($kontak, 200);
    }
    */

    //Masukan function selanjutnya disini
}
?>
