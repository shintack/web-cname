<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\REST_Controller;

class Permohonan extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        //$this->load->database();
    }

    public function index_get($jenis=NULL,$user_id=NULL)
    {
        $data = [
            'status'    => FALSE,
            'message'   => 'KEY NOT VALID',
            'data'      => NULL,
        ];

        if ($jenis==='angkutan-permukiman') {
            $data = [
                'status'    => TRUE,
                'message'   => 'SUCCESS',
                'data'      => $this->jenis_permohonan('perum',$user_id),
            ];
        }elseif($jenis==='angkutan-perkotaan'){
            $data = [
                'status'    => TRUE,
                'message'   => 'SUCCESS',
                'data'      => $this->jenis_permohonan('kota',$user_id),
            ];
        }elseif($jenis==='register'){
            $data = $this->register();
        }
        $this->response($data, 200);
    }

    public function register()
    {
        $area    = $this->uri->segment(4);
        $type_id = decrypt($this->uri->segment(5));
        $user_id = decrypt($this->uri->segment(6));
        $register = [
            'type_id'    => $type_id,
            'created_by' => $user_id,
            'area'       => $area,
    	];
        $data_registrasi = NULL;
        //cek perusahan sdh di verifikasi apa blom
        $cek_perusahaan = $this->db->get_where('companies',['user_id'=>$user_id])->row();
        if ($cek_perusahaan->is_valid!=1) {
            return [
                'status'    => FALSE,
                'message'   => 'Data perusahaan belum di verifikasi petugas',
            ];
        }else{
            $cek_registrasi = $this->db->get_where('permissions',$register)->row();
            if ($cek_registrasi===NULL) {
                $register['permission_code']    = random_string();
                $register['company_id']         = $cek_perusahaan->company_id;
                $register['created_at']         = date('Y-m-d H:i:s');
                $register                       = $this->db->insert('permissions',$register);
                $insert_id                      = $this->db->insert_id();
                $data_registrasi                = $this->db->get_where('permissions',['permission_id'=>$insert_id])->row();
                $message                        = "REGISTER SUCCESS";
            }else{
                $data_registrasi = $cek_registrasi;
                $message = "REGISTER DATA";
            }
        }
        $param = [
            'status'    => TRUE,
            'message'   => $message,
            'data'      => NULL,
    	];
        return ($param);
    }

    public function index_post()
    {
        $data   = [];
        $action = $this->post('act');
        $key    = $this->post('key');
        $param  = $this->post('data');
        $secret = 'auahG3l4p5';
        if ($key===$secret) {
          if ($action!=NULL) {
              if ($action==='jenis_permohonan') {
                  $data = $this->jenis_permohonan();
              }elseif($action==='register'){
                  $data = $this->register($param);
              }
          }else{
              $data = [
                  'status'    => FALSE,
                  'message'   => 'NO ACTION',
              ];
          }
        }else{
          $data = [
              'status'    => FALSE,
              'message'   => 'KEY NOT VALID',
          ];
        }
        $this->response($data, 200);
    }

    //private method
    private function jenis_permohonan($area,$user_id=NULL)
    {
        $this->load->model('Permission_type_model');
        return $this->Permission_type_model->get_list_api($area,$user_id);
    }

}
