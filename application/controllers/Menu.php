<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MY_Controller
{    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->_init();
        logged_in();is_admin();
    }

    //1. _init
    public function _init()
    {
        $this->_mod    = '';//Menu'; 
        $this->_template = 'template/admin/template';
        $this->_view     = 'admin';
        $x = (object)$this->Menu_model->structure();
        $this->_tabel    = $x->table;
        $this->_pk       = $x->pk;
        $this->_field    = $x->field;
    }

    //digunakan untuk ajax datatable
    public function get_data()
    {
        echo $data = $this->Menu_model->get_datatables();
    }


    /**
     * buat liat data dimari tabel & satuan 
     * -- content --
     * 1. index         => list data
     * 2. read          => liat data satuan
     * 3. create        => form polosan 
     * 4. update        => form isian 
     */
    //1. index list data
    public function index()
    {
        //add data breadcumbs
        $data['title'] = 'Menu';
        $data['sub_title'] = 'Daftar Data';
        $data['home']  = 'Dashboard';
        $data['home_link'] = site_url(); 
        $data['input'] = $this->Menu_model->_set_value();
        $data['ajax']   = site_url('menu/get_data');
        //set module
        $data['module'] = $this->_mod;
        $this->template->load($this->_template,$this->_view.'/menu/menu_list',$data);
    }

    //2. liat data satuan
    public function read($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Menu_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Menu_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Menu';
            $data['sub_title'] = 'Lihat Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_view.'/menu/menu_read',$data);
        } else {
            set_sweat('eror','Data gak ada bro!!!!');
            redirect(site_url($this->_mod.'/menu'));
        }
    }

    //3. create 
    public function create() 
    {
        $data = $this->Menu_model->_set_value();
        //add data breadcumbs
        $data['title'] = 'Menu';
        $data['sub_title'] = 'Tambah Data';
        $data['home']  = 'Dashboard';
        $data['home_link'] = site_url(); 
        //set module
        $data['module'] = $this->_mod;
        $this->template->load($this->_template,$this->_view.'/menu/menu_form',$data);
    }

    //4. update
    public function update($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Menu_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Menu_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Menu';
            $data['sub_title'] = 'Update Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_view.'/menu/menu_form',$data);
        } else {
            set_sweat('eror','Data gak ada bro!!!!');
            redirect(site_url($this->_mod.'/menu'));
        }
    }


    /**
     * dimari khusus buat proses data ya bos 
     * -- content --
     * 1. create_action => save data
     * 2. update_action => update data
     * 3. delete        => hapus data 
     * 4. excel         => export to excel 
     */
    public function create_action() 
    {
        $this->Menu_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = $this->Menu_model->_get_post();

            $this->Menu_model->insert($this->_tabel,$data);
            set_sweat('success','Data berhasil disimpan!!!!');
            redirect(site_url($this->_mod.'/menu'));
        }
    }

    public function update_action() 
    {
        $this->Menu_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = $this->Menu_model->_get_post();
            $this->Menu_model->update($this->_tabel,$this->_pk,$this->input->post('id', TRUE), $data);
            set_sweat('success','Data berhasil disimpan!!!!');
            redirect(site_url($this->_mod.'/menu'));
        }
    }
 
    public function delete() 
    {
        $id    = $this->input->post('id',TRUE);
        $where = [ $this->_pk => $id ];
        $row   = $this->Menu_model->get_row($this->_tabel,$where);
        if ($row) {
            $this->Menu_model->delete($this->_tabel,$this->_pk,$id);
            set_sweat('success','Data berhasil dihapus!!!!');
            redirect(site_url($this->_mod.'/menu'));
        } else {
            set_sweat('eror','Data gak ada bro!!!!');
            redirect(site_url($this->_mod.'/menu'));
        }
    }


    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "menu.xls";
        $judul = "menu";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Name");
		xlsWriteLabel($tablehead, $kolomhead++, "Link");
		xlsWriteLabel($tablehead, $kolomhead++, "Icon");
		xlsWriteLabel($tablehead, $kolomhead++, "Is Active");
		xlsWriteLabel($tablehead, $kolomhead++, "Is Arrage");
		xlsWriteLabel($tablehead, $kolomhead++, "Is Parent");
		xlsWriteLabel($tablehead, $kolomhead++, "Type");

		foreach ($this->Menu_model->get_all($this->_tabel,$this->_pk) as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->name);
			xlsWriteLabel($tablebody, $kolombody++, $data->link);
			xlsWriteLabel($tablebody, $kolombody++, $data->icon);
			xlsWriteNumber($tablebody, $kolombody++, $data->is_active);
			xlsWriteLabel($tablebody, $kolombody++, $data->is_arrage);
			xlsWriteNumber($tablebody, $kolombody++, $data->is_parent);
			xlsWriteLabel($tablebody, $kolombody++, $data->type);

			$tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=menu.doc");

        $data = array(
            'menu_data' => $this->Menu_model->get_all($this->_tabel,$this->_pk),
            'start' => 0
        );
        
        $this->load->view($this->_view.'/menu/menu_doc',$data);
    }

    function pdf()
    {
        $data = array(
            'menu_data' => $this->Menu_model->get_all($this->_tabel,$this->_pk),
            'start' => 0
        );
        
        $this->load->library('Pdf');
        $html = $this->pdf->load_view($this->_view.'/menu/menu_pdf', $data);
        $this->pdf->render();
        $filename = 'pdf';
        $this->pdf->stream($filename, array('Attachment' => false));
        exit(0); 
    }

}

/* End of file Menu.php */
/* Location: ./application/controllers/Menu.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2018-04-08 10:24:27 */
/* http://shintackeror.web.id thanks to http://harviacode.com */