<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ups404 extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
		//add data breadcumbs
        $data['title'] = 'Page Not Found';
        $data['sub_title'] = 'Ups 404';
        $data['home']  = 'Home';
        $data['home_link'] = site_url();
        //set module
        //$data['module'] = $this->_mod;
        $this->template->load('template/admin/template','errors/new404',$data);
    }
}
