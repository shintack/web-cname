<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @author          insan muttaqien [ shintackeror.web.id | shintack@gmail.com ]
 * @license         MIT License
 * @link            https://gitlab.com/shintack
 */

class Home extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->_init();
    }

    //1. _init
    public function _init()
    {
        $this->_view        = 'front/sitebeat/';
        $this->data         = [
            'title'         => 'Categories',
            'sub_title'     => 'Categories Data',
            'home'          => 'Dashboard',
            'home_link'     => site_url(),
            'module'        => 'market',
        ];
    }

    public function index()
    {
      // $url_paket  = $this->_base_api_url.'host/host_packet/get_host';
      $list_paket = FALSE; //json_decode($this->curl->simple_get($url_paket));
      $list_paket = $list_paket ? $list_paket->data : NULL ;

      $data = [
        'list_paket'  => $list_paket,
        'transparent' => TRUE,
      ];

      $this->data = array_merge($this->data,$data);

      $this->template->load($this->_template,$this->_view.'home/index',$this->data);
    }

}
