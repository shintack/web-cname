<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends MY_Controller
{    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Logs_model');
        $this->_init();
        logged_in();
    }

    //1. _init
    public function _init()
    {
        $this->_mod    = '';//Logs'; 
        $this->_template = 'template/admin/template';
        $this->_view     = 'admin';
        $x = (object)$this->Logs_model->structure();
        $this->_tabel    = $x->table;
        $this->_pk       = $x->pk;
        $this->_field    = $x->field;
    }

    //digunakan untuk ajax datatable
    public function get_data()
    {
        echo $data = $this->Logs_model->get_datatables();
    }



    public function index()
    {
       //add data breadcumbs
       $data['title'] = 'Logs';
       $data['sub_title'] = 'Daftar Data';
       $data['home']  = 'Dashboard';
       $data['home_link'] = site_url(); 
       $data['input'] = $this->Logs_model->_set_value();
       //set module
       $data['module'] = $this->_mod;
       $data['ajax']   = site_url('logs/get_data');
       $this->template->load($this->_template,$this->_view.'/logs/actions_logs_list',$data);
    }

    //2. liat data satuan
    public function read($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Logs_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Logs_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Logs';
            $data['sub_title'] = 'Lihat Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_view.'/logs/actions_logs_read',$data);
        } else {
            set_sweat('eror','Data gak ada bro!!!!');
            redirect(site_url($this->_mod.'/logs'));
        }
    }

    //3. create 
    public function create() 
    {
        $data = $this->Logs_model->_set_value();
        //add data breadcumbs
        $data['title'] = 'Logs';
        $data['sub_title'] = 'Tambah Data';
        $data['home']  = 'Dashboard';
        $data['home_link'] = site_url(); 
        //set module
        $data['module'] = $this->_mod;
        $this->template->load($this->_template,$this->_view.'/logs/actions_logs_form',$data);
    }

    //4. update
    public function update($id) 
    {
        $where = [ $this->_pk => $id ];
        $row = $this->Logs_model->get_row($this->_tabel,$where);
        if ($row) {
            $data = $this->Logs_model->_set_value($row);
            //add data breadcumbs
            $data['title'] = 'Logs';
            $data['sub_title'] = 'Update Data';
            $data['home']  = 'Dashboard';
            $data['home_link'] = site_url(); 
            //set module
            $data['module'] = $this->_mod;
            $this->template->load($this->_template,$this->_view.'/logs/actions_logs_form',$data);
        } else {
            set_sweat('eror','Data gak ada bro!!!!');
            redirect(site_url($this->_mod.'/logs'));
        }
    }


    /**
     * dimari khusus buat proses data ya bos 
     * -- content --
     * 1. create_action => save data
     * 2. update_action => update data
     * 3. delete        => hapus data 
     * 4. excel         => export to excel 
     */
    public function create_action() 
    {
        $this->Logs_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = $this->Logs_model->_get_post();

            $this->Logs_model->insert($this->_tabel,$data);
            set_sweat('success','Data berhasil disimpan!!!!');
            redirect(site_url($this->_mod.'/logs'));
        }
    }

    public function update_action() 
    {
        $this->Logs_model->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('actions_logs_id', TRUE));
        } else {
            $data = $this->Logs_model->_get_post();
            $this->Logs_model->update($this->_tabel,$this->_pk,$this->input->post('actions_logs_id', TRUE), $data);
            set_sweat('success','Data berhasil disimpan!!!!');
            redirect(site_url($this->_mod.'/logs'));
        }
    }
 
    public function delete() 
    {
        $id    = $this->input->post('id',TRUE);
        $where = [ $this->_pk => $id ];
        $row   = $this->Logs_model->get_row($this->_tabel,$where);
        if ($row) {
            $this->Logs_model->delete($this->_tabel,$this->_pk,$id);
            set_sweat('success','Data berhasil dihapus!!!!');
            redirect(site_url($this->_mod.'/logs'));
        } else {
            set_sweat('eror','Data gak ada bro!!!!');
            redirect(site_url($this->_mod.'/logs'));
        }
    }


    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "actions_logs.xls";
        $judul = "actions_logs";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "User Id");
		xlsWriteLabel($tablehead, $kolomhead++, "Email Login");
		xlsWriteLabel($tablehead, $kolomhead++, "User Agent");
		xlsWriteLabel($tablehead, $kolomhead++, "Ip Address");
		xlsWriteLabel($tablehead, $kolomhead++, "Note");
		xlsWriteLabel($tablehead, $kolomhead++, "Url");
		xlsWriteLabel($tablehead, $kolomhead++, "Actions");
		xlsWriteLabel($tablehead, $kolomhead++, "Timestamp");

		foreach ($this->Logs_model->get_all($this->_tabel,$this->_pk) as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteNumber($tablebody, $kolombody++, $data->user_id);
			xlsWriteLabel($tablebody, $kolombody++, $data->email_login);
			xlsWriteLabel($tablebody, $kolombody++, $data->user_agent);
			xlsWriteLabel($tablebody, $kolombody++, $data->ip_address);
			xlsWriteLabel($tablebody, $kolombody++, $data->note);
			xlsWriteLabel($tablebody, $kolombody++, $data->url);
			xlsWriteLabel($tablebody, $kolombody++, $data->actions);
			xlsWriteLabel($tablebody, $kolombody++, $data->timestamp);

			$tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=actions_logs.doc");

        $data = array(
            'actions_logs_data' => $this->Logs_model->get_all($this->_tabel,$this->_pk),
            'start' => 0
        );
        
        $this->load->view($this->_view.'/logs/actions_logs_doc',$data);
    }

    function pdf()
    {
        $data = array(
            'actions_logs_data' => $this->Logs_model->get_all($this->_tabel,$this->_pk),
            'start' => 0
        );
        
        $this->load->library('Pdf');
        $html = $this->pdf->load_view($this->_view.'/actions_logs/actions_logs_pdf', $data);
        $this->pdf->render();
        $filename = 'pdf';
        $this->pdf->stream($filename, array('Attachment' => false));
        exit(0); 
    }

}

/* End of file Logs.php */
/* Location: ./application/controllers/Logs.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2018-05-18 01:11:08 */
/* http://shintackeror.web.id thanks to http://harviacode.com */