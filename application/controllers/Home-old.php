<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @author          insan muttaqien [ shintackeror.web.id | shintack@gmail.com ]
 * @license         MIT License
 * @link            https://gitlab.com/shintack
 */

class Home extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->_init();
        $this->load->library('curl');
    }

    //1. _init
    public function _init()
    {
        $this->_template    = 'layout/sitebeat/template';
        $this->_view        = 'front/sitebeat/';
        $this->path_assets  = base_url('template/sitebeat/');
        $this->data         = [
            'title'         => 'Categories',
            'sub_title'     => 'Categories Data',
            'home'          => 'Dashboard',
            'home_link'     => site_url(),
            'module'        => 'market',
        ];
    }

    public function index()
    {
      $url_paket  = $this->_base_api_url.'host/host_packet/get_host';
      $list_paket = json_decode($this->curl->simple_get($url_paket));
      $list_paket = $list_paket ? $list_paket->data : NULL ;

      $data = [
        'list_paket'  => $list_paket,
        'transparent' => TRUE,
      ];

      $this->data = array_merge($this->data,$data);

      $this->template->load($this->_template,$this->_view.'home/index',$this->data);
    }

    public function register()
    {
      $this->template->load($this->_template,$this->_view.'register/index',$this->data);
    }

    public function pre_register()
    {
      $paket_id     = $this->input->post('paket_id');
      // get detail  paket_id
      // $url_paket  = $_SERVER['SERVER_NAME']=='localhost' ? 'http://localhost/deplaza/' : 'https://api.deplaza.id/v3/';
      $url_paket  = $this->_base_api_url.'host/host_packet/get_host/'.$paket_id;
      $paket = $this->curl->simple_get($url_paket);
      $paket = json_decode($paket);
      $paket = $paket->data[0];
      $data = [
        'paket_id'    => $paket_id,
        'domain_type' => $this->input->post('domain_type'),
        'domain_name' => $this->input->post('domain_name'),
        'detail_paket'=> $paket,
      ];
      // set session
      $this->session->set_userdata([
        'order_data' => $data,
      ]);

      $this->output
      ->set_content_type('application/json')
      ->set_output(json_encode([
        'status'  => $data ? TRUE : FALSE,
        'message' => $data ? 'Success Pre-Register' : 'Failed Pre-Register',
        'data'    => $data
      ]));

    }

}
