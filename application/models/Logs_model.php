<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Logs_model extends MY_Model
{

    private $table = 'actions_logs';
    private $id    = 'actions_logs_id';
    private $order = 'DESC';
    private $field = [ 'user_id','email_login','user_agent','ip_address','note','url','actions','timestamp', ];
    //variabel untuk datatable
    private $_column_order;
    private $_column_search;
    private $_order; 

    function __construct()
    {
        parent::__construct();
        $this->_column_order = [ NULL,'user_id','email_login','user_agent','ip_address','note','url','actions','timestamp', ];
        $this->_column_search = $this->field;
        $this->_order = [ $this->id=>$this->order ];
    }

    function structure()
    {   
        return ['table' => $this->table, 'pk' => $this->id, 'field' => $this->field];
    }

    //datatable
    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->_column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item,$_POST['search']['value']);
                }
                if(count($this->_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->_order))
        {
            $order = $this->_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    private function count_filtered()
    {
        $this->_get_datatables_query();
        return $this->db->get()->num_rows();
    }

    private function count_all()
    {
        return $this->db->from($this->table)->count_all_results();
    }

    function get_datatables()
    {
        $data = array();

        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        
        $no = $_POST['start'];
        foreach ($query->result() as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->user_id;
            $row[] = $field->email_login;
            $row[] = $field->user_agent;
            $row[] = $field->ip_address;
            $row[] = $field->note;
            $row[] = $field->url;
            $row[] = $field->actions;
            $row[] = $field->timestamp; 
            $row[] =  
            anchor(site_url($this->_mod.'/logs/read/'.$field->actions_logs_id),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-info btn-sm')).
            anchor(site_url($this->_mod.'/logs/update/'.$field->actions_logs_id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')).
            '<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModalDelete'.$field->actions_logs_id.'" title="delete"><i class="fa fa-trash-o"></i></button>'.
            $this->modal_delete($this->_set_value(),$field->actions_logs_id);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->count_all(),
            "recordsFiltered" => $this->count_filtered(),
            "data" => $data,
        );
        return json_encode($output);
    }


    //2. _rules
    function _rules() 
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
		$this->form_validation->set_rules('email_login', 'email login', 'trim|required');
		$this->form_validation->set_rules('user_agent', 'user agent', 'trim|required');
		$this->form_validation->set_rules('ip_address', 'ip address', 'trim|required');
		$this->form_validation->set_rules('note', 'note', 'trim|required');
		$this->form_validation->set_rules('url', 'url', 'trim|required');
		$this->form_validation->set_rules('actions', 'actions', 'trim|required');
		$this->form_validation->set_rules('timestamp', 'timestamp', 'trim|required');

		$this->form_validation->set_rules('actions_logs_id', 'actions_logs_id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    //3. _set_value
    function _set_value($row=NULL)
    {
        if($row){
            $data = [
                'button' => 'Update',
                'action' => site_url($this->_mod.'/logs/update_action'),
				'actions_logs_id' => set_value('actions_logs_id', $row->actions_logs_id),
				'user_id' => set_value('user_id', $row->user_id),
				'email_login' => set_value('email_login', $row->email_login),
				'user_agent' => set_value('user_agent', $row->user_agent),
				'ip_address' => set_value('ip_address', $row->ip_address),
				'note' => set_value('note', $row->note),
				'url' => set_value('url', $row->url),
				'actions' => set_value('actions', $row->actions),
				'timestamp' => set_value('timestamp', $row->timestamp),
			];
        }else{
            $data = [
                'button' => 'Save',
                'action' => site_url($this->_mod.'/logs/create_action'),
				'actions_logs_id' => set_value('actions_logs_id'),
				'user_id' => set_value('user_id'),
				'email_login' => set_value('email_login'),
				'user_agent' => set_value('user_agent'),
				'ip_address' => set_value('ip_address'),
				'note' => set_value('note'),
				'url' => set_value('url'),
				'actions' => set_value('actions'),
				'timestamp' => set_value('timestamp'),
			];
        }
        $data['delete'] = site_url($this->_mod.'/logs/delete');
        return $data;
    }

    //4. _get_post
    function _get_post()
    {
        $data = [
			'user_id' => $this->input->post('user_id',TRUE),
			'email_login' => $this->input->post('email_login',TRUE),
			'user_agent' => $this->input->post('user_agent',TRUE),
			'ip_address' => $this->input->post('ip_address',TRUE),
			'note' => $this->input->post('note',TRUE),
			'url' => $this->input->post('url',TRUE),
			'actions' => $this->input->post('actions',TRUE),
			'timestamp' => $this->input->post('timestamp',TRUE),
		];
        return $data;
    }


}

/* End of file Logs_model.php */
/* Location: ./application/models/Logs_model.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2018-05-18 01:11:08 */
/* http://shintackeror.web.id thanks to http://harviacode.com */