<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends MY_Model
{

    private $table = 'menu';
    private $id    = 'id';
    private $order = 'DESC';
    private $field = [ 'name','link','icon','is_active','is_arrage','is_parent','type', ];
    //variabel untuk datatable
    private $_column_order;
    private $_column_search;
    private $_order; 

    function __construct()
    {
        parent::__construct();
        $this->_column_order = [ NULL,'name','link','icon','is_active','is_arrage','is_parent','type', ];
        $this->_column_search = $this->field;
        $this->_order = [ $this->id=>$this->order ];
    }

    function structure()
    {   
        return ['table' => $this->table, 'pk' => $this->id, 'field' => $this->field];
    }

    //datatable
    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->_column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item,$_POST['search']['value']);
                }
                if(count($this->_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->_order))
        {
            $order = $this->_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    private function count_filtered()
    {
        $this->_get_datatables_query();
        return $this->db->get()->num_rows();
    }

    private function count_all()
    {
        return $this->db->from($this->table)->count_all_results();
    }

    function get_datatables()
    {
        $data = array();

        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        
        $no = $_POST['start'];
        foreach ($query->result() as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->name;
            $row[] = $field->modules;
            $row[] = $field->class;
            $row[] = $field->link;
            $row[] = '<i class="fa '.$field->icon.'"></i>';
            // $row[] = $field->is_active;
            // $row[] = $field->is_arrage;
            $row[] = menu_name($field->is_parent);
            $row[] = $field->type; 
            $row[] = '<center>'. 
            anchor(site_url($this->_mod.'/menu/read/'.$field->id),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-info btn-sm')).
            anchor(site_url($this->_mod.'/menu/update/'.$field->id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')).
            '<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModalDelete'.$field->id.'" title="delete"><i class="fa fa-trash-o"></i></button>'.'</center>'.
            $this->modal_delete($this->_set_value(),$field->id);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->count_all(),
            "recordsFiltered" => $this->count_filtered(),
            "data" => $data,
        );
        return json_encode($output);
    }

    // get data with limit and search
    function get_menu($tabel,$pk,$limit = 0, $start = 0, $q = NULL, $fields = NULL, $order = 'DESC', $join = NULL, $type_join = NULL) {
        
        $this->db->order_by($pk, $order);
        if (is_array($fields)) {
            foreach($fields as $key => $value){
                if ($key[0]) {
                    $this->db->like($value, $q);              
                }
                $this->db->or_like($value, $q);
            }
        }
        if (is_array($join)) {
            foreach($join as $tb_join => $field){
                if ($type_join != NULL) {
                   $this->db->join($tabel,$field,$type_join);
                }else{
                    $this->db->join($tb_join,$field);
                }
            }
        }
        if (is_numeric($limit)&&$limit>0) {
            return $this->db->limit($limit, $start)->get($tabel)->result();
        }else{
            return $this->db->get($tabel)->num_rows();
        }
    }


    //2. _rules
    function _rules() 
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('link', 'link', 'trim|required');
		$this->form_validation->set_rules('icon', 'icon', 'trim|required');
		$this->form_validation->set_rules('is_active', 'is active', 'trim|required');
		$this->form_validation->set_rules('is_arrage', 'is arrage', 'trim|required');
		$this->form_validation->set_rules('is_parent', 'is parent', 'trim');
		$this->form_validation->set_rules('type', 'type', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    //3. _set_value
    function _set_value($row=NULL)
    {
        if($row){
            $data = [
                'button' => 'Update',
                'action' => site_url($this->_mod.'/menu/update_action'),
				'id' => set_value('id', $row->id),
				'name' => set_value('name', $row->name),
                'modules' => set_value('modules', $row->modules),
                'class' => set_value('class', $row->class),
				'link' => set_value('link', $row->link),
				'icon' => set_value('icon', $row->icon),
				'is_active' => set_value('is_active', $row->is_active),
				'is_arrage' => set_value('is_arrage', $row->is_arrage),
				'is_parent' => set_value('is_parent', $row->is_parent),
				'type' => set_value('type', $row->type),
			];
        }else{
            $data = [
                'button' => 'Save',
                'action' => site_url($this->_mod.'/menu/create_action'),
				'id' => set_value('id'),
				'name' => set_value('name'),
                'modules' => set_value('modules'),
                'class' => set_value('class'),                
				'link' => set_value('link'),
				'icon' => set_value('icon'),
				'is_active' => set_value('is_active'),
				'is_arrage' => set_value('is_arrage'),
				'is_parent' => set_value('is_parent'),
				'type' => set_value('type'),
			];
        }
        $data['delete'] = site_url($this->_mod.'/menu/delete');
        return $data;
    }

    //4. _get_post
    function _get_post()
    {
        $data = [
            'modules' => $this->input->post('modules',TRUE),
            'class' => $this->input->post('class',TRUE),
			'name' => $this->input->post('name',TRUE),
			'link' => $this->input->post('link',TRUE),
			'icon' => $this->input->post('icon',TRUE),
			'is_active' => $this->input->post('is_active',TRUE),
			'is_arrage' => $this->input->post('is_arrage',TRUE),
			'is_parent' => $this->input->post('is_parent',TRUE),
			'type' => $this->input->post('type',TRUE),
		];
        return $data;
    }


}

/* End of file Menu_model.php */
/* Location: ./application/models/Menu_model.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2018-04-08 10:24:27 */
/* http://shintackeror.web.id thanks to http://harviacode.com */