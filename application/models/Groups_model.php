<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Groups_model extends MY_Model
{

    private $table = 'groups';
    private $id    = 'id';
    private $order = 'DESC';
    private $field = [ 'name','description', ];
    //variabel untuk datatable
    private $_column_order;
    private $_column_search;
    private $_order; 

    function __construct()
    {
        parent::__construct();
        $this->_column_order = [ NULL,'name','description', ];
        $this->_column_search = $this->field;
        $this->_order = [ $this->id=>$this->order ];
    }

    function structure()
    {   
        return ['table' => $this->table, 'pk' => $this->id, 'field' => $this->field];
    }

    //datatable
    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->_column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item,$_POST['search']['value']);
                }
                if(count($this->_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->_order))
        {
            $order = $this->_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    private function count_filtered()
    {
        $this->_get_datatables_query();
        return $this->db->get()->num_rows();
    }

    private function count_all()
    {
        return $this->db->from($this->table)->count_all_results();
    }

    function get_datatables()
    {
        $data = array();

        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        
        $no = $_POST['start'];
        foreach ($query->result() as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->name;
            $row[] = $field->description; 
            $row[] = 
            anchor(site_url($this->_mod.'/tes/read/'.$field->id),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-info btn-sm')).
            anchor(site_url($this->_mod.'/tes/update/'.$field->id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')).
            '<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModalDelete'.$field->id.'" title="delete"><i class="fa fa-trash-o"></i></button>'.
            $this->modal_delete($this->_set_value(),$field->id);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->count_all(),
            "recordsFiltered" => $this->count_filtered(),
            "data" => $data,
        );
        return json_encode($output);
    }

    function insert_hak_akses($data)
    {
        if (isset($data['read']) && $data!=NULL) {
            $group_id = $data['id'];

            //delete akses
            $delete = $this->db->where('group_id',$group_id)->delete('user_groups_menu');
            
            //insert read
            if ($delete) {
                $read = $list = $create = $update = $delete = array();

                foreach ($data['read'] as $key => $value) {
                    $read[] = [
                        'group_id' => $group_id,
                        'menu_id'  => $value,
                        'lihat'    => $value,
                    ];
                }
                $insert_read = $this->db->insert_batch('user_groups_menu', $read);

                if ($insert_read>0) {
                    //update akses list
                    if (isset($data['list'])) {
                        foreach ($data['list'] as $key => $value1) {                        
                            $this->db->where(['group_id'=>$group_id,'menu_id'=>$value1])->update('user_groups_menu',[ 'daftar' => $value1 ]);
                                //update akses list
                        }
                    }
                    if (isset($data['create'])) {
                        foreach ($data['create'] as $key => $value1) {                        
                            $this->db->where(['group_id'=>$group_id,'menu_id'=>$value1])->update('user_groups_menu',[ 'tambah' => $value1 ]);
                                //update akses list
                        }
                    }
                    if (isset($data['update'])) {
                        foreach ($data['update'] as $key => $value1) {                        
                            $this->db->where(['group_id'=>$group_id,'menu_id'=>$value1])->update('user_groups_menu',[ 'ubah' => $value1 ]);
                                //update akses list
                        }
                    }
                    if (isset($data['delete'])) {
                        foreach ($data['delete'] as $key => $value1) {                        
                            $this->db->where(['group_id'=>$group_id,'menu_id'=>$value1])->update('user_groups_menu',[ 'hapus' => $value1 ]);
                                //update akses list
                        }
                    }
                    return TRUE;
                }
            }
        }return FALSE;
    }
    //2. _rules
    function _rules() 
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'name', 'trim|required');
		$this->form_validation->set_rules('description', 'description', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    //3. _set_value
    function _set_value($row=NULL)
    {
        if($row){
            $data = [
                'button' => 'Update',
                'action' => site_url($this->_mod.'/groups/update_action'),
				'id' => set_value('id', $row->id),
				'name' => set_value('name', $row->name),
				'description' => set_value('description', $row->description),
			];
        }else{
            $data = [
                'button' => 'Save',
                'action' => site_url($this->_mod.'/groups/create_action'),
				'id' => set_value('id'),
				'name' => set_value('name'),
				'description' => set_value('description'),
			];
        }
        $data['delete'] = site_url($this->_mod.'/groups/delete');
        return $data;
    }

    //4. _get_post
    function _get_post()
    {
        $data = [
			'name' => $this->input->post('name',TRUE),
			'description' => $this->input->post('description',TRUE),
		];
        return $data;
    }


}

/* End of file Groups_model.php */
/* Location: ./application/models/Groups_model.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2018-03-31 14:55:52 */
/* http://shintackeror.web.id thanks to http://harviacode.com */