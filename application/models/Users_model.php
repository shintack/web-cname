<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends MY_Model
{

    private $table = 'users';
    private $id    = 'id';
    private $order = 'DESC';
    private $field = [ 'ip_address','username','password','salt','email','activation_code','forgotten_password_code','forgotten_password_time','remember_code','created_on','last_login','active','first_name','last_name','company','phone', ];
    //variabel untuk datatable
    private $_column_order;
    private $_column_search;
    private $_order; 

    function __construct()
    {
        parent::__construct();
        $this->_column_order = [ NULL,'ip_address','username','password','salt','email','activation_code','forgotten_password_code','forgotten_password_time','remember_code','created_on','last_login','active','first_name','last_name','company','phone', ];
        $this->_column_search = $this->field;
        $this->_order = [ $this->id=>$this->order ];
    }

    function structure()
    {   
        return ['table' => $this->table, 'pk' => $this->id, 'field' => $this->field];
    }

    //datatable
    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->_column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item,$_POST['search']['value']);
                }
                if(count($this->_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->_order))
        {
            $order = $this->_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    private function count_filtered()
    {
        $this->_get_datatables_query();
        return $this->db->get()->num_rows();
    }

    private function count_all()
    {
        return $this->db->from($this->table)->count_all_results();
    }

    function get_datatables()
    {
        $data = array();

        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        
        $no = $_POST['start'];
        foreach ($query->result() as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->ip_address;
            $row[] = $field->username;
            $row[] = $field->email;
            $row[] = $field->created_on;
            $row[] = $field->last_login;
            $act = ($field->active==='1') ? '<a class="btn btn-success btn-sm" title="aktif"> <i class="fa fa-play"></i> </a>' : '<a class="btn btn-warning btn-sm" title="nonaktif"> <i class="fa fa-stop"></i> </a>';
            $row[] = '<center>'.$act.'</center>';//($field->active) ? '<>';
            $row[] = $field->first_name.' '.$field->last_name;
            // $row[] = $field->phone; 
            $row[] =  
            anchor(site_url($this->_mod.'/users/read/'.$field->id),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-info btn-sm')).'&nbsp;'.
            anchor(site_url($this->_mod.'/users/update/'.$field->id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')).'&nbsp;'.
            '<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#myModalDelete'.$field->id.'" title="delete"><i class="fa fa-trash-o"></i></button>'.
            $this->modal_delete($this->_set_value(),$field->id);
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->count_all(),
            "recordsFiltered" => $this->count_filtered(),
            "data" => $data,
        );
        return json_encode($output);
    }


    //2. _rules
    function _rules() 
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('ip_address', 'ip address', 'trim|required');
		$this->form_validation->set_rules('username', 'username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		$this->form_validation->set_rules('salt', 'salt', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required');
		$this->form_validation->set_rules('activation_code', 'activation code', 'trim|required');
		$this->form_validation->set_rules('forgotten_password_code', 'forgotten password code', 'trim|required');
		$this->form_validation->set_rules('forgotten_password_time', 'forgotten password time', 'trim|required');
		$this->form_validation->set_rules('remember_code', 'remember code', 'trim|required');
		$this->form_validation->set_rules('created_on', 'created on', 'trim|required');
		$this->form_validation->set_rules('last_login', 'last login', 'trim|required');
		$this->form_validation->set_rules('active', 'active', 'trim|required');
		$this->form_validation->set_rules('first_name', 'first name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'last name', 'trim|required');
		$this->form_validation->set_rules('company', 'company', 'trim|required');
		$this->form_validation->set_rules('phone', 'phone', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    //3. _set_value
    function _set_value($row=NULL)
    {
        if($row){
            $data = [
                'button' => 'Update',
                'action' => site_url($this->_mod.'/users/update_action'),
				'id' => set_value('id', $row->id),
				'ip_address' => set_value('ip_address', $row->ip_address),
				'username' => set_value('username', $row->username),
				'password' => set_value('password', $row->password),
				'salt' => set_value('salt', $row->salt),
				'email' => set_value('email', $row->email),
				'activation_code' => set_value('activation_code', $row->activation_code),
				'forgotten_password_code' => set_value('forgotten_password_code', $row->forgotten_password_code),
				'forgotten_password_time' => set_value('forgotten_password_time', $row->forgotten_password_time),
				'remember_code' => set_value('remember_code', $row->remember_code),
				'created_on' => set_value('created_on', $row->created_on),
				'last_login' => set_value('last_login', $row->last_login),
				'active' => set_value('active', $row->active),
				'first_name' => set_value('first_name', $row->first_name),
				'last_name' => set_value('last_name', $row->last_name),
				'company' => set_value('company', $row->company),
				'phone' => set_value('phone', $row->phone),
			];
        }else{
            $data = [
                'button' => 'Save',
                'action' => site_url($this->_mod.'/users/create_action'),
				'id' => set_value('id'),
				'ip_address' => set_value('ip_address'),
				'username' => set_value('username'),
				'password' => set_value('password'),
				'salt' => set_value('salt'),
				'email' => set_value('email'),
				'activation_code' => set_value('activation_code'),
				'forgotten_password_code' => set_value('forgotten_password_code'),
				'forgotten_password_time' => set_value('forgotten_password_time'),
				'remember_code' => set_value('remember_code'),
				'created_on' => set_value('created_on'),
				'last_login' => set_value('last_login'),
				'active' => set_value('active'),
				'first_name' => set_value('first_name'),
				'last_name' => set_value('last_name'),
				'company' => set_value('company'),
				'phone' => set_value('phone'),
			];
        }
        $data['delete'] = site_url($this->_mod.'/users/delete');
        return $data;
    }

    //4. _get_post
    function _get_post()
    {
        $data = [
			'ip_address' => $this->input->post('ip_address',TRUE),
			'username' => $this->input->post('username',TRUE),
			'password' => $this->input->post('password',TRUE),
			'salt' => $this->input->post('salt',TRUE),
			'email' => $this->input->post('email',TRUE),
			'activation_code' => $this->input->post('activation_code',TRUE),
			'forgotten_password_code' => $this->input->post('forgotten_password_code',TRUE),
			'forgotten_password_time' => $this->input->post('forgotten_password_time',TRUE),
			'remember_code' => $this->input->post('remember_code',TRUE),
			'created_on' => $this->input->post('created_on',TRUE),
			'last_login' => $this->input->post('last_login',TRUE),
			'active' => $this->input->post('active',TRUE),
			'first_name' => $this->input->post('first_name',TRUE),
			'last_name' => $this->input->post('last_name',TRUE),
			'company' => $this->input->post('company',TRUE),
			'phone' => $this->input->post('phone',TRUE),
		];
        return $data;
    }


}

/* End of file Users_model.php */
/* Location: ./application/models/Users_model.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2018-06-03 01:09:12 */
/* http://shintackeror.web.id thanks to http://harviacode.com */