<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Riset market dan produk laris">
    <meta name="author" content="babastudio.com">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="https://www.babastudio.com/assets/front_page/img/icon/favicon_515ad23b30404b883798e5bebafbcc45.png">
    <!-- <link rel="icon" type="image/png" sizes="16x16" href="https://wrappixel.com/demos/admin-templates/material-pro/assets/images/favicon.png"> -->
    <title>SiteBite Deplaza.id</title>
    <!-- Bootstrap Core CSS -->
    <link href="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- datatables -->
    <!-- <link href="https://wrappixel.com/demos/admin-templates/material-pro/plugins/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet"> -->
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/rowreorder/1.2.5/css/rowReorder.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">
    <!-- sewat alert -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=base_url('template/material/')?>css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?=base_url('template/material/')?>css/colors/blue.css" id="theme" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://www.babastudio.com/assets/floating-whatsapp/floating-wpp.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/jquery/jquery.min.js"></script>

</head>

<body class="fix-header fix-sidebar card-no-border">
<script>
var base_url = "<?=base_url()?>";
</script>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <?php if($_SERVER['SERVER_NAME']!="localhost"): ?>
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <?php endif; ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?php
        /*
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        */
        $this->load->view('layout/material/top_menu');
        /*
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        */

        /*
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        */
        $this->load->view('layout/material/side_menu');
        /*
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        */

        /*
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        */
        ?>
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">

                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor"><?=isset($title)?$title:''?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard2</li>
                        </ol>
                    </div>
                    <!-- <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                    <h4 class="m-t-0 text-info">$58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div>
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>LAST MONTH</small></h6>
                                    <h4 class="m-t-0 text-primary">$48,356</h4></div>
                                <div class="spark-chart">
                                    <div id="lastmonthchart"></div>
                                </div>
                            </div>
                            <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div>
                        </div>
                    </div> -->
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <?=isset($contents)?$contents:show_404();?>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <?php //$this->load->view('layout/material/right_menu');?>

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <div class="floating-wpp" style="z-index:9999999999;"></div>
            <?php $this->load->view('layout/material/footer');?>

        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/popper/popper.min.js"></script>
    <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url('template/material/')?>js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url('template/material/')?>js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url('template/material/')?>js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--stickey kit -->
    <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url('template/material/')?>js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- This is data table -->
    <!-- <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/datatables/datatables.min.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/rowreorder/1.2.5/js/dataTables.rowReorder.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    <!-- sweat alert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>

    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="https://wrappixel.com/demos/admin-templates/material-pro/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>

    <script type="text/javascript" src="https://www.babastudio.com/assets/floating-whatsapp/floating-wpp.min.js"></script>

    <script>
        $(function(){
            var table = $('.datatable-responsive').DataTable( {
                            rowReorder: {
                                selector: 'td:nth-child(3)'
                            },
                            responsive: true
                        } );

            $('.datatable').DataTable();

            $('.datatbles-hidesearch').dataTable( {
                "searching": false
            } );

        });

        $( document ).ready(function() {
			// $('#modalPerson').modal('show');
		    $('.floating-wpp').floatingWhatsApp({
	            phone: '6281398130236', //'62895333607909',
	            popupMessage: 'Hi, ini Rani... minat materi kursus apa? Rani ada Beasiswa Kursus 20%',
	            showPopup: true,
	            //position: 'right',
	            //autoOpen: false,
	            //autoOpenTimer: 4000,
	            message: '',
	            //headerColor: 'orange',
	            headerTitle: 'Whatsapp Chat',
	        });
		});
    </script>

    <?php if($_SERVER['SERVER_NAME']!='localhost'):?>
    <script async type="text/javascript" src="https://www.babastudio.com/assets/front_page/js/analytics.js"></script>
    <?php endif; ?>
</body>
</html>
