<!-- FOOTER -->
<div class="row bg-grey" id="footer">
  <div class="section">
    <div class="col-md-6">
      <h2 class="font-lg font-bold font-blue">Aplikasi dePlaza di support oleh www.babastudio.com</h2>
      <p class="font-light font-sm">Let's get in touch on any of these platforms.</p>
    </div>
    <div class="col-md-6 col-xs-12 text-right socials font-white">
      <button data-toggle="tooltip" title="Follow Us" data-placement="top" class="bg-blue-lighten"><a href="#"><i class="fa fa-twitter"></i></a></button>
      <button data-toggle="tooltip" title="Follow Us" data-placement="top" class="bg-blue"><a href="#"><i class="fa fa-facebook"></i></a></button>
      <button data-toggle="tooltip" title="Follow Us" data-placement="top" class="bg-pink"><a href="#"><i class="fa fa-google-plus"></i></a></button>
      <button data-toggle="tooltip" title="Follow Us" data-placement="top" class="bg-black"><a href="#"><i class="fa fa-instagram"></i></a></button>
    </div>
    <div class="col-md-12"><hr></div>
    <div class="col-md-5 col-xs-12 font-sm">
      &copy; 2019. <a href="#" class="font-blue">All rights reserved.</a>
    </div>
    <div class="col-md-7 col-xs-12 text-right foot-list">
      <ul class="font-black font-medium font-sm">
        <li class="active"><a href="#">Keuntungan</a></li>
        <li><a href="#">Keunggulan</a></li>
        <li><a href="#">Harga Paket</a></li>
        <li><a href="#">Apa itu dePLAZA</a></li>
        <li><a href="#">Produk</a></li>
      </ul>
    </div>
  </div>
</div>
<!-- FOOTER END -->
