<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<title>dePLAZA.id - Automatisasi Bisnis Online Anda</title>
	<link rel="icon" href="<?=$this->path_assets?>assets/images/favicon.png">

	<link rel="stylesheet" type="text/css" href="<?=$this->path_assets?>assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->path_assets?>assets/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->path_assets?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->path_assets?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->path_assets?>assets/css/swiper.min.css">
	<link rel="stylesheet" type="text/css" href="<?=$this->path_assets?>assets/css/aos.css">

	<?php /*
	<link rel="preload" as="style" href="<?=$this->path_assets?>assets/css/style.css" onload="this.rel='stylesheet'">
	<link rel="preload" as="style" href="<?=$this->path_assets?>assets/css/responsive.css" onload="this.rel='stylesheet'">
	<link rel="preload" as="style" href="<?=$this->path_assets?>assets/css/bootstrap.min.css" onload="this.rel='stylesheet'">
	<link rel="preload" as="style" href="<?=$this->path_assets?>assets/css/font-awesome.min.css" onload="this.rel='stylesheet'">
	<link rel="preload" as="style" href="<?=$this->path_assets?>assets/css/swiper.min.css" onload="this.rel='stylesheet'">
	<link rel="preload" as="style" href="<?=$this->path_assets?>assets/css/aos.css" onload="this.rel='stylesheet'">

	<!-- Latest compiled and minified CSS -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<!-- Optional theme -->
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> -->
	<!-- Latest compiled and minified JavaScript -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
	<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

	*/ ?>
	<script type="text/javascript" src="<?=$this->path_assets?>assets/js/jquery.js"></script>

	<script type="text/javascript" src="<?=$this->path_assets?>assets/js/bootstrap.min.js"></script>

	<style type="text/css">
	.preloader {
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background-color: #fff;
	}
	.preloader .loading {
		position: absolute;
		left: 50%;
		top: 50%;
		transform: translate(-50%,-50%);
		font: 14px arial;
	}
	</style>
	<script>
	$(document).ready(function(){
		$(".preloader").fadeOut();
	})
	</script>

</head>
<body>

<div class="preloader">
  <div class="loading">
    <img src="<?=base_url('assets/loading.gif')?>" width="80">
    <p>Harap Tunggu</p>
  </div>
</div>
<script type="text/javascript">
	var base_url 			= "<?=base_url()?>";
	var base_url_api 	= "<?=$_SERVER['SERVER_NAME']=='localhost'?'http://localhost/deplaza/':'https://api.deplaza.id/'?>";
</script>


  <?php $this->load->view('layout/sitebeat/side_menu_mobile') ?>


	<div class="overlay-menu bg-ilang"></div>

	<div class="container-fluid bg-white ">

    <?php $this->load->view('layout/sitebeat/top_menu') ?>

    <?=isset($contents)?$contents:show_404();?>

		<?php $this->load->view('layout/sitebeat/footer') ?>
	</div>


	<!-- BACK TO TOP -->
	<a href="#header" id="backtotop" class="bg-blue"><i class="fa fa-arrow-up fa-2x"></i></a>
	<!-- BACK TO TOP END -->

	<!-- <script type="text/javascript" src="<?=$this->path_assets?>assets/js/browser-polyfill.min.js"></script> -->
	<script type="text/javascript" src="<?=$this->path_assets?>assets/js/swiper.min.js"></script>
	<script type="text/javascript" src="<?=$this->path_assets?>assets/js/aos.js"></script>
	<script type="text/javascript" src="<?=$this->path_assets?>assets/js/global.js"></script>
	<script type="text/javascript" src="<?=$this->path_assets?>assets/js/responsive.js"></script>

	<script type="text/javascript" src="<?=$this->path_assets?>assets/js/blazy.min.js"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

	<script type="text/javascript">

	$(function(){

		AOS.init();

		$('[name="daftar-domain-input"]').on('input',function(e){
			$('[name="domain_name"]').val(this.value);
		});

		$('.link-domain').click(function(){
			postBeforeRefister();
		});

		var bLazy = new Blazy();

	});

	// var bLazy = new Blazy({
	// 	// Options
	// });

	function pilihPaket(paket_id) {
		$('[name="paket_id"]').val(paket_id);
	}

	function linkDomain(type) {
		$('[name="domain_type"]').val(type);
	}

	function validateDomain(the_domain)
  {
    // strip off "http://" and/or "www."
    the_domain = the_domain.replace("http://","");
    the_domain = the_domain.replace("www.","");
    var reg = /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,}$/;
    return reg.test(the_domain);
  }

	function postBeforeRefister() {
		var paket_id 			= $('[name="paket_id"]').val();
		var domain_type 	= $('[name="domain_type"]').val();
		var domain_name 	= $('[name="domain_name"]').val();
		if ( paket_id!='' && domain_type!='' && domain_name!='' ) {
			$.post(base_url+'pre-register',{
				paket_id:paket_id,
				domain_type:domain_type,
				domain_name:domain_name,
			},function(data,status){
				if ( status=='success' ) {
					window.location.href = base_url+'register';
				}else{
					console.log(data);
					swal('Error','Something went wrong!','error');
				}
			},'json');
		}else{
			swal('Info','Silahkan pilih pakt dan domain','info');
		}
	}

	function payMethod(type) {
		if (type=='02') {
			$("#02").addClass('active');
			$("#03").removeClass('active');
			$("#tab-02").show('slow');
			$("#tab-03").hide('slow');
		}else if(type=='03'){
			$("#03").addClass('active');
			$("#02").removeClass('active');
			$("#tab-02").hide('slow');
			$("#tab-03").show('slow');
		}
		$('[name="pay_code"]').val(type);
		$("#tab-button").show('slow');
		// alert(type);
	}
	</script>
</body>
</html>
