<!-- NAVBAR -->
<div class="row intop <?=isset($transparent)?'':'bg-blue-gradient not-home'?>" id="navbar">
  <div class="section" data-aos="fade">
    <div class="col-md-2">
      <a href="<?=base_url()?>">
        <img src="<?=$this->path_assets?>assets/images/logo.png" width="100%">
      </a>
    </div>
    <div class="col-md-10">
      <ul class="font-white font-xs">
        <li class="active"><a href="#header">Keuntungan</a></li>
        <li><a href="#reason">Keunggulan</a></li>
        <li><a href="#getstarted">Harga Paket</a></li>
        <li><a href="#">Apa itu dePLAZA</a></li>
        <li><a href="<?=base_url('produk')?>">Produk</a></li>
        <?php if(cek_logged_in()): ?>
          <a href="<?=site_url('dashboard')?>" class="btn btn-outline btn-rounded font-bold">Dashboard</a>
        <?php else: ?>
          <a href="<?=site_url('login')?>" class="btn btn-outline btn-rounded font-bold">Login</a>
          <a href="#getstarted" class="btn btn-outline btn-rounded font-bold">Daftar</a>
          <!-- <a href="<?=site_url('register')?>" class="btn btn-outline btn-rounded font-bold">Daftar</a> -->
        <?php endif; ?>
      </ul>
    </div>
  </div>
</div>
<!-- NAVBAR END -->

<!-- NAV MOBILE -->
<div class="row bg-blue-gradient" id="nav-mob">
  <img src="<?=$this->path_assets?>assets/images/logo.png" class="col-xs-7">
  <div class="col-xs-2 col-xs-offset-3 text-center" id="btn-menu">
    <i class="fa fa-bars font-white fa-2x"></i>
  </div>
</div>
<!-- END NAV MOBILE -->
