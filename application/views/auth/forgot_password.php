<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Babastudio | Log in</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url() ?>template/admin/font-awesome-4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url() ?>template/admin/ionicons-2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>template/admin/plugins/iCheck/square/blue.css">

        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
        
        <script src="<?php echo base_url('template/admin/plugins/sweetalert/sweetalert.min.js') ?>"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="<?php echo base_url(); ?>"><b>SIMPEG</b></a> <br>
                <a href="<?php echo base_url(); ?>"><b>BABA</b>STUDIO</a>
            </div><!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">
                    <?php if(!is_array($message)){
                        echo $message ;
                    }else{
                        $this->load->view('sweet');    
                    }?>
                </p>
                
                  <h1><?php echo lang('forgot_password_heading');?></h1>
                  <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

                  <div id="infoMessage"><?php echo $message;?></div>

                  <?php echo form_open("auth/forgot_password");?>
                        <p>
                              <label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
                              <?php echo form_input($identity);?>
                        </p>

                        <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'),['class'=>'btn btn-block btn-primary']);?></p>

                  <?php echo form_close();?>
                  <div class="row">
                    <div class="col-xs-6">
                        <a href="register">
                        <button type="button" class="btn btn-default btn-block btn-flat"><i class="fa fa-user"></i> REGISTER </button>
                        </a>
                    </div><!-- /.col -->

                    <div class="col-xs-6">
                        <a href="<?=base_url('login')?>">
                        <button name="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> LOGIN</button>
                        </a>
                    </div><!-- /.col -->
                </div>



            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

             
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>


    </body>
</html>


