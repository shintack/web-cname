<!-- <div style="margin-top: -30px;">
    <h2><?php echo lang('index_heading');?></h2>
    <?php echo create_breadcrumb('Home', base_url('auth')); ?>    
</div> -->

<div id="infoMessage"><?php echo $message;?></div>

<table class="table table-bordered table-striped table-hover" style="margin-bottom: 10px">
	<thead class="btn-primary">
		<tr>
			<th><?php echo lang('index_fname_th');?></th>
			<th><?php echo lang('index_lname_th');?></th>
			<th><?php echo lang('index_email_th');?></th>
			<th><?php echo lang('index_groups_th');?></th>
			<th><?php echo lang('index_action_th');?></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($users as $user):?>
		<tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td>
				<?php foreach ($user->groups as $group):?>
					<?php echo anchor("auth/edit_group/".$group->id, htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')) ;?><br />
                <?php endforeach?>
			</td>
			<td> <center>
          <?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, '<i class="fa fa-play"></i>' ,['title'=>lang('index_active_link'),'class'=>'btn btn-success btn-sm']) : anchor("auth/activate/". $user->id, '<i class="fa fa-stop"></i>' ,['title'=>lang('index_inactive_link'),'class'=>'btn btn-warning btn-sm']);?>
          <?php echo anchor("auth/edit_user/".$user->id, '<i class="fa fa-edit"></i>',['title'=>'Edit','class'=>'btn btn-info btn-sm']) ;?></td>
        </center>
    </tr>
	<?php endforeach;?>
	</tbody>
</table>

<p><?php echo anchor('auth/create_user', '<i class="fa fa-plus"></i> '.lang('index_create_user_link'),['title'=>'Add User','class'=>'btn btn-primary'])?> | <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalAdd"> <i class="fa fa-plus"></i> <?=lang('index_create_group_link')?></button> <?php //echo anchor('auth/create_group', '<i class="fa fa-plus"></i> '.lang('index_create_group_link'),['title'=>'Add Group','class'=>'btn btn-success'])?></p>



<!-- Modal -->
<div id="myModalAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
<?php echo form_open("auth/create_group"); ?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Group</h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">

          <tr>
            <td> <label>Name </label> <i> <?php echo form_error('name') ?> </i></td>
            <td><input type="text" class="form-control" name="group_name" id="group_name" placeholder="Group Name" value="" /></td>
          </tr>
          <tr>
            <td> <label>Description </label> <i> <?php echo form_error('description') ?> </i></td>
            <td><input type="text" class="form-control" name="description" id="description" placeholder="Description" value="" /></td>
          </tr>
        </table>
        
      </div>
      <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
<?php form_close(); ?>

  </div>
</div>

<script>
$(document).ready( function () {
    $('.table').DataTable();
} );
</script>
