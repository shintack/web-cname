
<div id="infoMessage"><?php echo $message;?></div>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo lang('create_group_subheading');?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
  
    <?php echo form_open(uri_string());?>

          <p>
                <?php echo lang('edit_user_fname_label', 'first_name');?> <br />
                <?php echo form_input($first_name);?>
          </p>

          <p>
                <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
                <?php echo form_input($last_name);?>
          </p>

          <p>
                <?php echo lang('edit_user_company_label', 'company');?> <br />
                <?php echo form_input($company);?>
          </p>

          <p>
                <?php echo lang('edit_user_phone_label', 'phone');?> <br />
                <?php echo form_input($phone);?>
          </p>

          <p>
                <?php echo lang('edit_user_password_label', 'password');?> <br />
                <?php echo form_input($password);?>
          </p>

          <p>
                <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
                <?php echo form_input($password_confirm);?>
          </p>

          <?php if ($this->ion_auth->is_admin()): ?>

              <h3><?php echo lang('edit_user_groups_heading');?></h3>
              <?php foreach ($groups as $group):?>
                  <label class="checkbox" style="margin-left: 50px; ">
                  <?php
                      $gID=$group['id'];
                      $checked = null;
                      $item = null;
                      foreach($currentGroups as $grp) {
                          if ($gID == $grp->id) {
                              $checked= ' checked="checked"';
                          break;
                          }
                      }
                  ?>
                  <input type="checkbox" name="groups[]" class="" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                  <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                  </label>
              <?php endforeach?>
              
          <?php endif ?>
          <?php echo form_hidden('id', $user->id);?>
          <?php echo form_hidden($csrf); ?>
          <hr>
          <p><?php echo form_submit('submit', lang('edit_user_submit_btn'),['class'=>'btn btn-primary']);?>
            <a href="<?=base_url('auth')?>" class="btn btn-default">Cancel</a>
          </p>

    <?php echo form_close();?>
    </div>
</div> 



