<!-- page script -->
<?php if ( is_array($this->session->userdata("message")) && $this->session->userdata("message") <> NULL): ?>
    <script>
        swal({
            title: "<?php echo $this->session->userdata('message')['title']; ?>",
            text: "<?php echo $this->session->userdata('message')['message']; ?>",
            icon: "<?php echo $this->session->userdata('message')['icon']; ?>",
            timer: 1500,
            showConfirmButton: false,
            type: 'success'
        });
    </script>
<?php endif; ?>

<script>

$(document).ready(function(){
  setTimeout(function(){
    $('body').addClass('loaded');
    $('h1').css('color','#222222')
  }, 1000);
});

function goBack() {
    window.history.back();
}

function sweat(title,text,icon)
{
    swal({
        title: title,
        text: text,
        icon: icon,
        //timer: 1500,
        button: "OK!",
    });
}


</script>
