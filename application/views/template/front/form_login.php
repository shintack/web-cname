<?php $dir_template = base_url('template/frontend/web/'); ?>
  <!-- Body Login -->
  <div id="inner-login">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-xs-12 col-sm-4">
          <div class="registration-form">
            <?php echo form_open('auth/login',['class'=>'login-form']); ?>
              <h2 class="title">Masuk Ke Perizinan Online BPTJ</h2>
                <?php if(!is_array($message) && $message!=NULL){
                    echo '
                    <div class="alert alert-info alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Info!</strong> '.$message .'
                    </div>';
                }else{
                    $this->load->view('sweet');    
                }?>
              <div class="form-group">
                <label for="username">Username</label>
                <?php echo form_input($identity); ?>
                <!-- <input type="text" class="form-control input-md" id="username" placeholder="Masukkan Email atau Username"> -->
                <i class="fa fa-user-o field-icon" aria-hidden="true"></i>
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <?php echo form_input($password); ?>
                <!-- <input id="password-field" type="password" class="form-control" name="password" placeholder="Masukkan Password"> -->
                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
              </div>
              <?php echo $this->recaptcha->render(); ?>
              <button type="submit" class="btn btn-default login">Login</button>
              <a href="<?=base_url('register');?>">
                <button type="button" class="btn btn-default reg">REGISTRASI</button>
              </a> 
              <div class="footer-form">
                <div class="checkbox">
                  <label>
                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> 
                    Remember Me
                  </label>
                </div>
                <div class="link">
                  <a href="#">Forgot Password?</a>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-8 col-xs-12 col-sm-8">
          <div class="slide-home">
            <div id="Reg_slide" class="slider-reg">
              <div class="owl-carousel reg-home owl-theme">
                <div class="item">
                  <img src=<?=$dir_template?>"assets/img/reg-slider.jpg" alt="slider" class="img-responsive">
                </div>
                <div class="item">
                  <img src="<?=$dir_template?>assets/img/reg-slider.jpg" alt="slider" class="img-responsive">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('template/front/footer_home')?>

  </div>
  <!-- End Body Login -->

