
  <!-- Body Login -->
  <div id="inner-login" class="reg-page">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
          <div class="registration-form">
              <?php //$this->load->view('admin/companies/datakota') ?>

            <?php echo form_open("auth/register",['class'=>'login-form']);?>
              <div class="header-form">
                <h2 class="title">Registrasi Pemohon</h2>
                <p>Untuk aktifasi aplikasi BPTJ Mbile, masukan data diri<br>Badan Usaha Perusahaan Anda.</p>
              </div>
              <?php if(!is_array($message) && $message!=NULL){
                  echo '
                  <div class="alert alert-info alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Info!</strong> '.$message .'
                  </div>';
              }else{
                  $this->load->view('sweet');
              }?>
              <div class="badan-usaha">
                <p class="title">1. Badan Usaha</p>
                <div class="form-inline">
                  <div class="form-group first-input">
                    <?php echo form_input($company_name);?>
                    <span class="floating-label">Nama Badan Usaha</span>
                    <?php //echo form_error('company_name') ?>
                  </div>
                  <div class="form-group npwp">
                    <?php echo form_input($company_npwp);?>
                    <span class="floating-label">NPWP*</span>
                    <?php //echo form_error('company_npwp') ?>
                  </div>
                </div>
                <div class="form-group alamat">
                  <?php echo form_input($company_address);?>
                  <span class="floating-label">Alamat Badan Usaha*</span>
                  <?php //echo form_error('company_address') ?>
                </div>
                <?php //$this->load->view('admin/companies/datakota') ?>

                <div class="form-inline last-inline">
                  <div class="form-group" style="width:220px;">
                      <?php
                        /*
                        foreach ($provinsi as $row)
                        {
                            $result[0]= '-Pilih Propinsi-';
                            $result[$row->id_prov]= $row->nama;
                        }
                        $option_propinsi = $result;
                        $add = [
                            'class' => 'form-control select2 provinsi',
                            'id'    => 'provinsi_id',
                        ];
                        echo form_dropdown("provinsi",$option_propinsi,"",$add);
                        echo form_dropdown("kota_id",array('Pilih Kota / Kabupaten'=>'Pilih Propinsi Dahulu'),'','disabled');

                        <script type="text/javascript">

                        $("#provinsi_id").change(function(){
                                var propinsi_id = {propinsi_id:$("#provinsi_id").val()};
                                $.ajax({
                                        type: "POST",
                                        url : "<?php echo site_url('daerah/datakota')?>",
                                        data: provinsi_id,
                                        success: function(msg){
                                            $('#kota').html(msg);
                                        }
                                    });
                        });
                       </script>
                       */
                        ?>
                      <select name="provinsi" id="provinsi" required onchange="ajaxkota(this.value)" class="form-control select2">
                        <option value="">Pilih Provinsi</option>
                        <?php
                        foreach($provinsi as $data){
                          echo '<option value="'.$data->id_prov.'">'.$data->nama.'</option>';
                        }
                        ?>
                      <select>
                    <?php //echo form_input($provinsi);?>
                    <!-- <span class="floating-label">Provinsi*</span> -->
                    <?php //echo form_error('provinsi') ?>
                  </div>
                  <div class="form-group" id='kab_box' style="width:220px;">
                      <select name="kota" id="kota" required onchange="ajaxkec(this.value)" class="form-control select2" style="width:100%;">
                        <option value="">Pilih Kota</option>
                      </select>
                  <?php //echo form_input($kota);?>
                    <!-- <span class="floating-label">Kota*</span> -->
                    <?php //echo form_error('kota') ?>
                  </div>
                  <div class="form-group last">
                    <?php echo form_input($kode_pos);?>
                    <span class="floating-label">Kode Pos*</span>
                    <?php //echo form_error('kode_pos') ?>
                  </div>
                </div>
              </div>
              <div class="badan-usaha bottom">
                <p class="title">2. Data Pemohon</p>
                <div class="form-group seven">
                  <?php echo form_input($ktp);?>
                  <span class="floating-label">KTP/SIM/Passport Pimpinan Perusahaan*</span>
                  <?php //echo form_error('ktp') ?>
                </div>
                <div class="form-group seven">
                  <?php echo form_input($first_name);?>
                  <span class="floating-label">Nama Lengkap*</span>
                  <?php //echo form_error('first_name') ?>
                </div>
                <div class="form-group seven">
                    <?php echo form_input($last_name);?>
                  <span class="floating-label">Alamat Pimpinan Perusahan*</span>
                   <?php // echo form_error('last_name') ?>
                </div>
                <?php
                if($identity_column!=='email') {
                    echo '<p>';
                    echo lang('create_user_identity_label', 'identity');
                    echo '<br />';
                    echo form_error('identity');
                    echo form_input($identity);
                    echo '</p>';
                }
                ?>
                <div class="form-inline last-inline">
                    <div class="form-group" style="width:220px;">
                        <select name="provinsi1" id="provinsi1" required onchange="ajaxkota1(this.value)" class="form-control select2">
                          <option value="">Pilih Provinsi</option>
                          <?php
                          foreach($provinsi as $data){
                            echo '<option value="'.$data->id_prov.'">'.$data->nama.'</option>';
                          }
                          ?>
                        <select>
                      <?php //echo form_input($provinsi);?>
                      <!-- <span class="floating-label">Provinsi*</span> -->
                      <?php //echo form_error('provinsi') ?>
                    </div>
                      <!-- <div class="form-group">
                        <?php //echo form_input($provinsi1);?>
                        <span class="floating-label">Provinsi*</span>
                        <?php //echo form_error('provinsi1') ?>
                      </div> -->
                  <div class="form-group" id='kab_box'style="width:220px;">
                      <select name="kota1" id="kota1" required onchange="ajaxkec(this.value)" class="form-control select2" style="width:100%;">
                        <option value="">Pilih Kota</option>
                      </select>
                      <?php //echo form_input($kota1);?>
                    <!-- <span class="floating-label">Kota*</span> -->
                    <?php //echo form_error('kota1') ?>
                  </div>
                  <div class="form-group last">
                    <?php echo form_input($kode_pos1);?>
                    <span class="floating-label">Kode Pos*</span>
                    <?php //echo form_error('kode_pos1') ?>
                  </div>
                </div>
                <div class="form-inline last-inline">
                  <div class="form-group">
                     <?php echo form_input($phone);?>
                    <span class="floating-label">Telepon*</span>
                  </div>
                  <?php echo form_error('phone') ?>
                  <div class="form-group last">
                    <?php echo form_input($email);?>
                    <span class="floating-label">Email* <?php //echo form_error('email') ?></span>
                  </div>

                </div>
                <div class="form-group half">
                     <?php echo form_input($password);?>
                  <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                  <span class="floating-label">Password* <?php //echo form_error('password') ?></span>

                </div>
                <div class="form-group half">
                    <?php echo form_input($password_confirm);?>
                  <span toggle="#password_confirm" class="fa fa-fw fa-eye field-icon toggle-password-2"></span>
                  <span class="floating-label">Ketik Ulang Password* <?php //echo form_error('password_confirm') ?></span>

                </div>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox" required>I Agree with the Term
                </label>
              </div>
              <div class="bottom-submit">
                <div class="row">
                  <div class="col-md-6 col-xs-12 col-sm-6">
                    <?php //form_submit('submit', 'REGISTER',['class' => 'btn btn-default login']);?>
                    <button type="submit" class="btn btn-default login">REGISTER</button>
                    <p>Sudah punya akun? <b> <a href="<?=base_url('login')?>" style="color:white;">Klik untuk login</a> </b> </p>
                  </div>
                  <div class="col-md-6 col-xs-12 col-sm-6">
                    <?php echo $this->recaptcha->render(); ?>
                  </div>
                </div>
              </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="<?=base_url('assets/js/')?>ajax_daerah.js"></script>

    <?php $this->load->view('template/front/footer_home')?>
  </div>
  <!-- End Body Login -->
