<div class="footer-home">
    <div class="container">
    <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-6">
        <div class="process">
            <h3>FLOW PROSES</h3>
            <ul>
            <li>
                <div class="icon-circle"><i class="icon icon-employee"></i></div>
                <span>Pendaftaran Akun</span>
            </li>
            <li>
                <div class="icon-circle"><i class="icon icon-doc"></i></div>
                <span>Permohonan<br>Persetujuan izin</span>
            </li>
            <li>
                <div class="icon-circle"><i class="icon icon-checklist"></i></div>
                <span>Permohonan<br>Realisasi SK</span>
            </li>
            </ul>
        </div>
        </div>
        <div class="col-md-6 col-xs-12 col-sm-6">
        <div class="guide">
            <h3>DOWNLOAD USER GUIDE</h3>
            <ul>
            <li><a><i class="fa fa-caret-right" aria-hidden="true"></i> <span class="text">Pendaftaran Akun</span></a></li>
            <li><a><i class="fa fa-caret-right" aria-hidden="true"></i> <span class="text">Permohonan Persetujuan Izin/izin Prinsip</span></a></li>
            <li><a><i class="fa fa-caret-right" aria-hidden="true"></i> <span class="text">Permohonan Realisasi SK, Izin Penyelenggaraan dan Kartu Pengawasan</span></a></li>
            </ul>
        </div>
        </div>
    </div>
    </div>
</div>
