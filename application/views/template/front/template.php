<?php $dir_template = base_url('template/frontend/web/'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=isset($title)?$title:'BPTJ'?></title>

    <link rel="shortcut icon" href="<?=$dir_template?>assets/img/favicon.png">
    <!-- <link rel="stylesheet" href="<?php echo base_url('template/admin/bootstrap/css/bootstrap.min.css') ?>"> -->
    <link href="<?=$dir_template?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$dir_template?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=$dir_template?>assets/css/style.css" rel="stylesheet">
    <link href="<?=$dir_template?>assets/css/responsive-style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="<?=$dir_template?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=$dir_template?>assets/css/owl.theme.default.min.css">
    <!-- Select -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('template/admin/plugins/jQuery/jQuery-2.1.4.min.js') ?>"></script> 

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->

    <!-- Select -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script src="<?=$dir_template?>assets/js/bootstrap.min.js"></script>
    <script src="<?=$dir_template?>assets/js/owl.carousel.min.js"></script>
    <script src="<?=$dir_template?>assets/js/main-script.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    
    <!-- sweat alert -->
    <script src="<?php echo base_url('template/admin/plugins/sweetalert/sweetalert.min.js') ?>"></script>

  </head>
<body class="bg-white-grey col-dark">
  <header>
    <div class="top-header bg-soft-blue col-white">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-xs-12 col-sm-4">
            <div class="info-top-header">
              <i class="fa fa-phone"aria-hidden="true"></i> Hubungi kami: <a href="tel:+622127791412" class="col-white">(021) 2779 1412</a>
            </div>
          </div>
          <div class="col-md-6 col-xs-12 col-sm-8">
            <div class="action-top">
              <ul class="list-item">
                <li><a href="#" class="col-white"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="#" class="col-white"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a href="#" class="col-white"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a href="#" class="col-white"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                <ul class="list-unstyled">
                  <i class="fa fa-angle-down col-white" aria-hidden="true"></i>
                  <li class="init col-white"><img src="<?=$dir_template?>assets/img/ind.png" class="img-responsive inline-block"> IND</li>
                  <li data-value="Indonesia" class="col-dark ina"><img src="<?=$dir_template?>assets/img/ind.png" class="img-responsive inline-block"> IND</li>
                  <li data-value="English" class="col-dark eng"><img src="<?=$dir_template?>assets/img/eng.png" class="img-responsive inline-block"> ENG</li>
                </ul>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="logo">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="image-logo">
              <a href="#"><img src="<?=$dir_template?>assets/img/logo-long.png" alt="BPTJ" class="img-responsive"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

    <section>
        <?=$contents;?>
    </section>

<?php $this->load->view('sweet')?>

<script type="text/javascript">
$(".toggle-password").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
$(".toggle-password-2").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
$('.owl-carousel.reg-home').owlCarousel({
    loop:true,
    margin:0,
    responsiveClass:true,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:true
        },
        1000:{
            items:1,
            nav:true,
            loop:true
        }
    }
})

$(document).ready(function() {
    $('.select2').select2();
    $('.multiple').select2();
});
</script>

<?php if(isset($ajax)): ?>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo $ajax; ?>",
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
        });
    });
</script>
<?php endif; ?>
</body>
</html>

