<?php $dir_template = base_url('template/frontend/web/'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$title?></title>
	<link rel="stylesheet" href="<?=$dir_template?>assets/css/app.css">
	<link rel="stylesheet" href="<?=$dir_template?>node_modules/owl.carousel/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=$dir_template?>node_modules/owl.carousel/dist/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="<?=$dir_template?>node_modules/font-awesome/css/font-awesome.min.css">
</head>
<body class="body-auth">
    <nav class="navbar navbar-expand-md " style="background:rgba(39, 34, 98); height:30px;" >

		<div class="container">
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#"> <i class="fa fa-phone"></i> (021) 123123 <span class="sr-only">(current)</span></a>
					</li>
                </ul>
                <style>
                    .ml-auto li{
                        margin:0 10px;
                    }
                    .warna-black{
                        color:black !important;
                    }
                </style>
                <ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#"> <i class="fa fa-facebook"></i> </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="#"> <i class="fa fa-twitter"></i> </a>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="#"> <i class="fa fa-envelope"></i> </a>
					</li>
					<li class="dropdown">
                        <a class="nav-link" class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-flag"></i> </a>
                        <ul class="dropdown-menu">
                            <li class="nav-item">
                                <a class="nav-link warna-black" href="#"> <i class="fa fa-twitter"></i> aaa </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link warna-black" href="#"> <i class="fa fa-envelope"></i> aaa </a>
                            </li>
                        </ul>
					</li>

                </ul>
			</div>
		</div>
    </nav>
    
	<nav class="navbar navbar-expand-md navbar-light bg-light">

		<div class="container">

			<a class="navbar-brand" href="#">
				<img src="<?=$dir_template?>assets/img/logo.png" alt="" class="navbar-brand-logo">
				<span class="navbar-brand-title text-primary">Badan Pengelola <br>Transportasi Jabodetabek</span>
			</a>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<!-- <div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#">Disabled</a>
					</li>
				</ul>
				<form class="form-inline mt-2 mt-md-0">
					<input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>
			</div> -->
		</div>
	</nav>

	<section class="hero-section">


		<div class="container">

            <?=$contents;?>

        </div>


		<div class="guide-section">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="step">
							
							<li class="step__item">
								<i class="fa fa-user step__item__icon"></i>
								<span class="step__item__title">Pendaftaran Akun</span>
							</li>
							<li class="step__item">
								<i class="fa fa-file-text step__item__icon"></i>
								<span class="step__item__title">Permohonan Izin</span>
							</li>
							<li class="step__item">
								<i class="fa fa-list step__item__icon"></i>
								<span class="step__item__title">Permohonan Realisasi SK</span>
							</li>
						</ul>
					</div>
					<div class="col-sm-6">
						<h3>DOWNLOAD USER GUIDE</h3>
						<ul>
							<li><a href="#">Pendaftaran Akun</a></li>
							<li><a href="#">Permohonan Persetujuan Izin/Izin Prinsip</a></li>
							<li><a href="#">Permohonan Realisasi SK, Izin Penyelenggaraan dan Kartu Pengawasan</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>




	<script src="<?=$dir_template?>node_modules/jquery/dist/jquery.min.js"></script>
	<script src="<?=$dir_template?>node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?=$dir_template?>node_modules/owl.carousel/dist/owl.carousel.min.js"></script>
	<script src="<?=$dir_template?>assets/js/app.js"></script>
	<!-- sweat alert -->
	<script src="<?php echo base_url('template/admin/plugins/sweetalert/sweetalert.min.js') ?>"></script>


<?php $this->load->view('sweet')?>
<!-- page script -->
<script>
    $(function () {
      $(".datepicker").datepicker({ 
            autoclose: true, 
            todayHighlight: true
      }).datepicker('update', new Date());
    });
    //$('.timepicker').timepicker();
    //$('.timepicker').timepicker({ timeFormat: 'HH:mm:ss' });
    // $(document).ready(function(){
    //     $('input.timepicker').timepicker({ timeFormat: 'h:mm:ss p' });
    // });
    $(function () {
        $('.timepicker').timepicker({
            //use24hours: true;
        });
    });
    $(document).ready(function() {
        $('.select2').select2();
        $('.multiple').select2();
    });

    $(document).ready( function () {
        $('.datatables').DataTable();
    } );

    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });

    /*
    $(document).ready(function() {
        //toggle `popup` / `inline` mode
        $.fn.editable.defaults.mode = 'popup';
        $.fn.editableform.buttons = 
            '<button type="submit" class="btn btn-primary editable-submit btn-sm"><i class="fa fa-check"></i></button>' +
            '<button type="button" class="btn editable-cancel btn-sm"><i class="fa fa-times"></i></button>'; 

        $('#username').editable({
             type:  'text',
             pk:    1,
             name:  'username',
             url:   'post.php',  
             title: 'Enter username',
             validate: function(value) {
               if($.trim(value) == '') return 'This field is required';
            }
          });

    });
    */

    $(document).ready(function() {
      $('.summernote').summernote({
          tabsize: 2,
          height: 200,
        });
    });
    
    function goBack() {
        window.history.back();
    }
    
    function sweat(title,text,icon)
    {
        swal({
            title: title,
            text: text,
            icon: icon,
            timer: 1500,
            button: "OK!",
        });
    }

    function reload_table()
    {
        table.ajax.reload(null,true);
        $('#table_salary').DataTable().ajax.reload();
        $('#overtime_table').DataTable().ajax.reload();
        $('#outstation_table').DataTable().ajax.reload();
        $('.attendance_table').DataTable().ajax.reload();
    }


</script>


<?php if(isset($ajax)): ?>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#table').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo $ajax; ?>",
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
        });
    });
</script>
<?php endif; ?>

</body>
</html>