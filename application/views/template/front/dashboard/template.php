<?php $dir_template = base_url('template/frontend/web/'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard | Beranda - BPTJ</title>

    <link rel="shortcut icon" href="<?=$dir_template?>assets/img/favicon.png">
    <link href="<?=$dir_template?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$dir_template?>assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Dashboard Style -->
    <link rel="stylesheet" href="<?=$dir_template?>assets/css/jasny-bootstrap.min.css">
    <link rel="stylesheet" href="<?=$dir_template?>assets/css/navmenu-reveal.css">
    <!-- End Dashboard Style -->

    <!-- Style Calendar -->
    <link href='<?=$dir_template?>assets/css/fullcalendar.css' rel='stylesheet' />
    <link href='<?=$dir_template?>assets/css/fullcalendar.print.css' rel='stylesheet' media='print' />
    <!-- Ennd Style Calendar -->

    <link href="<?=$dir_template?>assets/css/style.css" rel="stylesheet">
    <link href="<?=$dir_template?>assets/css/responsive-style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="<?=$dir_template?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?=$dir_template?>assets/css/owl.theme.default.min.css">

    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url('template/admin/plugins/datatables/dataTables.bootstrap.css') ?>">

    <!-- Dashboard script-->
    <script src="<?=$dir_template?>assets/js/jquery.min.js"></script>
    <script src="<?=$dir_template?>assets/js/jasny-bootstrap.min.js"></script>
    <!-- End Dashboard script-->

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <script src="<?=$dir_template?>assets/js/bootstrap.min.js"></script>
    <script src="<?=$dir_template?>assets/js/owl.carousel.min.js"></script>
    <script src="<?=$dir_template?>assets/js/main-script.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('template/admin/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo base_url('template/admin/plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>

    <!-- sweat alert -->
    <script src="<?php echo base_url('template/admin/plugins/sweetalert/sweetalert.min.js') ?>"></script>

  </head>
<body class="bg-grey col-dark">
  <div class="navmenu navmenu-default navmenu-fixed-left">
      <div class="slide-menu">
        <span class="navmenu-brand">DASHBOARD MENU</span>
        <div class="navbar navbar-default slide-button">
          <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas=".canvas">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
      </div>
      <ul class="nav navmenu-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?=$dir_template?>assets/img/dash-1.png" class="img-responsive inline-block">Perizinan Online <i class="fa fa-chevron-right col-white" aria-hidden="true"></i></a>
          <ul class="dropdown-menu navmenu-nav">
              <li><a href="<?=base_url('permohonan/angkutan-permukiman')?>"> Angkutan Permukiman</a></li>
              <li><a href="<?=base_url('permohonan/angkutan-perkotaan')?>"> Angkutan Perkotaan</a></li>
            <?php $list_perizinan = list_perizinan(['is_parent'=>5]);if($list_perizinan!=NULL):
                foreach ($list_perizinan as $row):?>
                <li><a href="<?=base_url($row->url)?>"><?=ucfirst(strtolower($row->type_name))?></a></li>
            <?php endforeach; endif; ?>
          </ul>
        </li>
        <li class="dropdown">
          <a href="<?=base_url('permohonan/monitoring');?>" class="dropdown-toggle"><img src="<?=$dir_template?>assets/img/dash-2.png" class="img-responsive inline-block">Monitoring Berkas</a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle"><img src="<?=$dir_template?>assets/img/dash-3.png" class="img-responsive inline-block">Cara Penggunaan</a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle"><img src="<?=$dir_template?>assets/img/dash-4.png" class="img-responsive inline-block">Call Center</a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle"><img src="<?=$dir_template?>assets/img/dash-5.png" class="img-responsive inline-block">Regulasi / Ketentuan</a>
        </li>
      </ul>
    </div>

    <div class="canvas">
      <div class="container-fluid header">
        <div class="dash-header">
          <div class="navbar navbar-default navbar-fixed-top">
            <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas=".canvas">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <img src="<?=$dir_template?>assets/img/logo-dashboard.png" alt="" class="img-responsive inline-block logo-dashboard">
          <div class="notification">
            <ul class="notify">
              <li>
                <a href="#"><i class="fa fa-phone" aria-hidden="true"></i> <span class="text">Hubungi</span></a>
              </li>
              <li>
                <a href="#"><i class="fa fa-info-circle" aria-hidden="true"></i> <span class="text">Tentang</span></a>
              </li>
              <li>
                <a href="#"><i class="fa fa-question-circle" aria-hidden="true"></i> <span class="text">Bantuan</span></a>
              </li>
            </ul>
            <div class="logout">
              <a href="<?=base_url('logout')?>"><i class="fa fa-sign-out" aria-hidden="true"></i> <span class="text">LOGOUT</span></a>
            </div>
          </div>
        </div>
      </div>
      <style>
        .breadcrumb{
          padding: 8px 0px;
          margin-bottom: 0px;
          list-style: none;
          background-color: transparent !important;
          border-radius: 0px;
        }
        .col-white{
          color:white !important;
        }
      </style>
      <!-- Breadcrumb Dashboard -->
      <div class="breadcrumb dashboard">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-9 col-xs-12">
              <div class="info-dashboard">
                <h1 class="name-title col-white bread"><?php echo $title ?></h1>
                <div class="breadcrumb-info">
                  <?php echo create_breadcrumb($home, $home_link); ?>
                </div>
              </div>

            </div>
            <div class="col-md-3 col-xs-12">
              <div class="search-dashboard">
                <div class="input-group">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="#"></i></button>
                  </span>
                  <input type="text" class="form-control" placeholder="Search people, documents, dates...">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Breadcrumb Dashboard -->

      <div class="block-cover" id="loading">
        <div class="cssload-loader tengah">
            <div class="cssload-inner cssload-one"></div>
            <div class="cssload-inner cssload-two"></div>
            <div class="cssload-inner cssload-three"></div>
        </div>
      </div>

<!--
      <div id="loader-wrapper">
        <h2>Loading...</h2>
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
      </div>
       -->

    <?php echo $contents;?>

      <!-- Footer Dashboard -->
      <div class="copywriting">
        <div class="container-fluid">
          <p>Hak Cipta &copy; 2018 . <strong>Badan Pengelola Transportasi JABODETABEK</strong>  | All Rights Reserved.</p>
        </div>
      </div>
      <!-- End Footer Dashboard -->

    </div>
    <?php $this->load->view('sweet')?>
    <script type="text/javascript">
    $(document).ready( function () {
        $('.datatables').DataTable();
    } );
    </script>

    <?php if(isset($ajax)): ?>
    <script type="text/javascript">

        var table;
        $(document).ready(function() {
            //datatables
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?php echo $ajax; ?>",
                    "type": "POST"
                },
                "columnDefs": [
                {
                    "targets": [ 0 ],
                    "orderable": false,
                },
                ],
            });
        });
    </script>
    <?php endif; ?>
  </body>
</html>
