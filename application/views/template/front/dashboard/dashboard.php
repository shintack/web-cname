<div class="container-fluid top-info">
    <div class="top-info-dashboard">
        <div class="row">
        <div class="col-md-6 col-xs-12 col-sm-6">
            <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
            <div class="grey-box">
            <h4>Selamat Datang <br>di Halaman Dashboard Anda, User</h4>
            <p>Gunakan halaman ini untuk mengajukan Surat Pengajuan Berkas-berkas kendaraan Anda.</p>
            <p>Kami menyarankan Anda untuk mempersiapkan berkas-berkas yang akan diupload terlebih dahulu sebelum menggunakan aplikasi ini.</p>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 col-sm-6">
            <div class="white-box">
            <h4>Tahapan pengajuan berkas</h4>
            <ul class="list-step">
                <li><i class="icon icon-list one"></i>
                <div class="desc">
                    <p class="title-step">Pendaftaran Akun</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                </div>
                </li>
                <li><i class="icon icon-list two"></i>
                <div class="desc">
                    <p class="title-step">Permohonan Persetujuan Izin</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                </div>
                </li>
                <li><i class="icon icon-list three"></i>
                <div class="desc">
                    <p class="title-step">Permohonan Realisasi SK</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                </div>
                </li>
            </ul>
            </div>
        </div>
        </div>
    </div>
</div>

<div class="container-fluid bottom-info">
    <div class="guide">
        <h3>DOWNLOAD USER GUIDE</h3>
        <ul>
        <li><a><i class="fa fa-caret-right" aria-hidden="true"></i> <span class="text">Pendaftaran Akun</span></a></li>
        <li><a><i class="fa fa-caret-right" aria-hidden="true"></i> <span class="text">Permohonan Persetujuan Izin/izin Prinsip</span></a></li>
        <li><a><i class="fa fa-caret-right" aria-hidden="true"></i> <span class="text">Permohonan Realisasi SK, Izin Penyelenggaraan dan Kartu Pengawasan</span></a></li>
        </ul>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
