<header class="main-header">
    <style>
    @media only screen and (max-width: 600px) {
        #logo {
            display: none;
        }
    }
    </style>
    <div id="logo">
        <!-- Logo -->
        <a href="<?=base_url()?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <!-- <span class="logo-mini"></span> -->
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Deplaza Panel</b></span>
        </a>
    </div>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php /*
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-home"></i> Home
                    </a>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-home"></i> Hubungi
                    </a>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-home"></i> Tentang
                    </a>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-home"></i> Bantuan
                    </a>
                </li>
                <!-- <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-sign-out"></i> Logout</a>
                </li> -->
                */ ?>
                <!-- User Account: style can be found in dropdown.less -->
                <?php if($this->session->userdata('user_id')!==NULL):?>
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-bell-o"></i>
                    <?php
                    $jml_notif=0;
                    if(count($this->_notification)>0){
                        $jml_notif = count($this->_notification);
                        echo '<span class="label label-warning">'.$jml_notif.'</span>';
                    } ?>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?=$jml_notif?> notifications</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <?php if($jml_notif>0): ?>
                                <ul class="menu">
                                    <?php foreach($this->_notification as $notif): ?>
                                        <li>
                                            <a href="<?=site_url('message/read/'.encrypt($notif->notification_id))?>" title="<?=$notif->created?>" >
                                                <?=substr($notif->pengirim,0,12).', '. substr($notif->message,0,140) ?>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </li>

                        <li class="footer"><a href="#">Lihat Semua</a></li>
                        <!-- <li class="footer"><a href="#">Tandai sudah dibaca</a></li> -->
                    </ul>
                </li>
                <li class="dropdown user user-menu">
                    <?php $image = isset($this->session->userdata('staffdata')->image_profil)?$this->session->userdata('staffdata')->image_profil:NULL; ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?=($image!=NULL)?base_url('uploads/profile/'.$image):base_url('template/admin/dist/img/user2-160x160.jpg')?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?=isset($this->session->userdata('staffdata')->nama_lengkap)?$this->session->userdata('staffdata')->nama_lengkap:$this->session->userdata('email'); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?=($image!=NULL)?base_url('uploads/profile/'.$image):base_url('template/admin/dist/img/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
                            <p>
                                <?=isset($this->session->userdata('staffdata')->nama_lengkap)?$this->session->userdata('staffdata')->nama_lengkap:$this->session->userdata('email'); ?>
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                        <!-- Menu Body --
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?=site_url('auth/profile/'.encrypt($this->session->userdata('user_id')))?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?php
                                echo anchor('auth/logout','Sing out',array('class'=>'btn btn-default btn-flat'));
                                ?>

                            </div>
                        </li>
                    </ul>
                </li>
                <?php endif; ?>

            </ul>
        </div>


    </nav>
</header>
