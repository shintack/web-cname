<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?=(isset($title))?$title:'ShintackEror';?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php $this->load->view('template/admin/css'); ?>
        <style type="text/css">
            thead tr{
                background-color: #367FA9;
                color: white; 
                height: 35px;
            }
            thead tr th{
                text-align: center;
                padding-left: 5px;
            }
            tbody tr td{
                padding-left: 5px;   
            }
            tfoot{
                display: none;
            }
            .breadcrumb{
                padding: 8px 0px;
                margin-bottom: 20px;
                list-style: none;
                background-color: rgba(201, 76, 76, 0);
                color:white;
                border-radius: 4px;
            }
            /* .breadcrumb li a{
                color:white;
            } */
        </style>
    </head> 
    <body class="hold-transition sidebar-mini skin-black sidebar-collapsea">
            <div class="block-cover" id="loading">  
                <div class="cssload-loader tengah">
                    <div class="cssload-inner cssload-one"></div>
                    <div class="cssload-inner cssload-two"></div>
                    <div class="cssload-inner cssload-three"></div>
                </div>
            </div>
        <div class="wrapper">

            <?php $this->load->view('template/admin/top_menu'); ?>
           

            <!-- Left side column. contains the logo and sidebar -->
            <?php $this->load->view('template/admin/side_menu'); ?>
           

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
            
            <!-- <section class="content-headera"> -->
            <section class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2><?php echo (isset($title))?$title:'Home' ?></h2>
                        <?php //echo (isset($home))?create_breadcrumb($home, $home_link):create_breadcrumb('Home', current_url()); ?>        
                        <ol class="breadcrumb">
                            <?php echo (isset($home))?create_breadcrumb($home, $home_link):create_breadcrumb('Home', current_url()); ?>        
                        </ol>
                    </div>
                </div>
            </section>

            <section class="content" >
            <?php
            echo $contents;
            ?>
            </section>


            </div><!-- /.content-wrapper -->
            
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    Hak Cipta <?=date('Y')?> <b>Badan Pengelola Transportasi JABODETABEK</b> | All Rights Reserved.
                </div>
                    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>

            </footer>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div><!-- ./wrapper -->

        <?php $this->load->view('template/admin/js'); ?>
    </body>
</html>
