
<!-- DatePicker -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-timepicker/0.5.2/js/bootstrap-timepicker.min.js"></script> -->
<!-- x-editable --
<script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.1/bootstrap-editable/js/bootstrap-editable.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('template/admin/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('template/admin/plugins/fastclick/fastclick.min.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('template/admin/dist/js/app.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('template/admin/dist/js/demo.js') ?>"></script>

<?php $this->load->view('sweet')?>
<!-- page script -->
<script>

    $(document).ready(function() {
        $('.select2').select2();
        $('.multiple').select2();
    });

    $(document).ready( function () {
        $('.datatables').DataTable();
    } );

    // $(function () {
    //     $("#example1").DataTable();
    //     $('#example2').DataTable({
    //         "paging": true,
    //         "lengthChange": false,
    //         "searching": false,
    //         "ordering": true,
    //         "info": true,
    //         "autoWidth": false
    //     });
    // });

    /*
    $(document).ready(function() {
        //toggle `popup` / `inline` mode
        $.fn.editable.defaults.mode = 'popup';
        $.fn.editableform.buttons = 
            '<button type="submit" class="btn btn-primary editable-submit btn-sm"><i class="fa fa-check"></i></button>' +
            '<button type="button" class="btn editable-cancel btn-sm"><i class="fa fa-times"></i></button>'; 

        $('#username').editable({
             type:  'text',
             pk:    1,
             name:  'username',
             url:   'post.php',  
             title: 'Enter username',
             validate: function(value) {
               if($.trim(value) == '') return 'This field is required';
            }
          });

    });
    */

    $(document).ready(function() {
        /*
        $('.summernote').summernote({
          tabsize: 2,
          height: 200,
        });
        */
    });
    
    function goBack() {
        window.history.back();
    }
    
    function sweat(title,text,icon)
    {
        swal({
            title: title,
            text: text,
            icon: icon,
            timer: 1500,
            button: "OK!",
        });
    }

    function reload_table()
    {
        table.ajax.reload(null,true);
        $('#table_salary').DataTable().ajax.reload();
        $('#overtime_table').DataTable().ajax.reload();
        $('#outstation_table').DataTable().ajax.reload();
        $('.attendance_table').DataTable().ajax.reload();
        
    }


</script>


<?php if(isset($ajax)): ?>
<script type="text/javascript">

var table;
var area = $("#area").val();
$(document).ready(function() {
    //datatables
    table = $('#table').DataTable({ 
        "processing": true, 
        "serverSide": true, 
        "order": [], 
        "ajax": {
            "url": "<?php echo $ajax; ?>",
            "type": "POST",
            "data": function ( data ) {
                data.area       = $('#area').val();
                data.type_id    = $('#type_id').val();
                data.group_id   = $('#group_id').val();
                data.status   = $('#status').val();
                data.company_id   = $('#company_id').val();
            },
        },
        "columnDefs": [
        { 
            "targets": [ 0 ], 
            "orderable": false, 
        },
        ],
    });

    $('#btn-filter').click(function(){ //button filter event click
        table.ajax.reload();  //just reload table
    });

    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        // $('.select2').select2('refresh');
        table.ajax.reload();  //just reload table
    });
});
</script>
<?php endif; ?>