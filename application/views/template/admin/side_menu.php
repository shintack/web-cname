<?php
    $user = $this->session->userdata('email');
    $ip = $_SERVER['REMOTE_ADDR'];
    if ($ip == '119.110.66.180') {
        $cabang = 'Bekasi';
    }elseif ($ip == '43.247.14.24') {
        $cabang = 'Cempaka Mas';
    }elseif ($ip == '66.96.238.89') {
        $cabang = 'Permata Hijau';
    }elseif ($ip == '103.234.253.44') {
        $cabang = 'Serpong';
    }elseif ($ip == '120.188.32.134') {
        $cabang = 'STC';
    }else{
        $cabang = 'NULL';
    }
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <?php if($this->session->userdata('user_id')!==NULL):?>
        <!-- <div class="user-panel">
            <div class="pull-left image">
            <?php $image = isset($this->session->userdata('staffdata')->image_profil)?$this->session->userdata('staffdata')->image_profil:NULL; ?>
                <img src="<?=($image!=NULL)?base_url('uploads/profile/'.$image):base_url('template/admin/dist/img/user2-160x160.jpg')?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?=isset($this->session->userdata('staffdata')->nama_lengkap)?substr($this->session->userdata('staffdata')->nama_lengkap,0,15):$this->session->userdata('email'); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> <?php echo 'IP '. $ip . ' | Login '.$this->session->userdata('cabang_name');  ?> </a>
                <p> <a href="" id="time"></a> </p>
            </div>
        </div> -->
        <?php endif; ?>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

            <li>
                <a href="<?=base_url()?>">
                    <i class="fa fa-laptop"></i> <span>DASHBOARD</span>
                    <span class="pull-right-container">
                      <span class="label label-primary pull-right">4</span>
                    </span>
                </a>
            </li>

            <?php
            //if ($this->session->userdata('user_menu')!==NULL) {
                //echo $this->session->userdata('user_menu');
                /*
                $menu = $this->db->order_by('is_arrage','ASC')->get_where('menu', array('is_parent' => 0,'is_active'=>1));
                foreach ($menu->result() as $m) {
                    // chek ada sub menu
                    $submenu = $this->db->order_by('is_arrage','ASC')->get_where('menu',array('is_parent'=>$m->id,'is_active'=>1));
                    if($submenu->num_rows()>0){
                        // tampilkan submenu
                        echo "<li class='treeview'>
                            ".anchor('#',  "<i class='$m->icon'></i><span>".strtoupper($m->name).'</span> <i class="fa fa-angle-left pull-right"></i>')."
                                <ul class='treeview-menu'>";
                        foreach ($submenu->result() as $s){
                             echo "<li>" . anchor($s->link, "<i class='$s->icon'></i> <span>" . strtoupper($s->name)) . "</span> </li>";
                        }
                           echo"</ul>
                            </li>";
                    }else{
                        echo "<li>" . anchor($m->link, "<i class='$m->icon'></i> <span>" . strtoupper($m->name)) . "</span></li>";
                    }

                }
                */
                $user_menu = '';
                $user_id = $this->session->userdata('user_id');
                $menu = $this->db->distinct()
                ->select('menu.id,name,link,icon,is_parent')
                ->join('user_groups_menu','user_groups_menu.group_id = users_groups.group_id')
                ->join('menu','menu.id = user_groups_menu.menu_id')
                ->where('menu.is_active',1)
                ->where('menu.is_parent',0)
                ->where('user_id',$user_id)
                ->order_by('is_arrage','ASC')
                ->get('users_groups')->result();
                // vardump($menu);

                foreach ($menu as $m) {
                    // chek ada sub menu
                    $submenu = $this->db->distinct()
                    ->select('menu.id,name,link,icon,is_parent')
                    ->join('user_groups_menu','user_groups_menu.group_id = users_groups.group_id')
                    ->join('menu','menu.id = user_groups_menu.menu_id')
                    ->where('menu.is_active',1)
                    ->where('menu.is_parent',$m->id)
                    ->where('user_id',$user_id)
                    ->order_by('is_arrage','ASC')->get('users_groups');
                    //vardump($submenu);
                    if($submenu->num_rows()>0){
                        // tampilkan submenu
                        $user_menu .= "<li class='treeview'>
                            ".anchor('#',  "<i class='$m->icon'></i><span>".strtoupper($m->name).'</span> <i class="fa fa-angle-left pull-right"></i>')."
                                <ul class='treeview-menu'>";
                        foreach ($submenu->result() as $s){
                             $user_menu .= "<li>" . anchor($s->link, "<i class='$s->icon'></i> <span>" . strtoupper($s->name)) . "</span></li>";
                        }
                           $user_menu .= "</ul>
                            </li>";
                    }else{
                        $user_menu .= "<li>" . anchor($m->link, "<i class='$m->icon'></i> <span>" . strtoupper($m->name)) . "</span></li>";
                    }
                }
                echo $user_menu;

            //}
            ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i> <span>Multilevel</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level One
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li class="treeview">
                      <a href="#"><i class="fa fa-circle-o"></i> Level Two
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
