<div class="row">
    <div class="col-sm-12">
        <div class="card box-box-info">
            <div class="card-body box-body">
              <table id="table" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                  <thead>
                      <tr>
                          <th align="center" width="10px">No</th>
                          <th>Nama</th>
                          <th>Invoice</th>
                          <th>Status Pembayaran</th>
                          <th>Detail Order</th>
                          <!-- <th>Action</th> -->
                      </tr>
                  </thead>

                  <tbody>

                  </tbody>

              </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

var datatables;
var table           = $("#table");
var form            = $("<?=isset($form_input)?$form_input:'form'?>");
var urlDataTable    = "<?=isset($urlDataTable)?$urlDataTable:''; ?>";
var csrfName        = "<?=$this->security->get_csrf_token_name(); ?>";
var csrfHash        = "<?=$this->security->get_csrf_hash(); ?>";

//custom search

$(document).ready(function() {

    <?php if(isset($urlDataTable)): ?>
    datatables = table.DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "responsive": true,
        // "rowReorder": {
        //     "selector": "td:nth-child(2)"
        // },
        "ajax": {
            "url": urlDataTable,
            "type": "POST",
            "data": function ( data ) {
                data.start_date         = $("[name='start_date']").val();
                data.end_date           = $("[name='end_date']").val();
                data.shop_merchant_id   = $("[name='shop_merchant_id']").val();
                data.shop_merchant_id_supplier   = $("[name='shop_merchant_id_supplier']").val();
                data.penjualan          = $("[name='penjualan']").val();
                data.payment_status     = $("[name='payment_status']").val();
                data.order_status       = $("[name='order_status']").val();

            },
        },
        "columnDefs": [
        {
            "targets": [ 0 ],
            "orderable": false,
        },
        ],
    });
    <?php endif; ?>

    $('#btn-filter').click(function(){ //button filter event click
        datatables.ajax.reload();  //just reload table
        <?=($this->uri->segment(1)=='transaction')?'hitung_transaksi()':'' ?>
    });

    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        datatables.ajax.reload();  //just reload
        <?=($this->uri->segment(1)=='transaction')?'hitung_transaksi()':'' ?>
    });

});
</script>
