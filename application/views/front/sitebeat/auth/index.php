<!-- REGISTER -->
<div class="row bg-white" id="register">
  <div class="section">
    <div class="col-md-8 col-md-offset-4 col-xs-12 text-right">
      <ul class="wizard font-medium font-sm" data-aos="fade">
        <li><a href="#">01 - INFO PELANGGAN</a></li>
        <li class="disabled"><a href="#">02 - INFORMASI PEMBAYARAN</a></li>
        <li class="disabled"><a href="#">03 - PAYMENT SELECTION</a></li>
      </ul>

      <h1 class="font-bold font-lg font-black text-left" data-aos="fade-down">Pelanggan Baru - <span class="font-blue font-medium">Lengkapi Data Anda</span></h1>
      <p class="font-grey font-light text-left font-md" data-aos="fade-down" data-aos-delay="250">Sudah memiliki akun? <a href="#" class="font-blue">Masuk</a></p>
    </div>
    <div class="col-md-4 col-xs reg-img">
      <img src="<?=$this->path_assets?>assets/images/register.png" width="95%" data-aos="fade-right">
    </div>
    <form data-aos="fade" data-aos-delay="250">
      <div class="col-md-4">
        <div class="group">
          <input type="text" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Nama Depan</label>
        </div>

        <div class="group">
          <input type="text" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Nama Belakang</label>
        </div>

        <div class="group">
          <input type="text" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Alamat</label>
        </div>

        <div class="row">
          <div class="col-md-9 col-xs-12">
            <div class="group">
              <input type="text" required>
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>Kota</label>
            </div>
          </div>
          <div class="col-md-3 col-xs-12">
            <div class="group">
              <input type="text" required>
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>Kode Pos</label>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="group">
          <input type="text" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Negara</label>
        </div>

        <div class="group">
          <input type="text" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Provinsi</label>
        </div>

        <div class="group">
          <input type="text" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Email</label>
        </div>

        <div class="group">
          <input type="text" required>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Telepon</label>
        </div>
      </div>
      <div class="col-md-4">
        <p class="font-sm">Syarat dan Ketentuan Pendaftaran <a href="#" class="font-blue"><i class="fa fa-exclamation-circle" data-toggle="tooltip" title="Syarat & Ketentuan" data-placement="right"></i></a></p>
        <br>
        <div class="checkbox checkbox-primary font-light">
          <input id="skcheck" type="checkbox">
          <label for="skcheck">
            Saya setuju dengan kebijakan <a href="#" class="font-blue">Privasi</a>.
          </label>
        </div>
      </div>
      <div class="col-md-4 text-right">
        <input type="submit" value="LANJUTKAN PESANAN" class="btn btn-rounded bg-blue font-white">
      </div>
    </form>
  </div>
</div>
<!-- REGISTER END -->
