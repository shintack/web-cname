<!-- REGISTER -->
<div class="row bg-white" id="register">
  <div class="section">
    <div class="col-md-8 col-md-offset-4 col-xs-12 text-right">
      <ul class="wizard font-medium font-sm" data-aos="fade">
        <li><a href="#">01 - INFO PELANGGAN</a></li>
        <li class="disabled"><a href="#">02 - INFORMASI PEMBAYARAN</a></li>
        <li class="disabled"><a href="#">03 - PAYMENT SELECTION</a></li>
      </ul>

      <h1 class="font-bold font-lg font-black text-left" data-aos="fade-down">Pelanggan Baru - <span class="font-blue font-medium">Lengkapi Data Anda</span></h1>
      <p class="font-grey font-light text-left font-md" data-aos="fade-down" data-aos-delay="250">Sudah memiliki akun? <a href="<?=site_url('login')?>" class="font-blue">Masuk</a></p>
    </div>
    <div class="col-md-4 col-xs reg-img">
      <img src="<?=$this->path_assets?>assets/images/register.png" width="95%" data-aos="fade-right">
    </div>

    <form action="<?=base_url('register')?>" method="post" data-aos="fade" data-aos-delay="250">
      <?php if (isset($message)) {
        echo '
        <div class="col-md-8 alert alert-success">
          '.$message.'
        </div>';
      } ?>

      <div class="col-md-4">
        <div class="group">
          <?php echo form_input($first_name);?>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Nama Depan</label>
        </div>
      </div>

      <div class="col-md-4">
        <div class="group">
          <?php echo form_input($last_name);?>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Nama Belakang</label>
        </div>
      </div>

      <div class="col-md-4">
        <div class="group">
          <?php echo form_input($email);?>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Email</label>
        </div>
      </div>

      <div class="col-md-4">
        <div class="group">
          <?php echo form_input($phone);?>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Telepon</label>
        </div>
      </div>

      <div class="col-md-4">
        <div class="group">
          <?php echo form_input($password);?>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Password</label>
        </div>
      </div>

      <div class="col-md-4">
        <div class="group">
          <?php echo form_input($password_confirm);?>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Konfirmasi Password</label>
        </div>
      </div>

      <div class="col-md-4">
        <p class="font-sm">Syarat dan Ketentuan Pendaftaran <a href="#" class="font-blue"><i class="fa fa-exclamation-circle" data-toggle="tooltip" title="Syarat & Ketentuan" data-placement="right"></i></a></p>
        <br>
        <div class="checkbox checkbox-primary font-light">
          <label for="skcheck">
            <input id="skcheck" type="checkbox" required style="margin-top:5px !important; width:15px;">
            Saya setuju dengan kebijakan <a href="#" class="font-blue">Privasi</a>.
          </label>
        </div>
      </div>

      <div class="col-md-4">
        <?php echo $this->recaptcha->render(); ?>
        <br>
        <input type="submit" value="LANJUTKAN PESANAN" class="btn btn-rounded bg-blue font-white">
      </div>

    </form>
  </div>
</div>
<!-- REGISTER END -->
