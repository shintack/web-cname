<!-- REGISTER -->
<div class="row bg-white" id="register" style="padding-top:60px;">
  <div class="section">
    <div class="col-md-8 col-md-offset-4 col-xs-12 text-right">
      <!-- <ul class="wizard font-medium font-sm" data-aos="fade">
        <li><a href="#">01 - INFO PELANGGAN</a></li>
        <li class="disabled"><a href="#">02 - INFORMASI PEMBAYARAN</a></li>
        <li class="disabled"><a href="#">03 - PAYMENT SELECTION</a></li>
      </ul> -->
      <!-- <h1 class="font-bold font-lg font-black text-left" data-aos="fade-down">Pelanggan Baru - <span class="font-blue font-medium">Register</span></h1> -->
      <h1 class="font-bold font-lg font-black text-left" data-aos="fade-down">LOGIN</h1>

      <p class="font-grey font-light text-left font-md" data-aos="fade-down" data-aos-delay="250">Belum memiliki akun? <a href="<?=site_url('register')?>" class="font-blue">Daftar</a></p>
    </div>

    <div class="col-md-4 col-xs reg-img">
      <img src="<?=$this->path_assets?>assets/images/register.png" width="95%" data-aos="fade-right">
    </div>
    <form action="<?=base_url('auth/login')?>" method="post" data-aos="fade" data-aos-delay="250">
      <?php if (isset($message)) {
        echo '
        <div class="col-md-8 alert alert-success">
          '.$message.'
        </div>';
      } ?>
      <div class="col-md-4">
        <div class="group has-feedback">
          <?php echo form_input($identity); ?>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Email</label>
          <span class="fa fa-envelope form-control-feedback"></span>
        </div>
        <div class="group has-feedback">
          <?php echo form_input($password); ?>
          <span class="highlight"></span>
          <span class="bar"></span>
          <label>Password</label>
          <span class="fa fa-lock form-control-feedback"></span>
        </div>
        <div class="group has-feedback">
          <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
          <!-- <span class="highlight"></span>
          <span class="bar"></span> -->
          <label style="top: 0px; margin-left: 17px">Ingat Saya</label>
          <!-- <span class="fa fa-lock form-control-feedback"></span> -->
        </div>

        <?php //echo $this->recaptcha->render(); ?>
        <hr>
        <div class="row">
            <div class="col-xs-6">
                <button type="submit" name="submit" class="btn btn-primary btn btn-rounded bg-blue font-white"><i class="fa fa-sign-in"></i> LOGIN</button>
            </div><!-- /.col -->
        </div>
        <div class="row">
            <div class="col-xs-12">
                <br>
                <a href="<?=base_url('auth/forgot_password')?>">
                    Lupa Password
                </a>
            </div><!-- /.col -->
        </div>
      </div>
    </form>
  </div>
</div>
<!-- REGISTER END -->
