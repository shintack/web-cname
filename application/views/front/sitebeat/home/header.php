
<!-- HEADER -->
<div class="row b-lazy" id="header" data-src="<?=$this->path_assets?>assets/images/background/head.webp">
	<div class="section">
		<div class="col-md-5 col-xs-12">
			<h1 class="font-bold font-white font-xl" data-aos="fade-down">Miliki toko online dengan ribuan produk dropship hanya dengan 299rb</h1>
			<p class="font-md font-white" data-aos="fade-up">Cukup dengan satu klik ribuan produk terpasang otomatis di toko online anda.</p>
			<a href="#getstarted" class="bg-amber btn btn-rounded font-medium font-md font-black" id="btn-daftar" data-aos="fade-up" data-aos-delay="500">DAFTAR SEKARANG</a>
		</div>
		<div class="col-md-7 col-xs-12 text-center img-header">
			<!-- <img class="b-lazy" data-src="image.jpg" /> -->
			<!-- <img class="b-lazy" data-src="<?=$this->path_assets?>assets/images/header.png" width="80%" data-aos="fade-right"> -->
			<!-- <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/header.png" width="80%" data-aos="fade-right"> -->
			<img src="<?=$this->path_assets?>assets/images/header.png" width="80%" data-aos="fade-right">
		</div>

		<a href="#reason" class="bg-white font-black" id="todown"><i class="fa fa-arrow-down fa-2x"></i></a>
	</div>
</div>
<!-- HEADER END -->
