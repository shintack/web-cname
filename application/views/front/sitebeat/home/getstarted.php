<style media="screen">
  @media (min-width: 720px) {
    .p-lr-20{
      padding:0px 100px 0px 100px;
    }
  }
</style>


<!-- GET STARTED -->
<div class="row b-lazy" id="getstarted" data-src="<?=$this->path_assets?>assets/images/background/getstarted.webp">
  <div class="section">
    <div class="col-md-12 text-center" >
      <h2 class="font-xl font-bold font-white">Miliki toko online anda sekarang</h2>
      <p class="font-md font-light font-white">Mulai berjualan dan nikmati keuntungan dari ribuan produk yang siap anda jual</p>
    </div>

      <?php if( isset($list_paket) && is_array($list_paket) ): $i=1; foreach($list_paket as $paket): $color = ($i%2)==0?'orange':'blue';?>
        <div class="col-md-6 col-xs-12 p-lr-20">
          <div class="price bg-white text-center">
            <div class="price-head">
              <h3 class="font-blue font-bold font-lg"><?=$paket->name?></h3>
            </div>
            <div class="bg-<?=$color?>-gradient price-pricing">
              <h3 class="font-white font-xl font-bold"><?=$paket->price?></h3>
              <p class="font-black font-sm font-light">per bulan</p>
            </div>
            <div class="price-body">
              <p class="font-light font-sm">
                <?=$paket->specification?>
              </p>
              <hr>
              <!-- <a href="#" class="font-blue-lighten font-medium font-sm">Lihat Fitur Selengkapnya</a> -->
              <a href="#" onclick="pilihPaket(<?=$paket->paket_id?>)" class="btn btn-rounded bg-<?=$color?>-gradient btn-block" data-toggle="modal" data-target="#domain">DAFTAR SEKARANG</a>
            </div>
          </div>
        </div>
      <?php $i++; endforeach; endif; ?>

    <?php /*
      <div class="col-md-5 col-md-offset-1 col-xs-12">
        <div class="price bg-white text-center" data-aos="zoom-in-right" data-aos-delay="250">
          <div class="price-head">
            <h3 class="font-blue font-bold font-lg">BASIC</h3>
          </div>
          <div class="bg-orange-gradient price-pricing">
            <h3 class="font-white font-xl font-bold">Rp. 299.000</h3>
            <p class="font-black font-sm font-light">per bulan</p>
          </div>
          <div class="price-body">
            <p class="font-light font-sm"><s>Rp. 300.000</s> <br> 35% OFF</p>
            <hr>
            <!-- <a href="#" class="font-blue-lighten font-medium font-sm">Lihat Fitur Selengkapnya</a> -->

            <a href="#" class="btn btn-rounded bg-orange-gradient btn-block" data-toggle="modal" data-target="#domain">DAFTAR SEKARANG</a>
          </div>
        </div>
      </div>

      <div class="col-md-5 col-xs-12">
        <div class="price bg-white text-center" data-aos="zoom-in-right" data-aos-delay="500">
          <div class="price-head">
            <h3 class="font-blue font-bold font-lg">PREMIUM</h3>
          </div>
          <div class="bg-blue-gradient price-pricing">
            <h3 class="font-white font-xl font-bold">Rp. 399.000</h3>
            <p class="font-black font-sm font-light">per bulan</p>
          </div>
          <div class="price-body">
            <p class="font-light font-sm"><s>Rp. 300.000</s> <br> 35% OFF</p>
            <hr>
            <!-- <a href="#" class="font-blue-lighten font-medium font-sm">Lihat Fitur Selengkapnya</a> -->

            <a href="#" class="btn btn-rounded bg-blue-gradient btn-block" data-toggle="modal" data-target="#domain">DAFTAR SEKARANG</a>
          </div>
        </div>
      </div>
    */ ?>

    <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif"  data-src="<?=$this->path_assets?>assets/images/getstarted.png" width="60%" data-aos="fade-right">
  </div>
</div>

  <!-- DOMAIN MODAL -->
  <!-- Modal -->
  <div class="modal fade" id="domain" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body text-center">
          <img src="<?=$this->path_assets?>assets/images/domain.jpg" width="50%">
          <h2 class="font-lg font-bold">Hubungkan nama domain ke situs webmu</h2>
          <a href="#" class="btn btn-success bg-green" onclick="linkDomain('new')" data-toggle="modal" data-target="#punya-domain" data-dismiss="modal">SAYA SUDAH PUNYA DOMAIN</a>
          <a href="#" class="btn btn-success bg-green" onclick="linkDomain('link')" data-toggle="modal" data-target="#daftar-domain" data-dismiss="modal">DAPATKAN DOMAIN GRATIS</a>
        </div>
      </div>
    </div>
  </div>
  <!-- DOMAIN MODAL END -->

  <!-- DAFTAR DOMAIN MODAL -->
  <div class="modal fade" id="daftar-domain" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body row">
          <div class="col-md-6 col-xs-9">
            <p class="font-light font-sm font-blue-lighten" data-toggle="modal" data-target="#punya-domain" data-dismiss="modal"><i class="fa fa-chevron-left font-black"></i> <a href="#" onclick="linkDomain('link')">Gunakan Domain yang ada</a></p>
          </div>
          <div class="col-md-6 col-xs-3 text-right">
            <a href="#" data-dismiss="modal"><i class="fa fa-remove fa-2x font-grey"></i></a>
          </div>
          <div class="col-md-12">
            <div class="group">
              <input type="text" name="daftar_domain_input" required>
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>Dapatkan Domain</label>
              <i href="#" class="fa fa-search cek-domain"></i>
            </div>
            <div class="col-sm-12 text-center loading">
              <img src="https://upaep.mx/templates/idiomas/img/spinner.gif" alt="">
            </div>
            <div id="domain-avaliable" style="max-height:300px;overflow:auto;">

            </div>
            <!--
            <div class="table-responsive">
          <table class="table">
            <tr>
              <td width="80%">Khoirudin.id</td>
              <td>Gratis</td>
              <td class="font-blue"><a href="#">PILIH</a></td>
            </tr>
            <tr>
              <td>Khoirudin.id</td>
              <td>Gratis</td>
              <td class="font-blue"><a href="#">PILIH</a></td>
            </tr>
            <tr>
              <td>Khoirudin.id</td>
              <td>Gratis</td>
              <td class="font-blue"><a href="#">PILIH</a></td>
            </tr>
            <tr>
              <td>Khoirudin.id</td>
              <td>Gratis</td>
              <td class="font-blue"><a href="#">PILIH</a></td>
            </tr>

          </table>
        </div>
            -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- DAFTAR DOMAIN MODAL END -->

  <!-- PUNYA DOMAIN MODAL -->
  <div class="modal fade" id="punya-domain" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body row">
          <div class="col-sm-6 col-xs-9">
            <p class="font-light font-sm font-blue-lighten" data-toggle="modal" data-target="#daftar-domain" data-dismiss="modal"><i class="fa fa-chevron-left font-black"></i> <a href="#" onclick="linkDomain('new')" >Klaim domain gratis</a></p>
          </div>
          <div class="col-sm-6 col-xs-3 text-right">
            <a href="#" data-dismiss="modal"><i class="fa fa-remove fa-2x font-grey"></i></a>
          </div>
          <div class="col-sm-12">
            <div class="group">
              <input type="text"  name="daftar-domain-input" required>
              <span class="highlight"></span>
              <span class="bar"></span>
              <label>Dapatkan Domain</label>
              <i class="fa fa-check font-green"></i>
            </div>
        <div class="text-center font-white">
          <button class="link-domain btn btn-rounded bg-blue font-white">HUBUNGKAN DOMAIN</button>
        </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- PUNYA DOMAIN MODAL END -->

<!-- GET STARTED END -->

<input type="hidden" name="paket_id" value="" placeholder="paket_id">
<input type="hidden" name="domain_type" value="" placeholder="domain_type">
<input type="hidden" name="domain_name" value="" placeholder="nama_domain">

<script type="text/javascript">

  $(function(){

    $(".loading").hide();

    $(".cek-domain").click(function(){
      cekDomain();
    });

    $("[name='daftar_domain_input']").bind("enterKey",function(e){
      cekDomain();
    });
    $("[name='daftar_domain_input']").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
    });

  });

  function cekDomain() {
    $(".loading").show();
    $.post(base_url+'cekdomain/domain',{
      domain:$("[name='daftar_domain_input']").val(),
    },function(data,status){
      if ( status=='success' ) {
        $(".loading").hide('slow');
        $('#domain-avaliable').html(data);
      }else{
        console.log(data);
      }
    });
  }

  function pilihDomain(domain) {
    $('[name="domain_name"]').val(domain);
    postBeforeRefister();
  }

</script>
