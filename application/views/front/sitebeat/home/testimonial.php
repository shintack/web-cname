<!-- TESTIMONIAL -->
<div class="row b-lazy" id="testimonial" data-src="<?=$this->path_assets?>assets/images/background/testimony.webp">
  <div class="section" >
    <div class="col-md-12 text-center">
      <h2 class="font-white font-bold font-xl">Para Pengguna Aplikasi dePLAZA</h2>
    </div>
    <div class="col-md-12">
      <div class="swiper-testimonial">
        <div class="swiper-wrapper">
          <div class="swiper-slide">
            <div class="testimonial-header">
              <!-- <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="https://scontent-ams3-1.cdninstagram.com/vp/3e3b97c41bc473deb2f27c895370ca0b/5C99286F/t51.2885-15/sh0.08/e35/s640x640/37365441_827211870814622_1004774939967881216_n.jpg"> -->
              <img src="https://scontent-ams3-1.cdninstagram.com/vp/3e3b97c41bc473deb2f27c895370ca0b/5C99286F/t51.2885-15/sh0.08/e35/s640x640/37365441_827211870814622_1004774939967881216_n.jpg">

              <div class="testimonial-header-text">
                <h3 class="font-white font-medium
                font-sm">Kho Jhonatan</h3>
                <p class="font-black font-light font-xs">www.mbotak.com</p>
              </div>
            </div>
            <div class="text-center">
              <i class="fa fa-2x fa-quote-left font-white"></i>
              <p class="font-sm font-black font-light text-left">Pemilik Brand Ombotak.com yang berhasil menjual ratusan produk setiap hari nya di semua marketplace ternama seperti tokopedia, bukalapak, shoppe dan lazada."</p>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="testimonial-header">
              <!-- <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="https://media.licdn.com/dms/image/C5103AQFEUNVTuqarxQ/profile-displayphoto-shrink_200_200/0?e=1548892800&v=beta&t=Ep3vGkpgJ9yq-ntfJGo5mWMloPQLh9fss999zXdTfG8"> -->
              <img src="https://media.licdn.com/dms/image/C5103AQFEUNVTuqarxQ/profile-displayphoto-shrink_200_200/0?e=1548892800&v=beta&t=Ep3vGkpgJ9yq-ntfJGo5mWMloPQLh9fss999zXdTfG8">
              <div class="testimonial-header-text">
                <h3 class="font-white font-medium
                font-sm">Putri Wana</h3>
                <p class="font-black font-light font-xs">www.indo-dealz.com</p>
              </div>
            </div>
            <div class="text-center">
              <i class="fa fa-2x fa-quote-left font-white"></i>
              <p class="font-sm font-black font-light text-left">Supplier dan Founder dari Indo-dealz, Pemilik Brand ternama di marketplace tokopedia dan bukalapak dengan nama Indo Dealz yang menjual sampai ratusan produk per hari."</p>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="testimonial-header">
              <!-- <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="https://lh3.googleusercontent.com/a-/AAuE7mAnthDWKM68-4dERy_NfZYzarLkj5YhhhuCZjim=s640-rw-il"> -->
              <img src="https://lh3.googleusercontent.com/a-/AAuE7mAnthDWKM68-4dERy_NfZYzarLkj5YhhhuCZjim=s640-rw-il">
              <div class="testimonial-header-text">
                <h3 class="font-white font-medium
                font-sm">Ari Ginsha</h3>
                <p class="font-black font-light font-xs">www.ginshamall.comt</p>
              </div>
            </div>
            <div class="text-center">
              <i class="fa fa-2x fa-quote-left font-white"></i>
              <p class="font-sm font-black font-light text-left">Supplier produk-produk terlaris di marketplace tokopedia, bukalapak shopee dll dengan nama Ginsha Mall yang menjual sampai ribuan produk per hari."</p>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="testimonial-header">
              <!-- <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/testimonial/3.jpg"> -->
              <img src="<?=$this->path_assets?>assets/images/testimonial/3.jpg">
              <div class="testimonial-header-text">
                <h3 class="font-white font-medium
                font-sm">Koh Daniel</h3>
                <p class="font-black font-light font-xs">www.barangcina.com</p>
              </div>
            </div>
            <div class="text-center">
              <i class="fa fa-2x fa-quote-left font-white"></i>
              <p class="font-sm font-black font-light text-left">Supplier produk-produk terlaris di marketplace tokopedia, bukalapak shopee dll dengan nama brand Barangcina.com yang menjual sampai ribuan produk per hari."</p>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="testimonial-header">
              <!-- <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/testimonial/3.jpg"> -->
              <img src="<?=$this->path_assets?>assets/images/testimonial/3.jpg">
              <div class="testimonial-header-text">
                <h3 class="font-white font-medium
                font-sm">Koh Mulyanto</h3>
                <p class="font-black font-light font-xs">www.palugada.id</p>
              </div>
            </div>
            <div class="text-center">
              <i class="fa fa-2x fa-quote-left font-white"></i>
              <p class="font-sm font-black font-light text-left">Supplier ternama di marketplace tokopedia, bukalapak shopee dll dengan nama brand Palugada yang menjual sampai ribuan produk per hari."</p>
            </div>
          </div>
          <div class="swiper-slide">
            <div class="testimonial-header">
              <!-- <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/testimonial/3.jpg"> -->
              <img src="<?=$this->path_assets?>assets/images/testimonial/3.jpg">
              <div class="testimonial-header-text">
                <h3 class="font-white font-medium
                font-sm">Bpk Rojak</h3>
                <p class="font-black font-light font-xs">www.yuboboshop.com</p>
              </div>
            </div>
            <div class="text-center">
              <i class="fa fa-2x fa-quote-left font-white"></i>
              <p class="font-sm font-black font-light text-left">Supplier produk-produk terlaris di marketplace tokopedia, bukalapak shopee dll dengan nama brand yuboboshop yang menjual sampai ribuan produk per hari."</p>
            </div>
          </div>



          <div class="swiper-slide">
            <div class="testimonial-header">
              <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/testimonial/4.jpg">
              <div class="testimonial-header-text">
                <h3 class="font-white font-medium
                font-sm">Ibu Gunarta</h3>
                <p class="font-black font-light font-xs">www.veltramall.com</p>
              </div>
            </div>
            <div class="text-center">
              <i class="fa fa-2x fa-quote-left font-white"></i>
              <p class="font-sm font-black font-light text-left">Supplier produk-produk terlaris di marketplace tokopedia, bukalapak shopee dll dengan nama Brand Veltra Mall yang menjual sampai ribuan produk per hari."</p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- TESTIMONIAL END -->
