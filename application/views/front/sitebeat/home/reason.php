<style media="screen">
@media (min-width:1025px) {
  .cmt-100{
    margin-top: 100px;
  }
}
</style>

<!-- REASONS -->
<div class="row" id="reason">
  <div class="section">
    <div class="col-md-12 text-center" data-aos="fade">
      <h2 class="font-xl font-bold">Keuntungan Memiliki Deplaza </h2>
      <p class="font-md font-light">(Bisnis Sampingan Omset Jutaan)</p>
    </div>
      <!-- reason1 -->
    <div class="row">
      <div class="col-md-6 col-xs-12 reason-text">
        <div class="bg-shape col-xs-12 col-md-12" data-aos="fade-down">
          <h2 class="font-lg font-bold">Tidak punya produk ? Kami sediakan ribuan produk dengan harga supplier ( komisi s/d 1jt produk ) </h2>
        </div>
        <p class="font-light font-sm" data-aos="fade-up">Variasi produk super lengkap, mulai dari bumbu dapur, buku, baju, sepatu, jam tangan, sampai laptop (<a href="<?=base_url('produk')?>"><strong>lihat disini</strong></a>).</p>
        <p class="font-light font-sm" data-aos="fade-up">Harga Produk mulai dari Rp.1000 sampai Rp. 17.245.000. (<a target="_blank" href="https://dplasa.com/produk/READY-STOCK-iPhone-X-256GB-SILVER-Garansi-Resmi-Apple-International"><strong>iPhone X 256GB</strong></a>)</p>
        <p class="font-light font-sm" data-aos="fade-up"><strong>Anda juga dapat menjual produk sendiri</strong></p>
      </div>
      <div class="col-md-6 col-xs-12 text-right" data-aos="fade-left">
          <video class="cmt-100" src="<?=$this->path_assets?>assets/videos/upload-produk.webm" controls autoplay loop muted width="100%" height="100%"> </video>
        <!-- <img class="b-lazy cmt-100" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/reason/upload-otomatis-new.gif" width="100%" height="100%" > -->
        <!-- <img src="<?=$this->path_assets?>assets/images/reason/1.png" width="100%" height="100%" data-aos="fade-left"> -->
      </div>
    </div>

    <!-- reason2 -->
    <div class="row">

      <div class="col-md-6 col-xs-12 text-right" data-aos="fade-right">
          <video class="cmt-100" src="<?=$this->path_assets?>assets/videos/ganti-tema.webm" controls autoplay loop muted width="100%" height="100%"> </video>
        <!-- <img class="b-lazy cmt-100" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/reason/pilih-tema-fix.gif" width="100%" height="100%"> -->
        <!-- <img src="<?=$this->path_assets?>assets/images/reason/2.png" width="100%" height="100%" data-aos="fade-left"> -->
      </div>
      <div class="col-md-6 col-xs-12 reason-text">
        <div class="bg-shape col-xs-12 col-md-12" data-aos="fade-down">
          <h2 class="font-lg font-bold">Tersedia Puluhan Desain Untuk Toko Anda </h2>
        </div>
        <p class="font-light font-sm" data-aos="fade-up">Anda dapat memilih tema website anda sesuai selera anda sendiri. kami memberikan puluhan tema yang siap anda gunakan sehingga website anda akan semakin menarik.</p>
      </div>
    </div>

    <div class="row">
      <!-- reason3 -->
      <div class="col-md-6 col-xs-12 reason-text">
        <div class="bg-shape col-xs-12 col-md-12" >
          <h2 class="font-lg font-bold">Diajarkan Cara Berjualan Online </h2>
        </div>
        <p class="font-light font-sm" >Anda akan di berikan tips dan cara jualan online di social media, google dan marketplace. bedasarkan pengalaman puluhan orang yang berhasil menjual seribu produk per hari terus menerus.</p>
      </div>
      <div class="col-md-6 col-xs-12 text-right" >
          <video class="cmt-100" src="<?=$this->path_assets?>assets/videos/diajarkan-berjualan.webm" controls autoplay loop muted width="100%" height="100%"> </video>
        <!-- <img class="b-lazy cmt-100" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/reason/materi-marketing-online.gif" width="100%" height="100%" > -->
        <!-- <img src="<?=$this->path_assets?>assets/images/reason/3.png" width="100%" height="100%" data-aos="fade-left"> -->
      </div>
    </div>

  </div>
</div>
<!-- REASONS END -->
