
<!-- ORDER -->
<div class="row" id="getstarted">
  <div class="section">
    <div class="col-md-12 text-center" data-aos="fade">
      <h2 class="font-xl font-bold font-white">Miliki toko online anda sekarang</h2>
      <p class="font-md font-light font-white">Mulai berjualan dan nikmati keuntungan dari ribuan produk yang siap anda jual</p>
    </div>
    <div class="col-md-5 col-md-offset-1 col-xs-12">
      <div class="price bg-white text-center" data-aos="zoom-in-right" data-aos-delay="250">
        <div class="price-head">
          <h3 class="font-blue font-bold font-lg">BASIC</h3>
        </div>
        <div class="bg-orange-gradient price-pricing">
          <h3 class="font-white font-xl font-bold">Rp. 299.000</h3>
          <p class="font-black font-sm font-light">per bulan</p>
        </div>
        <div class="price-body">
          <p class="font-light font-sm"><s>Rp. 300.000</s> <br> 35% OFF</p>
          <hr>
          <!-- <a href="#" class="font-blue-lighten font-medium font-sm">Lihat Fitur Selengkapnya</a> -->

          <a href="#" class="btn btn-rounded bg-orange-gradient btn-block" data-toggle="modal" data-target="#domain">DAFTAR SEKARANG</a>
        </div>
      </div>
    </div>

    <div class="col-md-5 col-xs-12">
      <div class="price bg-white text-center" data-aos="zoom-in-right" data-aos-delay="500">
        <div class="price-head">
          <h3 class="font-blue font-bold font-lg">PREMIUM</h3>
        </div>
        <div class="bg-blue-gradient price-pricing">
          <h3 class="font-white font-xl font-bold">Rp. 399.000</h3>
          <p class="font-black font-sm font-light">per bulan</p>
        </div>
        <div class="price-body">
          <p class="font-light font-sm"><s>Rp. 300.000</s> <br> 35% OFF</p>
          <hr>
          <!-- <a href="#" class="font-blue-lighten font-medium font-sm">Lihat Fitur Selengkapnya</a> -->

          <a href="#" class="btn btn-rounded bg-blue-gradient btn-block" data-toggle="modal" data-target="#domain">DAFTAR SEKARANG</a>
        </div>
      </div>
    </div>

    <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/getstarted.png" width="60%" data-aos="fade-right">
  </div>
</div>
<!-- ORDER END -->
