

<div class="row" id="whychooseus">
  <div class="section">
    <div class="col-md-12 text-center">
      <h2 class="font-xl font-bold">Fitur canggih di aplikasi toko online dePLAZA</h2>
      <p class="font-md font-light">Toko online anda akan berjalan otomatis</p>
    </div>
    <div class="col-md-4 text-center">
      <div class="icon bg-white" >
        <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/whychooseus/1.png" height="100%">
      </div>
      <h3 class="font-bold font-sm" >CEK RESI REALTIME</h3>
      <p>Tidak perku khawatir barang tidak terkirim, Anda Bisa Memantau atau melacak keberadaan paket customer secara realtime sesuai resi<a class="btn" data-toggle="modal" data-target="#auto-resi"><strong>(LIHAT DEMO)</strong></a>.</p>
    </div>
    <div class="col-md-4 text-center">
      <div class="icon bg-white" >
        <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/whychooseus/2.png" height="100%">
      </div>
      <h3 class="font-bold font-sm" >AUTO UPLOAD PRODUK</h3>
      <p>Hanya dengan 1 klik ribuan Produk Dropship terasang secara instant di toko online anda <a class="btn" data-toggle="modal" data-target="#auto-upload" ><strong>(LIHAT DEMO)</strong></a>.</p>
    </div>
    <div class="col-md-4 text-center">
      <div class="icon bg-white" >
        <img class="b-lazy" src="https://thumbs.gfycat.com/RewardingBlandBluetonguelizard-max-1mb.gif" data-src="<?=$this->path_assets?>assets/images/whychooseus/3.png" height="100%">
      </div>
      <h3 class="font-bold font-sm" >SISTEM PEMBAYARAN OTOMATIS</h3>
      <p>Pembeli Tidak perlu konfirmasi pembayaran, Kami menyediakan pembayaran otomatis mulai dari Bank Transfer sampai Alfamart<a class="btn" data-toggle="modal" data-target="#auto-payment"><strong>(LIHAT DEMO)</strong></a>.</p>
    </div>
  </div>
</div>

<div id="auto-resi" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
      <video class="cmt-100" src="<?=$this->path_assets?>assets/videos/cek-resi.webm" controls autoplay loop muted width="100%" height="100%"> </video>
    <!-- <img class="cmt-100" src="<?=$this->path_assets?>assets/images/reason/upload-otomatis-new.gif" width="100%" height="100%"> -->
  </div>
</div>

<div id="auto-upload" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
      <video class="cmt-100" src="<?=$this->path_assets?>assets/videos/upload-produk.webm" controls autoplay loop muted width="100%" height="100%"> </video>
    <!-- <img class="cmt-100" src="<?=$this->path_assets?>assets/images/reason/upload-otomatis-new.gif" width="100%" height="100%"> -->
  </div>
</div>

<div id="auto-payment" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
      <video class="cmt-100" src="<?=$this->path_assets?>assets/videos/metode-pembayaran-lengkap.webm" controls autoplay loop muted width="100%" height="100%"> </video>
    <!-- <img class="cmt-100" src="<?=$this->path_assets?>assets/images/reason/pembayaran-otomatis.gif" width="100%" height="100%"> -->
  </div>
</div>
