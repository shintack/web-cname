<?php $total = 0;//vardump($order_data); ?>

<form class="" action="<?=base_url('payment/post')?>" method="post">

<!-- PAYMENT -->
<div id="payment">
  <div class="row bg-white">
    <div class="section">
      <div class="col-md-12 text-center">
        <ul class="wizard font-medium font-sm" data-aos="fade">
          <li class="complete"><a href="#">01 - INFO PELANGGAN</a></li>
          <li><a href="#">02 - INFORMASI PEMBAYARAN</a></li>
          <li class="disabled"><a href="#">03 - PAYMENT SELECTION</a></li>
        </ul>
      </div>
      <div class="col-md-12 text-center">
        <h1 class="font-lg font-bold" data-aos="fade-down">Bayar dan Aktifkan</h1>
        <p class="font-sm font-light" data-aos="fade-down" data-aos-delay="250">Selangkah lagi anda memiliki website dan siap berjualan</p>
      </div>
    </div>
  </div>
  <div class="row bg-grey text-left" id="checkout" data-aos="fade">
    <div class="section">
      <div class="col-md-12 col-xs-12">
        <table class="table font-sm done">
          <tr class="font-medium">
            <td width="70%">
              <img src="<?=$this->path_assets?>assets/images/payment/product.png">
              <?=$order_data['product_name']?>
              <input type="hidden" name="product_name[]" value="<?=$order_data['product_name']?>">
            </td>
            <td colspan="2" class="text-right">
              <?=$order_data['product_price']?>
              <input type="hidden" name="product_price[]" value="<?=$order_data['product_price_int']?>">
            </td>
          </tr>
          <?php $total = $order_data['product_price_int'];
           foreach($order_data['product_detail'] as $detail): $total += $detail['domain_price']; ?>
          <tr class="font-light">
            <td>
              <img src="<?=$this->path_assets?>assets/images/payment/domain.png">
              <?=$detail['domain_name']?>
              <input type="hidden" name="product_name[]" value="<?=$detail['domain_name']?>">
            </td>
            <td width="15%">
              1 tahun
            </td>
            <td width="15%">
              <?='Rp. '.number_format($detail['domain_price'])?>
              <input type="hidden" name="product_price[]" value="<?=$detail['domain_price']?>">
            </td>
          </tr>
        <?php endforeach; ?>
        <?php /*
          <tr class="font-light">
            <td>
              <img src="<?=$this->path_assets?>assets/images/payment/ssl.png">
              Standard SSL Security
            </td>
            <td width="15%">
              1 tahun
            </td>
            <td width="15%">
              1 Gratis
            </td>
          </tr>
          */ ?>
          <tr>
            <td id="code">
              <!-- <a href="#" class="font-light"><u>Punya kode promo?</u></a> -->
            </td>
            <td colspan="2" class="text-right font-bold font-md"><?='Rp. '.number_format($total)?></td>
          </tr>
        </table>
      </div>
    </div>
  </div>

  <?php if ( isset($pay_avaliable) && is_array($pay_avaliable) ): ?>

  <div class="row bg-white" id="payment-method">
    <form class="section">
      <div class="col-md-12 text-center">
        <ul class="payment-method font-medium font-md font-grey" data-aos="fade">
          <?php foreach ($pay_avaliable as $pa): ?>
            <li id="<?=$pa->cara_bayar->pay_code?>" class=""><a onclick="payMethod('<?=$pa->cara_bayar->pay_code?>')" href="#payment-method" class="" ><i class="fa <?=$pa->cara_bayar->icon?>"></i> <?=$pa->cara_bayar->pay_name?> </a></li>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class="col-sm-3"></div>
      <div class="col-md-6 p box-payment">
          <input type="hidden" name="pay_method" value="">
          <?php foreach ($pay_avaliable as $pa): ?>
            <div class="panel panel-default" id="tab-<?=$pa->cara_bayar->pay_code?>">
              <div class="panel-heading"></div>
              <div class="panel-body">
                <?php foreach ($pa->bayar_via as $bv): ?>
                    <div class="radio">
                      <label> <input type="radio" name="merchant_code" value="<?=$bv->merchant_code?>" style="margin-top: 15px;">
                        <img src="<?=$bv->image?>" alt="" height="50px">
                        <?=$bv->merchant_name?>
                      </label>
                    </div>
                <?php endforeach; ?>
              </div>
            </div>
          <?php endforeach; ?>

        <!-- PANEL -->
        <!-- <div class="payment-bank text-center bg-grey" data-aos="fade">
          <img src="<?=$this->path_assets?>assets/images/payment/bank/mandiri.png">
          <img src="<?=$this->path_assets?>assets/images/payment/bank/bca.png">
          <img src="<?=$this->path_assets?>assets/images/payment/bank/bni.png">
          <img src="<?=$this->path_assets?>assets/images/payment/bank/atm_bersama.png">
          <img src="<?=$this->path_assets?>assets/images/payment/bank/prima.png">
          <img src="<?=$this->path_assets?>assets/images/payment/bank/alto.png">
          <img src="<?=$this->path_assets?>assets/images/payment/bank/alfamart.png">
        </div>
        <br> -->

        <!-- PANEL END -->
      </div>
      <div class="col-md-12 text-center" id="tab-button">
        <input type="submit" value="LANJUTKAN" class="btn btn-rounded bg-blue font-white">
      </div>
    </form>
  </div>

  <?php endif; ?>

</div>


</form>

<script type="text/javascript">
  $("#tab-02").hide();
  $("#tab-03").hide();
  $("#tab-button").hide();
</script>
