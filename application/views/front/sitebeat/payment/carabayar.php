<?php //vardump($bank_data->bank_detail[0]->image); ?>
<!-- PAYMENT -->
<div id="payment">
  <div class="row bg-white">
    <div class="section">
      <div class="col-md-12 text-center">
        <ul class="wizard font-medium font-sm" data-aos="fade">
          <li class="complete"><a href="#">01 - INFO PELANGGAN</a></li>
          <li class="complete"><a href="#">02 - INFORMASI PEMBAYARAN</a></li>
          <li><a href="#">03 - PAYMENT SELECTION</a></li>
        </ul>
      </div>

      <div class="col-md-12 p transfer box-payment">
        <!-- PANEL -->
        <div class="panel panel-default" data-aos="fade">
          <div class="panel panel-heading text-center">
            <img src="<?=$bank_data->bank_detail[0]->image?>">
            <!-- <img src="assets/images/payment/bank/bca.png">
            <img src="assets/images/payment/bank/bni.png">
            <img src="assets/images/payment/bank/atm_bersama.png">
            <img src="assets/images/payment/bank/prima.png">
            <img src="assets/images/payment/bank/alto.png">
            <img src="assets/images/payment/bank/alfamart.png"> -->
          </div>
          <div class="panel-body">
            <p class="font-light font-sm text-center">Nomor Pembayaran</p>
            <h3 class="font-bold font-md text-center font-blue"><?=$payment_info['no_pembayaran']?></h3>
            <table class="table font-md done">
              <tr>
                <td class="font-light">Jumlah Pembayaran</td>
                <td class="font-bold text-right"><?=$payment_info['total']?></td>
              </tr>
              <tr>
                <td class="font-light">Nomor Invoice</td>
                <td class="font-bold text-right font-orange"><?=$payment_info['invoice']?></td>
              </tr>
            </table>
            <?=$bank_data->panduan_bayar?>
            <?php /*
            <h4 class="font-blue trigger font-md" id="atm-trigger">Cara Membayar via ATM <i class="fa fa-chevron-down font-black"></i></h4>
            <ol id="atm" class="font-sm font-light">
              <li>Masukkan PIN</li>
              <li>Pilih <b>"Transfer"</b>. Apabila menggunakan ATM Bank Lain, pilih <b>"Transaksi lainnya"</b> lalu <b>"Transfer"</b></li>
              <li>Pilih <b>"Ke Rek Bank Lain"</b></li>
              <li>Masukkan Kode Bank Permata <b>(013)</b> dilkuti 16 digit kode bayar <b class="font-red">8965034700221213</b> sebagai rekening tujuan, kemudian tekan <b>"Benar"</b></li>
              <li>Masukkan Jumlah pembayaran sesuai dengan yang ditagihkan (Jumlah yang ditransfer harus sama persis, tldak boleh leblh dan kurang). Jumlah nominal yang tidak sesuai dengan tagihan akan menyebabkan transaksi gagal</li>
              <li>Muncul Layar Konfirmasi Transfer yang berlsi nomor rekening tujuan Bank Permata dan Nama berserta jumlah yang dibayar, jika sudah benar, Tekan <b>"Benar"</b></li>
              <li>Selesai</li>
            </ol>
            <h4 class="font-blue trigger font-md" id="ebanking-trigger">Cara Membayar via Internet Banking <i class="fa fa-chevron-down font-black"></i></h4>
            <ol id="ebanking" class="font-sm font-light">
              <i class="font-red font-light"><b>Keterangan :</b> Pembayaran tidak bisa dilakukan di Internet Banking BCA (KlikBCA)</i><br><br>

              <li>Login ke dalam akun internet Banking</li>
              <li>Pilih <b>"Transfer dan pilih Bank Lainnya"</b>. Pilih Bank Permata <b>(013)</b> sebagai rekening tujuan</li>
              <li>Masukkan jumlah pembayaran sesuai dengan yang ditagihkan</li>
              <li>Isi nomor rekening tujuan dengan 16 digit kode pembayaran <b class="font-red">8965034700221213</b></li>
              <li>Muncul layar konfirmasi Transfer yang berisi nomor rekening tujuan Bank Permata dan Nama beserta Jumlah yang dibayar. Jika sudah benar, tekan <b>"Benar"</b></li>
              <li>Selesai</li>
            </ol>
            */ ?>
            <div class="text-center font-white">
              <a target="_blank" href="<?=base_url('payment/invoice/'.$payment_info['invoice'])?>" class="text-center btn btn-rounded bg-blue">Cek Status Pembayaran</a>
            </div>
          </div>
        </div>
        <!-- PANEL END -->
      </div>
      <div class="col-md-12 text-center">
      </div>
    </div>
  </div>
</div>
<!-- PAYMENT END -->
<?php //vardump($invoice_data,TRUE); ?>
