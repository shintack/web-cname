<div class="row bg-white terimakasih" id="register">
  <div class="section">
    <div class="col-md-4 col-xs-12 reg-img">
      <img src="<?=$this->path_assets?>assets/images/register.png" width="95%" data-aos="fade-right">
    </div>
    <div class="col-md-8 col-xs-12 font-sm font-light thanks">
      <h2 class="font-bold font-lg font-blue">Terima kasih</h2>
      <p>Kami sudah menerima pesananmu.</p>
      <p>Pembayaran berhasil di konfirmasi.</p>
      <p>Silahkan <b class="font-blue"><a href="https://wa.me/6282122941869" target="_blank">hubungi kami</a></b> jika kamu memiliki pertanyaan.</p>

      <div class="panel panel-default col-md-12 col-xs-12" data-aos="fade">
        <div class="panel-body col-md-12 col-xs-12 pending" >
          <div class="col-md-2 col-xs-12 text-center refresh">
            <center>
              <div class="refresh-icon text-center">
                <i class="fa fa-refresh fa-3x fa-spin font-blue"></i>
              </div>
            </center>
          </div>
          <div class="col-md-10 col-xs-12 wait">
            <p class="font-bold font-md">Sitebeat-mu Sedang di proses</p>
            <p>Pembayaran telah kami terima, Sitebeat-mu sedang di proses tim kami, kami akan menghubungi kamu paling lambat 2x24 jam setelah pembayaran kami terima.</p>
          </div>
        </div>
      </div>

      <div class="text-right font-white" data-aos="fade">
        <a href="<?=site_url('dashboard')?>" class="btn btn-rounded bg-blue">Kembali ke dashboard</a>
      </div>
    </div>
  </div>
</div>
