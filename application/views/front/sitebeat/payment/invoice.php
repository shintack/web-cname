<!-- PAYMENT -->
<div id="payment">
  <div class="row bg-white">
    <div class="section">
      <div class="col-md-10 col-md-offset-1 text-center">
        <h1 class="font-lg font-bold" data-aos="fade-down">Menunggu Pembayaran</h1>
        <p class="font-sm font-light" data-aos="fade-down" data-aos-delay="250">Kami sudah menerima pesanan kamu, sekarang kami sedang menunggu konfirmasi pembayaran.<br>Silahkan <b class="font-blue"><a href="#">hubungi kami</a></b> jika kamu memiliki pertanyaan.</p>
      </div>

      <div class="col-md-12 p col-xs-12 box-payment">
        <!-- PANEL -->
        <div class="payment-panel col-md-12 col-xs-12 font-light font-sm" data-aos="fade">
          <div class="col-md-5 col-xs-12 col-md-push-5">
            <p class="font-md font-bold">Faktur #37882571</p>
            <p>08 Jan 2019</p>
            <p>ID Member M8777451</p>
          </div>
          <div class="col-md-5 col-xs-12 col-md-pull-5">
            <p class="font-bold">Sitebeat <br>Beat teh ordinary</p>
            <p>P: +62-21-29223056</p>
            <p>E: customercare@sitebeat.com</p>
            <p>W: manage.sitebeat.com</p>
          </div>
          <div class="col-md-2 col-xs-12 text-center">
            <img src="assets/images/payment/jatuhtempo.png">
            <p class="font-red font-medium">Jatuh Tempo Dalam 30 Hari</p>
          </div>
          <div class="col-md-12"><hr></div>

          <div class="col-md-5 col-xs-12">
            <p>Ditagihkan kepada:</p>
            <p class="font-medium">
              Khoirudin.id, <br>
              Kp.ceringin 02/08 <br>
              Bogor, Jawa Barat 16920 <br>
              Negara: ID
            </p>
          </div>
          <div class="col-md-5 col-xs-12">
            <p>Info pembayaran:</p>
            <p class="font-medium">
              Tipe Pembayaran: Adyen <br>
              Lunas: Rp. 0
            </p>
          </div>
          <div class="col-md-2 col-xs-12 text-center">
            <a href="#" data-toggle="tooltip" title="Cetak" data-placement="bottom"><i class="fa fa-print fa-2x font-grey"></i></a>
            <a href="#" data-toggle="tooltip" title="Download" data-placement="bottom"><i class="fa fa-download fa-2x font-grey"></i></a>
          </div>

          <div class="col-md-12 col-xs-12" style="overflow-x: auto;">
            <div class="" style="overflow-x: auto;">
              <table class="table">
                <tr class="bg-grey text-center font-bold">
                  <th width="40%">ITEM</th>
                  <th>PERIODE</th>
                  <th>HARGA SATUAN</th>
                  <th>QTY</th>
                  <th class="text-right">JUMLAH</th>
                </tr>
                <tr>
                  <td>SitebeatPRO eCommerce for 1 year<br><b class="font-blue-lighten">khoirudin.id</b></td>
                  <td>1 tahun</td>
                  <td>Rp. 5.532.000</td>
                  <td>1</td>
                  <td class="text-right">Rp. 5.532.000</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="col-md-4 col-md-offset-8 col-xs-12 text-right total">
            <table width="100%">
              <tr>
                <td width="30%">Subtotal</td>
                <td width="10%">:</td>
                <td width="60%">Rp. 5.532.000</td>
              </tr>
              <tr>
                <td>Tax</td>
                <td>:</td>
                <td>Rp. 0</td>
              </tr>
              <tr class="font-md font-bold">
                <td>Total</td>
                <td>:</td>
                <td>Rp. 5.532.000</td>
              </tr>
              <tr>
                <td width="30%">Sisa</td>
                <td width="10%">:</td>
                <td width="60%">Rp. 5.532.000</td>
              </tr>
            </table>
          </div>
          <div class="col-md-5 font-grey">
            <p>Semua harga ditampilkan dan dibebankan dalam Indonesia Rupiah (Rp)</p>
          </div>
        </div>
        <!-- PANEL END -->
      </div>
      <div class="col-md-6 font-grey font-sm back-to-info">
        <a href="#"><i class="fa fa-arrow-left"></i> Back to Shipping Info</a>
      </div>
      <div class="col-md-6 text-right">
        <input type="submit" value="BAYAR & AKTIFKAN" class="btn btn-rounded bg-blue font-white">
      </div>
    </div>
  </div>
</div>
<!-- PAYMENT END -->
