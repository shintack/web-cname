<div style="margin-top: 100px;">
</div>

<div class="error-page">
  <h2 class="headline text-yellow"> 404</h2>

  <div class="error-content">
    <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

    <p>
      Maaf, Halaman yg dituju gak ketemu. <br>
      Kembali <a href="index.php"> kelajalan yg benar </a> kalo nyasar.....
    </p>

    <form class="search-form">
      <div class="input-group">
        <input name="search" class="form-control" placeholder="Search" type="text">

        <div class="input-group-btn">
          <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
          </button>
        </div>
      </div>
      <!-- /.input-group -->
    </form>
  </div>
  <!-- /.error-content -->
</div>