
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalAdd"> <i class="fa fa-plus"></i> Add</button>
            <?php /*
            <a href="<?php echo site_url($module."/users/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            */ ?>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <!-- tabel here -->
        <div style="overflow: auto; margin-bottom: 10px;">
            <table id="table" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th align="center" width="10px">No</th>
                        <th>Ip Address</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Created On</th>
                        <th>Last Login</th>
                        <th>Active</th>
                        <th>First Name</th>
                        <!-- <th>Phone</th>             -->
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                </tbody>

                <tfoot>
                    <tr>
                        <th align="center" width="10px">No</th>
                        <th>Ip Address</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Created On</th>
                        <th>Last Login</th>
                        <th>Active</th>
                        <th>First Name</th>
                        <!-- <th>Phone</th>             -->
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- ./tabel here -->

        <div class="row">
            <div class="col-md-6">
                <?php echo anchor(site_url($module.'/users/excel'), '<i class="fa fa-file-excel-o"></i>', 'title="Export to Excel" class="btn btn-success btn-sm"'); ?>
                <?php echo anchor(site_url($module.'/users/word'), '<i class="fa fa-file-word-o"></i>', 'title="Export to Word"class="btn btn-info btn-sm"'); ?>
                <?php echo anchor(site_url($module.'/users/pdf'), '<i class="fa fa-file-pdf-o"></i>', 'title="Export to Pdf"class="btn btn-danger btn-sm"'); ?>
            </div>
            <div class="col-md-6 text-right"></div>
        </div>

    </div>
</div>


<!-- Modal -->
<div id="myModalAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
<?php echo form_open($input["action"]); ?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add <?=$title?></h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">

          <tr>
            <td>Ip Address <?php echo form_error('ip_address') ?></td>
            <td><input type="text" class="form-control" name="ip_address" id="ip_address" placeholder="Ip Address" value="<?php echo $input['ip_address']; ?>" /></td>
          </tr>
          <tr>
            <td>Username <?php echo form_error('username') ?></td>
            <td><input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $input['username']; ?>" /></td>
          </tr>
          <tr>
            <td>Password <?php echo form_error('password') ?></td>
            <td><input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $input['password']; ?>" /></td>
          </tr>
          <tr>
            <td>Salt <?php echo form_error('salt') ?></td>
            <td><input type="text" class="form-control" name="salt" id="salt" placeholder="Salt" value="<?php echo $input['salt']; ?>" /></td>
          </tr>
          <tr>
            <td>Email <?php echo form_error('email') ?></td>
            <td><input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $input['email']; ?>" /></td>
          </tr>
          <tr>
            <td>Activation Code <?php echo form_error('activation_code') ?></td>
            <td><input type="text" class="form-control" name="activation_code" id="activation_code" placeholder="Activation Code" value="<?php echo $input['activation_code']; ?>" /></td>
          </tr>
          <tr>
            <td>Forgotten Password Code <?php echo form_error('forgotten_password_code') ?></td>
            <td><input type="text" class="form-control" name="forgotten_password_code" id="forgotten_password_code" placeholder="Forgotten Password Code" value="<?php echo $input['forgotten_password_code']; ?>" /></td>
          </tr>
          <tr>
            <td>Forgotten Password Time <?php echo form_error('forgotten_password_time') ?></td>
            <td><input type="text" class="form-control" name="forgotten_password_time" id="forgotten_password_time" placeholder="Forgotten Password Time" value="<?php echo $input['forgotten_password_time']; ?>" /></td>
          </tr>
          <tr>
            <td>Remember Code <?php echo form_error('remember_code') ?></td>
            <td><input type="text" class="form-control" name="remember_code" id="remember_code" placeholder="Remember Code" value="<?php echo $input['remember_code']; ?>" /></td>
          </tr>
          <tr>
            <td>Created On <?php echo form_error('created_on') ?></td>
            <td><input type="text" class="form-control" name="created_on" id="created_on" placeholder="Created On" value="<?php echo $input['created_on']; ?>" /></td>
          </tr>
          <tr>
            <td>Last Login <?php echo form_error('last_login') ?></td>
            <td><input type="text" class="form-control" name="last_login" id="last_login" placeholder="Last Login" value="<?php echo $input['last_login']; ?>" /></td>
          </tr>
          <tr>
            <td>Active <?php echo form_error('active') ?></td>
            <td><input type="text" class="form-control" name="active" id="active" placeholder="Active" value="<?php echo $input['active']; ?>" /></td>
          </tr>
          <tr>
            <td>First Name <?php echo form_error('first_name') ?></td>
            <td><input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?php echo $input['first_name']; ?>" /></td>
          </tr>
          <tr>
            <td>Last Name <?php echo form_error('last_name') ?></td>
            <td><input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo $input['last_name']; ?>" /></td>
          </tr>
          <tr>
            <td>Company <?php echo form_error('company') ?></td>
            <td><input type="text" class="form-control" name="company" id="company" placeholder="Company" value="<?php echo $input['company']; ?>" /></td>
          </tr>
          <tr>
            <td>Phone <?php echo form_error('phone') ?></td>
            <td><input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $input['phone']; ?>" /></td>
          </tr>
        </table>
        <input type="hidden" name="id" value="<?php echo $input['id']; ?>" /> 
      </div>
      <div class="modal-footer">
            <button type="submit" class="btn btn-primary"><?php echo $input["button"] ?></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
<?php form_close(); ?>

  </div>
</div>


