

<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
      <?php echo form_open($action); ?>
        <table class="table table-bordered">

          <tr>
            <td> <label>Ip Address</label> <i> <?php echo form_error('ip_address') ?> </i> </td>
            <td><input type="text" class="form-control" name="ip_address" id="ip_address" placeholder="Ip Address" value="<?php echo $ip_address; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Username</label> <i> <?php echo form_error('username') ?> </i> </td>
            <td><input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Password</label> <i> <?php echo form_error('password') ?> </i> </td>
            <td><input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Salt</label> <i> <?php echo form_error('salt') ?> </i> </td>
            <td><input type="text" class="form-control" name="salt" id="salt" placeholder="Salt" value="<?php echo $salt; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Email</label> <i> <?php echo form_error('email') ?> </i> </td>
            <td><input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Activation Code</label> <i> <?php echo form_error('activation_code') ?> </i> </td>
            <td><input type="text" class="form-control" name="activation_code" id="activation_code" placeholder="Activation Code" value="<?php echo $activation_code; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Forgotten Password Code</label> <i> <?php echo form_error('forgotten_password_code') ?> </i> </td>
            <td><input type="text" class="form-control" name="forgotten_password_code" id="forgotten_password_code" placeholder="Forgotten Password Code" value="<?php echo $forgotten_password_code; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Forgotten Password Time</label> <i> <?php echo form_error('forgotten_password_time') ?> </i> </td>
            <td><input type="text" class="form-control" name="forgotten_password_time" id="forgotten_password_time" placeholder="Forgotten Password Time" value="<?php echo $forgotten_password_time; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Remember Code</label> <i> <?php echo form_error('remember_code') ?> </i> </td>
            <td><input type="text" class="form-control" name="remember_code" id="remember_code" placeholder="Remember Code" value="<?php echo $remember_code; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Created On</label> <i> <?php echo form_error('created_on') ?> </i> </td>
            <td><input type="text" class="form-control" name="created_on" id="created_on" placeholder="Created On" value="<?php echo $created_on; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Last Login</label> <i> <?php echo form_error('last_login') ?> </i> </td>
            <td><input type="text" class="form-control" name="last_login" id="last_login" placeholder="Last Login" value="<?php echo $last_login; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Active</label> <i> <?php echo form_error('active') ?> </i> </td>
            <td><input type="text" class="form-control" name="active" id="active" placeholder="Active" value="<?php echo $active; ?>" /></td>
          </tr>
          <tr>
            <td> <label>First Name</label> <i> <?php echo form_error('first_name') ?> </i> </td>
            <td><input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?php echo $first_name; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Last Name</label> <i> <?php echo form_error('last_name') ?> </i> </td>
            <td><input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo $last_name; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Company</label> <i> <?php echo form_error('company') ?> </i> </td>
            <td><input type="text" class="form-control" name="company" id="company" placeholder="Company" value="<?php echo $company; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Phone</label> <i> <?php echo form_error('phone') ?> </i> </td>
            <td><input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="<?php echo $phone; ?>" /></td>
          </tr>
          <tr>
            <td colspan='2'>
              <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
              <a href="<?php echo site_url($module.'/users') ?>" class="btn btn-default">Cancel</a>
            </td>
          </tr>
        </table>
          <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
      <?php form_close(); ?>
    </div>
</div> 