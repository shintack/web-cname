
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalAdd"> <i class="fa fa-plus"></i> Add</button>
            <?php /*
            <a href="<?php echo site_url($module."/menu/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            */ ?>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <!-- tabel here -->
        <div style="overflow: auto; margin-bottom: 10px;">
            <table id="table" class="table table-bordered table-striped table-hover" style="margin-bottom: 10px">
                <thead class="btn-primary">
                    <tr>
                        <th align="center" width="10px">No</th>
                        <th>Name</th >
                        <th>Module</th>
                        <th>Class</th>
                        <th>Link</th>
                        <th>Icon</th>
                        <!-- <th>Is Active</th>
                        <th>Is Arrage</th> -->
                        <th>Is Parent</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th align="center" width="10px">No</th>
                        <th>Name</th >
                        <th>Module</th>
                        <th>Class</th>
                        <th>Link</th>
                        <th>Icon</th>
                        <!-- <th>Is Active</th>
                        <th>Is Arrage</th> -->
                        <th>Is Parent</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- ./tabel here -->

        <div class="row">
            <div class="col-md-6">
                <?php echo anchor(site_url($module.'/menu/excel'), '<i class="fa fa-file-excel-o"></i>', 'title="Export to Excel" class="btn btn-success btn-sm"'); ?>
                <?php echo anchor(site_url($module.'/menu/word'), '<i class="fa fa-file-word-o"></i>', 'title="Export to Word"class="btn btn-info btn-sm"'); ?>
                <?php echo anchor(site_url($module.'/menu/pdf'), '<i class="fa fa-file-pdf-o"></i>', 'title="Export to Pdf"class="btn btn-danger btn-sm"'); ?>
            </div>
            <div class="col-md-6 text-right">
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
<div id="myModalAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
<?php echo form_open($input["action"]); ?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add <?=$title?></h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
          <tr>
            <td> <label>Name </label> <i> <?php echo form_error('name') ?> </i></td>
            <td><input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $input['name']; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Module </label> <i> <?php echo form_error('modules') ?> </i></td>
            <td><input type="text" class="form-control" name="modules" id="modules" placeholder="Module Name" value="<?php echo $input['modules']; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Class </label> <i> <?php echo form_error('class') ?> </i></td>
            <td><input type="text" class="form-control" name="class" id="class" placeholder="Class Name" value="<?php echo $input['class']; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Link </label> <i> <?php echo form_error('link') ?> </i></td>
            <td><input type="text" class="form-control" name="link" id="link" placeholder="Link" value="<?php echo $input['link']; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Icon </label> <i> <?php echo form_error('icon') ?> </i></td>
            <td><input type="text" class="form-control" name="icon" id="icon" placeholder="Icon" value="<?php echo $input['icon']; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Is Active </label> <i> <?php echo form_error('is_active') ?> </i></td>
            <td>
                <select class="form-control" name="is_active" id="is_active">
                    <option value="1" >Aktif</option>
                    <option value="0" >Non Aktif</option>
                </select>
            </td>
          </tr>
          <tr>
            <td> <label>Is Arrage </label> <i> <?php echo form_error('is_arrage') ?> </i></td>
            <td><input type="text" class="form-control" name="is_arrage" id="is_arrage" placeholder="Is Arrage" value="<?php echo $input['is_arrage']; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Is Parent </label> <i> <?php echo form_error('is_parent') ?> </i></td>
            <td>
                <?=cmb_dinamis('is_parent','menu','name','id',$input['is_parent'],FALSE);?>
            </td>
          </tr>
          <tr>
            <td> <label>Type </label> <i> <?php echo form_error('type') ?> </i></td>
            <td>
                <select class="form-control" name="type" id="type">
                    <option value="backend" >Backend</option>
                    <option value="frontend">Frontend</option>
                </select>
            </td>
          </tr>
        </table>
        <input type="hidden" name="id" value="<?php echo $input['id']; ?>" />
      </div>
      <div class="modal-footer">
            <button type="submit" class="btn btn-primary"><?php echo $input["button"] ?></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
<?php form_close(); ?>

  </div>
</div>
