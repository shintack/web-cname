
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
      <?php echo form_open($action); ?>
        <table class="table table-bordered">
          <tr>
            <td> <label>Name</label> <i> <?php echo form_error('name') ?> </i> </td>
            <td><input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Module </label> <i> <?php echo form_error('modules') ?> </i></td>
            <td><input type="text" class="form-control" name="modules" id="modules" placeholder="Module Name" value="<?php echo $modules; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Class </label> <i> <?php echo form_error('class') ?> </i></td>
            <td><input type="text" class="form-control" name="class" id="class" placeholder="Class Name" value="<?php echo $class; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Link</label> <i> <?php echo form_error('link') ?> </i> </td>
            <td><input type="text" class="form-control" name="link" id="link" placeholder="Link" value="<?php echo $link; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Icon</label> <i> <?php echo form_error('icon') ?> </i> </td>
            <td><input type="text" class="form-control" name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Is Active</label> <i> <?php echo form_error('is_active') ?> </i> </td>
            <td>
              <select class="form-control" name="is_active" id="is_active">
                  <option value="1" <?=($is_active=='1')?'selected':''?> >Aktif</option>
                  <option value="0" <?=($is_active=='0')?'selected':''?>>Non Aktif</option>
              </select>
            </td>
          </tr>
          <tr>
            <td> <label>Is Arrage</label> <i> <?php echo form_error('is_arrage') ?> </i> </td>
            <td><input type="text" class="form-control" name="is_arrage" id="is_arrage" placeholder="Is Arrage" value="<?php echo $is_arrage; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Is Parent</label> <i> <?php echo form_error('is_parent') ?> </i> </td>
            <td>
              <?=cmb_dinamis('is_parent','menu','name','id',$is_parent,FALSE);?>
            </td>
          </tr>
          <tr>
            <td> <label>Type</label> <i> <?php echo form_error('type') ?> </i> </td>
            <td>
              <select class="form-control" name="type" id="type">
                  <option value="backend" <?=($type=='backend')?'selected':''?> >Backend</option>
                  <option value="frontend" <?=($type=='frontend')?'selected':''?>>Frontend</option>
              </select>
            </td>
          </tr>
          <tr>
            <td colspan='2'>
              <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
              <a href="<?php echo site_url($module.'/menu') ?>" class="btn btn-default">Cancel</a>
            </td>
          </tr>
        </table>
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
      <?php form_close(); ?>
    </div>
</div>
