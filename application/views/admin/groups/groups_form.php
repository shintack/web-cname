

<!-- <div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>
</div> -->

<?php echo form_open($action); ?>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered">

          <tr>
            <td> <label>Name</label> <i> <?php echo form_error('name') ?> </i> </td>
            <td><input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Description</label> <i> <?php echo form_error('description') ?> </i> </td>
            <td><input type="text" class="form-control" name="description" id="description" placeholder="Description" value="<?php echo $description; ?>" /></td>
          </tr>
          <tr>
            <td colspan='2'>
              <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
              <a href="<?php echo site_url($module.'/groups') ?>" class="btn btn-default">Cancel</a>
            </td>
          </tr>
        </table>
          <input type="hidden" name="id" value="<?php echo $id; ?>" />

    </div>
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> Hak Akses Menu </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalAdd"> <i class="fa fa-plus"></i> Add</button>
            <?php /*
            <a href="<?php echo site_url($module."/menu/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            */ ?>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <!-- tabel here -->
        <form action="base_url('groups/akses')" method="POST">
        <div style="overflow: auto; margin-bottom: 10px;">
            <table class="table table-bordered table-striped table-hover" style="margin-bottom: 10px">
                <thead class="btn-primary">
                    <tr>
                        <th align="center" width="10px">No</th>
                        <th>Name</th>
                        <th>Link</th>
                        <th>Icon</th>
                        <th>Parent</th>
                        <th>Type</th>
                        <th>Read</th>
                        <th>List</th>
                        <th>Create</th>
                        <th>Update</th>
                        <th>Delete</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php $start=0;foreach($menu_data as $menu):?>
                    <tr>
                        <td width="10px"><?php echo ++$start ?></td>
                        <td><?php echo $menu->name ?></td>
                        <td><?php echo $menu->link ?></td>
                        <td align="center"><i class="fa <?=$menu->icon?>"> </i> </td>
                        <td><?=menu_name($menu->is_parent);?></td>
                        <td><?php echo $menu->type ?></td>
                        <td align="center">
                          <input type="checkbox" class="check" name="read[]" <?=(get_akses($id,$menu->id,'lihat'))?'checked':'';?> value="<?=$menu->id?>" >
                        </td>
                        <td align="center">
                          <input type="checkbox" class="check" name="list[]" <?=(get_akses($id,$menu->id,'daftar'))?'checked':'';?> value="<?=$menu->id?>">
                        </td>
                        <td align="center">
                          <input type="checkbox" class="check" name="create[]" <?=(get_akses($id,$menu->id,'tambah'))?'checked':'';?> value="<?=$menu->id?>">
                        </td>
                        <td align="center">
                          <input type="checkbox" class="check" name="update[]" <?=(get_akses($id,$menu->id,'ubah'))?'checked':'';?> value="<?=$menu->id?>">
                        </td>
                        <td align="center">
                          <input type="checkbox" class="check" name="delete[]" <?=(get_akses($id,$menu->id,'hapus'))?'checked':'';?> value="<?=$menu->id?>">
                        </td>
                        <td style="text-align:center" width="140px">
                        <?php
                        echo anchor(site_url($module.'/menu/read/'.$menu->id),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-info btn-sm'));
                        echo '  ';
                        echo anchor(site_url($module.'/menu/update/'.$menu->id),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm'));
                        echo '  ';
                        //echo anchor(site_url($module.'/menu/delete/'.$menu->id),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); ?>
                        </td>
                    </tr>

                <?php endforeach; ?>
                </tbody>
                <tr>
                  <td colspan="11">
                    <input type="button" class="btn btn-default" onclick='selectAll()' value="Select All"/>
                    <input type="button" class="btn btn-default" onclick='UnSelectAll()' value="Unselect All"/>
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> SUBMIT</button>

                  </td>
                </tr>
            </table>
        </div>
        <!-- ./tabel here -->
        </form>

        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6 text-right"></div>
        </div>

    </div>
</div>

<?php form_close(); ?>
<script type="text/javascript">

function selectAll() {
    var items = document.getElementsByClassName('check');
    for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
            items[i].checked = true;
    }
}

function UnSelectAll() {
    var items = document.getElementsByClassName('check');
    for (var i = 0; i < items.length; i++) {
        if (items[i].type == 'checkbox')
            items[i].checked = false;
    }
}
</script>
