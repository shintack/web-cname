<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Actions_logs List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>User Id</th>
		<th>Email Login</th>
		<th>User Agent</th>
		<th>Ip Address</th>
		<th>Note</th>
		<th>Url</th>
		<th>Actions</th>
		<th>Timestamp</th>
		
            </tr><?php
            foreach ($logs_data as $logs)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $logs->user_id ?></td>
		      <td><?php echo $logs->email_login ?></td>
		      <td><?php echo $logs->user_agent ?></td>
		      <td><?php echo $logs->ip_address ?></td>
		      <td><?php echo $logs->note ?></td>
		      <td><?php echo $logs->url ?></td>
		      <td><?php echo $logs->actions ?></td>
		      <td><?php echo $logs->timestamp ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>