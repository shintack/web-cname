

<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
      <?php echo form_open($action); ?>
        <table class="table table-bordered">

          <tr>
            <td> <label>User Id</label> <i> <?php echo form_error('user_id') ?> </i> </td>
            <td><input type="text" class="form-control" name="user_id" id="user_id" placeholder="User Id" value="<?php echo $user_id; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Email Login</label> <i> <?php echo form_error('email_login') ?> </i> </td>
            <td><input type="text" class="form-control" name="email_login" id="email_login" placeholder="Email Login" value="<?php echo $email_login; ?>" /></td>
          </tr>
          <tr>
            <td> <label>User Agent</label> <i> <?php echo form_error('user_agent') ?> </i> </td>
            <td><input type="text" class="form-control" name="user_agent" id="user_agent" placeholder="User Agent" value="<?php echo $user_agent; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Ip Address</label> <i> <?php echo form_error('ip_address') ?> </i> </td>
            <td><input type="text" class="form-control" name="ip_address" id="ip_address" placeholder="Ip Address" value="<?php echo $ip_address; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Note</label> <i> <?php echo form_error('note') ?> </i> </td>
            <td><textarea class="form-control" rows="3" name="note" id="note" placeholder="Note"><?php echo $note; ?></textarea></td>
          </tr>
          <tr>
            <td> <label>Url</label> <i> <?php echo form_error('url') ?> </i> </td>
            <td><input type="text" class="form-control" name="url" id="url" placeholder="Url" value="<?php echo $url; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Actions</label> <i> <?php echo form_error('actions') ?> </i> </td>
            <td><input type="text" class="form-control" name="actions" id="actions" placeholder="Actions" value="<?php echo $actions; ?>" /></td>
          </tr>
          <tr>
            <td> <label>Timestamp</label> <i> <?php echo form_error('timestamp') ?> </i> </td>
            <td><input type="text" class="form-control" name="timestamp" id="timestamp" placeholder="Timestamp" value="<?php echo $timestamp; ?>" /></td>
          </tr>
          <tr>
            <td colspan='2'>
              <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
              <a href="<?php echo site_url($module.'/logs') ?>" class="btn btn-default">Cancel</a>
            </td>
          </tr>
        </table>
          <input type="hidden" name="actions_logs_id" value="<?php echo $actions_logs_id; ?>" /> 
      <?php form_close(); ?>
    </div>
</div> 