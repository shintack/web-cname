
<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <a href="<?php echo site_url($module."/logs/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
          <tr><td>User Id</td><td><?php echo $user_id; ?></td></tr>
          <tr><td>Email Login</td><td><?php echo $email_login; ?></td></tr>
          <tr><td>User Agent</td><td><?php echo $user_agent; ?></td></tr>
          <tr><td>Ip Address</td><td><?php echo $ip_address; ?></td></tr>
          <tr><td>Note</td><td><?php echo $note; ?></td></tr>
          <tr><td>Url</td><td><?php echo $url; ?></td></tr>
          <tr><td>Actions</td><td><?php echo $actions; ?></td></tr>
          <tr><td>Timestamp</td><td><?php echo $timestamp; ?></td></tr>
          <tr><td></td><td><a href="<?php echo site_url($module."/logs") ?>" class="btn btn-default">Cancel</a></td></tr>
        </table>
    </div>
</div>
