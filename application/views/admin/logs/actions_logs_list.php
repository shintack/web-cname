

<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalAdd"> <i class="fa fa-plus"></i> Add</button>
            <?php /*
            <a href="<?php echo site_url($module."/logs/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            */ ?>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <!-- tabel here -->
        <div style="overflow: auto; margin-bottom: 10px;">
            <table id="table" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th align="center" width="10px">No</th>
                        <th>User Id</th>
                        <th>Email Login</th>
                        <th>User Agent</th>
                        <th>Ip Address</th>
                        <th>Note</th>
                        <th>Url</th>
                        <th>Actions</th>
                        <th>Timestamp</th>            
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                </tbody>

                <tfoot>
                    <tr>
                        <th align="center" width="10px">No</th>
                        <th>User Id</th>
                        <th>Email Login</th>
                        <th>User Agent</th>
                        <th>Ip Address</th>
                        <th>Note</th>
                        <th>Url</th>
                        <th>Actions</th>
                        <th>Timestamp</th>            
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- ./tabel here -->

        <div class="row">
            <div class="col-md-6">
                <?php echo anchor(site_url($module.'/logs/excel'), '<i class="fa fa-file-excel-o"></i>', 'title="Export to Excel" class="btn btn-success btn-sm"'); ?>
                <?php echo anchor(site_url($module.'/logs/word'), '<i class="fa fa-file-word-o"></i>', 'title="Export to Word"class="btn btn-info btn-sm"'); ?>
                <?php echo anchor(site_url($module.'/logs/pdf'), '<i class="fa fa-file-pdf-o"></i>', 'title="Export to Pdf"class="btn btn-danger btn-sm"'); ?>
            </div>
            <div class="col-md-6 text-right"></div>
        </div>

    </div>
</div>


<!-- Modal -->
<div id="myModalAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
<?php echo form_open($input["action"]); ?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add <?=$title?></h4>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">

          <tr>
            <td>User Id <?php echo form_error('user_id') ?></td>
            <td><input type="text" class="form-control" name="user_id" id="user_id" placeholder="User Id" value="<?php echo $input['user_id']; ?>" /></td>
          </tr>
          <tr>
            <td>Email Login <?php echo form_error('email_login') ?></td>
            <td><input type="text" class="form-control" name="email_login" id="email_login" placeholder="Email Login" value="<?php echo $input['email_login']; ?>" /></td>
          </tr>
          <tr>
            <td>User Agent <?php echo form_error('user_agent') ?></td>
            <td><input type="text" class="form-control" name="user_agent" id="user_agent" placeholder="User Agent" value="<?php echo $input['user_agent']; ?>" /></td>
          </tr>
          <tr>
            <td>Ip Address <?php echo form_error('ip_address') ?></td>
            <td><input type="text" class="form-control" name="ip_address" id="ip_address" placeholder="Ip Address" value="<?php echo $input['ip_address']; ?>" /></td>
          </tr>
          <tr>
            <td>Note <?php echo form_error('note') ?></td>
            <td><textarea class="form-control" rows="3" name="note" id="note" placeholder="Note"><?php echo $input['note']; ?></textarea></td>
          </tr>
          <tr>
            <td>Url <?php echo form_error('url') ?></td>
            <td><input type="text" class="form-control" name="url" id="url" placeholder="Url" value="<?php echo $input['url']; ?>" /></td>
          </tr>
          <tr>
            <td>Actions <?php echo form_error('actions') ?></td>
            <td><input type="text" class="form-control" name="actions" id="actions" placeholder="Actions" value="<?php echo $input['actions']; ?>" /></td>
          </tr>
          <tr>
            <td>Timestamp <?php echo form_error('timestamp') ?></td>
            <td><input type="text" class="form-control" name="timestamp" id="timestamp" placeholder="Timestamp" value="<?php echo $input['timestamp']; ?>" /></td>
          </tr>
        </table>
        <input type="hidden" name="actions_logs_id" value="<?php echo $input['actions_logs_id']; ?>" /> 
      </div>
      <div class="modal-footer">
            <button type="submit" class="btn btn-primary"><?php echo $input["button"] ?></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>
<?php form_close(); ?>

  </div>
</div>


