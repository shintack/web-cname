

<script>

var datatables;
var table           = $("#table");
var form            = $("<?=isset($form_input)?$form_input:'form'?>");
var urlDataTable    = "<?=isset($urlDataTable)?$urlDataTable:''; ?>";
var urlGetData      = "<?=isset($urlGetData)?$urlGetData:''; ?>";
var urlSave         = "<?=isset($urlSave)?$urlSave:''; ?>";
var urlDelete       = "<?=isset($urlDelete)?$urlDelete:''; ?>";

var csrfName        = "<?=$this->security->get_csrf_token_name(); ?>";
var csrfHash        = "<?=$this->security->get_csrf_hash(); ?>";

//custom search

$(document).ready(function() {

    <?php if(isset($urlDataTable)): ?>
    datatables = table.DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": urlDataTable,
            "type": "POST",
            "data": {csrfName:csrfHash},
        },
        "columnDefs": [
        {
            "targets": [ 0 ],
            "orderable": false,
        },
        ],
    });
    <?php endif; ?>

    $('#btn-filter').click(function(){ //button filter event click
        alert('filter');
        datatables.ajax.reload();  //just reload table
    });

    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        datatables.ajax.reload();  //just reload table
        alert('reset');
    });

});

function getData(id)
{
    $.ajax({
        type : "POST",
        url  : urlGetData,
        dataType : "JSON",
        data : {id:id},
        beforeSend: function(){
            //$("#loading").show();
        },
        success: function(data){
            //$("#loading").hide();
            clearForm();
            $("#myModalAdd").modal("show");
            <?=isset($form)?$form:''?>
        }
    });
    return false;
}

function delData(id)
{
    swal({
        title: "Konfirmasi!",
        text: "Yakin akan data ini akan dihapus?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            deleteAction(id)
        } else {
            swal("Data aman!");
        }
    });
}

function deleteAction(id)
{
    $.ajax({
        type : "POST",
        url  : urlDelete,
        //dataType : "JSON",
        data : {id:id},
        beforeSend: function(){
            $("#loading").show();
        },
        success: function(data){
            $("#loading").hide();
            if(data==='ok'){
                sweat('Info','berhasil di hapus','success');
            }else if(data==='ko'){
                sweat('Info','gagal di hapus','warning');
            }
            reloadTable();
        }
    });
    return false;
}

form.submit(function(e){
    e.preventDefault();
        $.ajax({
            url:urlSave,
            type:"POST",
            data:new FormData(this),
            processData:false,
            contentType:false,
            cache:false,
            async:false,
            beforeSend: function(){
                $("#loading").show();
            },
            success: function(data){
                $("#loading").hide();
                var json_data = data;
                obj = JSON.parse(json_data);
                if (obj.status===false) {
                    clearEror();
                    <?=isset($form_er)?$form_er:''?>
                }else{
                    $('#myModalAdd').modal('hide');
                    sweat('Info',obj.message,'info');
                    reloadTable();
                }
            }
    });
});

function clearForm()
{
    form.find('input:text, input:hidden, input:password, select, textarea').val('');
    form.find('input:radio, input:checkbox').prop('checked', false);
    clearEror();
}

function clearEror()
{
    <?=isset($form_er_clr)?$form_er_clr:''?>
}

function reloadTable()
{
    datatables.ajax.reload(null,true);
}

/*Tambahan */
function clearVehicleId() {
    $("[name='vehicle_id']").val('');
}



</script>
