
<form id="themes">
<?php //echo form_open(isset($action)?$action:current_url()); ?>
  <table class="table table-bordered table-striped table-hover">

          <tr>
            <td> <label>Name</label> </td>
            <td>
                <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo set_value('name',isset($name)?$name:''); ?>" />
                <span id="er_name"><?php echo form_error('name') ?></span>
            </td>
          </tr>
          <tr>
            <td> <label>Path</label> </td>
            <td>
                <input type="text" class="form-control" name="path" id="path" placeholder="Path" value="<?php echo set_value('path',isset($path)?$path:''); ?>" />
                <span id="er_path"><?php echo form_error('path') ?></span>
            </td>
          </tr>
          <tr>
            <td> <label>Is Default</label> </td>
            <td>
                <input type="text" class="form-control" name="is_default" id="is_default" placeholder="Is Default" value="<?php echo set_value('is_default',isset($is_default)?$is_default:''); ?>" />
                <span id="er_is_default"><?php echo form_error('is_default') ?></span>
            </td>
          </tr>
          <tr>
            <td> <label>Is Active</label> </td>
            <td>
                <input type="text" class="form-control" name="is_active" id="is_active" placeholder="Is Active" value="<?php echo set_value('is_active',isset($is_active)?$is_active:''); ?>" />
                <span id="er_is_active"><?php echo form_error('is_active') ?></span>
            </td>
          </tr>

          <tr>
            <td colspan='2' align='right'>
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </td>
          </tr>

        </table>
          <input type="hidden" name="id" value="<?php echo set_value('id',isset($id)?$id:''); ?>" />
<?php //echo form_close(); ?>
</form>
