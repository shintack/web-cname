
<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>    
</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <a href="<?php echo site_url($module."/themes/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
          <tr><td>Name</td><td><?php echo $name; ?></td></tr>
          <tr><td>Path</td><td><?php echo $path; ?></td></tr>
          <tr><td>Is Default</td><td><?php echo $is_default; ?></td></tr>
          <tr><td>Is Active</td><td><?php echo $is_active; ?></td></tr>
          <tr><td>Created At</td><td><?php echo $created_at; ?></td></tr>
          <tr><td>Updated At</td><td><?php echo $updated_at; ?></td></tr>
          <tr><td></td><td><a href="<?php echo site_url($module."/themes") ?>" class="btn btn-default">Cancel</a></td></tr>
        </table>
    </div>
</div>
