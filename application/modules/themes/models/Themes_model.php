<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @author          insan muttaqien [ shintackeror.web.id | shintack@gmail.com ]
 * @license         MIT License
 * @link            https://gitlab.com/shintack
 */

class Themes_model extends MY_Model
{

    private $table = 'themes';
    private $id    = 'id';
    private $order = 'DESC';
    private $field = [ 'name','path','is_default','is_active','created_at','updated_at', ];
    //variabel untuk datatable
    private $_column_order;
    private $_column_search;
    private $_order;

    function __construct()
    {
        parent::__construct();
        $this->_column_order = [ NULL,'name','path','is_default','is_active','created_at','updated_at', ];
        $this->_column_search = $this->field;
        $this->_order = [ $this->id=>$this->order ];
    }

    function structure()
    {
        return ['table' => $this->table, 'pk' => $this->id, 'field' => $this->field];
    }

    //datatable
    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->_column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item,$_POST['search']['value']);
                }
                if(count($this->_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->_order))
        {
            $order = $this->_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    private function count_filtered()
    {
        $this->_get_datatables_query();
        return $this->db->get()->num_rows();
    }

    private function count_all()
    {
        return $this->db->from($this->table)->count_all_results();
    }

    function get_datatables()
    {
        $data = array();

        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();

        $no = $_POST['start'];
        foreach ($query->result() as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->name;
            $row[] = $field->path;
            $row[] = $field->is_default;
            $row[] = $field->is_active;
            $row[] = $field->created_at;
            $row[] = $field->updated_at;
            $row[] = '
            <center>
                <a class="btn btn-info btn-sm" title="Edit" onclick="getData('.$field->id.')"> <i class="fa fa-edit"></i> </a>
                <a class="btn btn-danger btn-sm" title="Delete" onclick="delData('.$field->id.')"> <i class="fa fa-trash"></i> </a>
            </center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->count_all(),
            "recordsFiltered" => $this->count_filtered(),
            "data" => $data,
        );
        return json_encode($output);
    }

    function get_one($id=NULL)
    {
        if ($id===NULL) return NULL;
        return $this->db->get_where($this->table,[$this->id=>$id])->row();
    }

    function insert_data()
    {
        $data = $this->input->post(NULL,TRUE);
        $id   = $this->input->post('id',TRUE);
        if ($id!=NULL) {
            $action = $this->db->where('id',$id)->update($this->table,$data);
        }else{
            $data['created_at'] = date('Y-m-d H:i:s');
            $action = $this->db->insert($this->table,$data);
        }
        return $action;
    }

    function delete_data($id)
    {
        return $this->db->where('id',$id)->delete($this->table);
    }

    function _rules()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('path', 'path', 'trim|required');
        $this->form_validation->set_rules('is_default', 'is default', 'trim|required');
        $this->form_validation->set_rules('is_active', 'is active', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            return [
                'name' => form_error('name'),
                'path' => form_error('path'),
                'is_default' => form_error('is_default'),
                'is_active' => form_error('is_active'),
                'id' => form_error('id'),
            ];
        }return TRUE;
    }

    function _set_value($row=NULL)
    {
        if($row){
            $data = [
                'button' => 'Update',
                'action' => site_url($this->_mod.'/themes/update_action'),
				'id' => set_value('id', $row->id),
				'name' => set_value('name', $row->name),
				'path' => set_value('path', $row->path),
				'is_default' => set_value('is_default', $row->is_default),
				'is_active' => set_value('is_active', $row->is_active),
			];
        }else{
            $data = [
                'button' => 'Save',
                'action' => site_url($this->_mod.'/themes/create_action'),
				'id' => set_value('id'),
				'name' => set_value('name'),
				'path' => set_value('path'),
				'is_default' => set_value('is_default'),
				'is_active' => set_value('is_active'),
			];
        }
        $data['delete'] = site_url($this->_mod.'/themes/delete');
        return $data;
    }

    function _get_post()
    {
        $data = [
            'name' => $this->input->post('name',TRUE),
            'path' => $this->input->post('path',TRUE),
            'is_default' => $this->input->post('is_default',TRUE),
            'is_active' => $this->input->post('is_active',TRUE),
        ]; return $data;
    }

    function string_ajax()
    {
        return [
            'form'      => '
                $("[name=\'name\']").val(data.name);
                $("[name=\'path\']").val(data.path);
                $("[name=\'is_default\']").val(data.is_default);
                $("[name=\'is_active\']").val(data.is_active);
                $("[name=\'id\']").val(data.id);',
            'form_er' => '
                $("#er_name").append(obj.message.name);
                $("#er_path").append(obj.message.path);
                $("#er_is_default").append(obj.message.is_default);
                $("#er_is_active").append(obj.message.is_active);
                $("#er_id").append(obj.message.id);',
            'form_er_clr'=> '
                $("#er_name").text(\'\');
                $("#er_path").text(\'\');
                $("#er_is_default").text(\'\');
                $("#er_is_active").text(\'\');
                $("#er_id").text(\'\');',
        ];
    }

    function _get_form($row=NULL)
    {
        return
        [
            'id' => [
                'type'  => 'text',
                'name'  => 'id',
                'id'    => 'id',
                'value' => set_value('id',isset($row->id)?$row->id:''),
                'class' => 'form-control'
            ],
            'name' => [
                'type'  => 'text',
                'name'  => 'name',
                'id'    => 'name',
                'value' => set_value('name',isset($row->name)?$row->name:''),
                'class' => 'form-control'
            ],

            'path' => [
                'type'  => 'text',
                'name'  => 'path',
                'id'    => 'path',
                'value' => set_value('path',isset($row->path)?$row->path:''),
                'class' => 'form-control'
            ],

            'is_default' => [
                'type'  => 'text',
                'name'  => 'is_default',
                'id'    => 'is_default',
                'value' => set_value('is_default',isset($row->is_default)?$row->is_default:''),
                'class' => 'form-control'
            ],

            'is_active' => [
                'type'  => 'text',
                'name'  => 'is_active',
                'id'    => 'is_active',
                'value' => set_value('is_active',isset($row->is_active)?$row->is_active:''),
                'class' => 'form-control'
            ],

            'created_at' => [
                'type'  => 'text',
                'name'  => 'created_at',
                'id'    => 'created_at',
                'value' => set_value('created_at',isset($row->created_at)?$row->created_at:''),
                'class' => 'form-control'
            ],

            'updated_at' => [
                'type'  => 'text',
                'name'  => 'updated_at',
                'id'    => 'updated_at',
                'value' => set_value('updated_at',isset($row->updated_at)?$row->updated_at:''),
                'class' => 'form-control'
            ],
        ];
    }


}

/* End of file Themes_model.php */
/* Location: ./application/models/Themes_model.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2019-02-02 03:46:43 */
/* http://shintackeror.web.id thanks to http://harviacode.com */
