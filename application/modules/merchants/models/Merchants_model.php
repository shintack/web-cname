<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @author          insan muttaqien [ shintackeror.web.id | shintack@gmail.com ]
 * @license         MIT License
 * @link            https://gitlab.com/shintack
 */

class Merchants_model extends MY_Model
{

    private $table = 'merchants';
    private $id    = 'id';
    private $order = 'DESC';
    private $field = [ 'name','domain','theme_id','is_active','created_at','updated_at', ];
    //variabel untuk datatable
    private $_column_order;
    private $_column_search;
    private $_order;

    function __construct()
    {
        parent::__construct();
        $this->_column_order = [ NULL,'name','domain','theme_id','is_active','created_at','updated_at', ];
        $this->_column_search = $this->field;
        $this->_order = [ $this->id=>$this->order ];
    }

    function structure()
    {
        return ['table' => $this->table, 'pk' => $this->id, 'field' => $this->field];
    }

    //datatable
    private function _get_datatables_query()
    {
        $this->db->select("*,
            (SELECT name FROM themes WHERE themes.id = $this->table.theme_id) as theme_id,
        ");
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->_column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item,$_POST['search']['value']);
                }
                if(count($this->_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->_order))
        {
            $order = $this->_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    private function count_filtered()
    {
        $this->_get_datatables_query();
        return $this->db->get()->num_rows();
    }

    private function count_all()
    {
        return $this->db->from($this->table)->count_all_results();
    }

    function get_datatables()
    {
        $data = array();

        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();

        $no = $_POST['start'];
        foreach ($query->result() as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->name;
            $row[] = $field->domain;
            $row[] = $field->theme_id;
            $row[] = $field->is_active;
            $row[] = $field->created_at;
            $row[] = $field->updated_at;
            $row[] = '
            <center>
                <a class="btn btn-info btn-sm" title="Edit" onclick="getData('.$field->id.')"> <i class="fa fa-edit"></i> </a>
                <a class="btn btn-danger btn-sm" title="Delete" onclick="delData('.$field->id.')"> <i class="fa fa-trash"></i> </a>
            </center>';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->count_all(),
            "recordsFiltered" => $this->count_filtered(),
            "data" => $data,
        );
        return json_encode($output);
    }

    function get_one($id=NULL)
    {
        if ($id===NULL) return NULL;
        return $this->db->get_where($this->table,[$this->id=>$id])->row();
    }

    function insert_data()
    {
        $data = $this->input->post(NULL,TRUE);
        $id   = $this->input->post('id',TRUE);
        if ($id!=NULL) {
            $action = $this->db->where('id',$id)->update($this->table,$data);
        }else{
            $data['created_at'] = date('Y-m-d H:i:s');
            $action = $this->db->insert($this->table,$data);
        }
        return $action;
    }

    function delete_data($id)
    {
        return $this->db->where('id',$id)->delete($this->table);
    }

    function _rules()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('domain', 'domain', 'trim|required');
        $this->form_validation->set_rules('theme_id', 'theme_id', 'trim|required');
        $this->form_validation->set_rules('is_active', 'is active', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run() == FALSE) {
            return [
                'name' => form_error('name'),
                'domain' => form_error('domain'),
                'theme_id' => form_error('theme_id'),
                'is_active' => form_error('is_active'),
                'id' => form_error('id'),
            ];
        }return TRUE;
    }

    function _set_value($row=NULL)
    {
        if($row){
            $data = [
                'button' => 'Update',
                'action' => site_url($this->_mod.'/merchants/update_action'),
				'id' => set_value('id', $row->id),
				'name' => set_value('name', $row->name),
				'domain' => set_value('domain', $row->domain),
                'theme_id' => set_value('theme_id', $row->theme_id),
				'is_active' => set_value('is_active', $row->is_active),
				'created_at' => set_value('created_at', $row->created_at),
				'updated_at' => set_value('updated_at', $row->updated_at),
			];
        }else{
            $data = [
                'button' => 'Save',
                'action' => site_url($this->_mod.'/merchants/create_action'),
				'id' => set_value('id'),
				'name' => set_value('name'),
				'domain' => set_value('domain'),
                'theme_id' => set_value('theme_id'),
    			'is_active' => set_value('is_active'),
				'created_at' => set_value('created_at'),
				'updated_at' => set_value('updated_at'),
			];
        }
        $data['delete'] = site_url($this->_mod.'/merchants/delete');
        return $data;
    }

    function _get_post()
    {
        $data = [
            'name' => $this->input->post('name',TRUE),
            'domain' => $this->input->post('domain',TRUE),
            'theme_id' => $this->input->post('theme_id',TRUE),
            'is_active' => $this->input->post('is_active',TRUE),
        ]; return $data;
    }

    function string_ajax()
    {
        return [
            'form'      => '
                $("[name=\'name\']").val(data.name);
                $("[name=\'domain\']").val(data.domain);
                $("[name=\'theme_id\']").val(data.theme_id);
                $("[name=\'is_active\']").val(data.is_active);
                $("[name=\'created_at\']").val(data.created_at);
                $("[name=\'updated_at\']").val(data.updated_at);
                $("[name=\'id\']").val(data.id);',
            'form_er' => '
                $("#er_name").append(obj.message.name);
                $("#er_domain").append(obj.message.domain);
                $("#er_theme_id").append(obj.message.theme_id);
                $("#er_is_active").append(obj.message.is_active);
                $("#er_created_at").append(obj.message.created_at);
                $("#er_updated_at").append(obj.message.updated_at);
                $("#er_id").append(obj.message.id);',
            'form_er_clr'=> '
                $("#er_name").text(\'\');
                $("#er_domain").text(\'\');
                $("#er_theme_id").text(\'\');
                $("#er_is_active").text(\'\');
                $("#er_created_at").text(\'\');
                $("#er_updated_at").text(\'\');
                $("#er_id").text(\'\');',
        ];
    }

}

/* End of file Merchants_model.php */
/* Location: ./application/models/Merchants_model.php */
/* Please DO NOT modify this information : */
/* Generated by shintackeror 2019-02-02 03:46:22 */
/* http://shintackeror.web.id thanks to http://harviacode.com */
