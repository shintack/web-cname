
<!--
<div style="margin-top: -30px;">
    <h2><?php echo $title ?></h2>
    <?php echo create_breadcrumb($home, $home_link); ?>
</div>
-->
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-list"></i> <?php echo $sub_title; ?> </h3>
        <div class="box-tools pull-right">
            <button onclick="clearForm()" type="button" class="btn btn-default" data-toggle="modal" data-target="#myModalAdd"> <i class="fa fa-plus"></i> Add</button>
            <?php /*
            <a href="<?php echo site_url($module."/merchants/create") ?>">
                <button type="button" class="btn btn-sm btn-primary" > <i class="fa fa-plus"></i> Add</button>
            </a>
            */ ?>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <!-- tabel here -->
        <div style="overflow: auto; margin-bottom: 10px;">
            <table id="table" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th align="center" width="10px">No</th>
                        <th>Name</th>
                        <th>Domain</th>
                        <th>Theme</th>
                        <th>Is Active</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                </tbody>

            </table>
        </div>
        <!-- ./tabel here -->

        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6 text-right"></div>
        </div>

    </div>
</div>

<!-- Modal -->
<div id="myModalAdd" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add <?=$title?></h4>
            </div>
            <div class="modal-body">
                <?php $this->load->view($module."/merchants/merchants_form.php") ?>
            </div>

            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>

<?php $this->load->view("admin/crud_js") ?>
